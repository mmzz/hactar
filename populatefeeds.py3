#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018-2022 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
#import urltools
import urllib.parse
from pymongo import MongoClient
from argparse import ArgumentParser

from hactar.db import openDB
import constant as k




def load_list(fileName):
    ''' reads a list of items from file, one per line 
        comments excluded '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            lst.append(line.strip())
    return(list(set(lst)))


#def buildFeedSource(site,name,URL,feedClass,protocol,hp):
def buildFeedSource(country,site,name,URL,protocol,hp,category):
    homepage=False
    
    if hp.lower == "true": 
         homepage = True
    feedSource= ({
               'dateAdded': datetime.datetime.now(),
               'URL':URL,
               'site':site,
               'country':country,
               'active':True,
               'name':name,
               'protocol':protocol,
               'feedCategory':category,
               'rating':'',
               'oldRating':'',
               'homepage': homepage,
            })
    return(feedSource)


# #
# MAIN
# #

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-d", "--dryrun", action="store_true", help="dont write to db")
parser.add_argument("-i", "--interactive", action="store_true", dest="interactive", help="insert interactively")
parser.add_argument("-j", "--json",  help="insert json")
parser.add_argument("-f", "--file",  help="insert tab-separated-values file")


(args) = parser.parse_args()

db=openDB(args.server,args.dbname)

#dbname=args.dbname

#if args.server:
#    server=args.server
#else:
#    server="localhost:27017"
#client = MongoClient(server)
#db = client[dbname]
#feedsource="feedsource"

feedsourceDB = db[k.FEEDSOURCE]
rulesDB = db[k.RULES]



lines=[]
if args.file:
    lines=load_list(args.file)
    if len(lines):
        print(f"read {len(lines)} records")
    else:
        exit(f"no input from {args.file}")
        
if args.interactive:
    # interactive terminal
    site = input('SITE (newspaper) ')  
    name = input('feed name ')  
#    feedclass = input('feed class')
    URL = input('feed URL ')  
    hp = input('homepage (True/False) ')  
    protocol = input('protocol (rss|http)')
    feedClass= input('feed Class (HOME|CR|CU|EC|ED|ST|SP|NONE)')
    buildFeedSource(site,name,URL,protocol,hp,category)
    if args.verbose: print(feedSource)
    if not args.dryrun: 
            res=feedsourceDB.insert_one(feedSource)
            print(res)

elif args.json:
    input_file = open (args.json)
    json_array = json.load(input_file)
    store_list = []
    
    for item in json_array:    
        if args.verbose: print(item)

        # populate feed sources db
        if (not args.dryrun):
            item['dateAdded']=datetime.datetime.now()
            res=feedsourceDB.insert_one(item)
            print(res)

else:
    if lines == []:
        exit("no input")
    for line in lines:
        ##1-country	2-category	3-site	4-hp	5-protocol	6-name	7-URL
        (country,category,site,hp,protocol,name,URL)=line.strip().split("\t")
        feedSource=    buildFeedSource(country,site,name,URL,protocol,hp,category)

        if args.dryrun: 
            print(f"dry run {feedSource}")
        else:
            res=feedsourceDB.insert_one(feedSource)
            print(res,feedSource)
