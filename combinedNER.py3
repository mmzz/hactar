#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2020 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
import pprint
import textstat
from pymongo import MongoClient, HASHED
from argparse import ArgumentParser

import constant as k
from hactar.db import loadRules
from hactar.db import openDB
from hactar.db import logEvent
from hactar.db import updateArticleNER
from hactar.strings import doNEC

import warnings
warnings.filterwarnings("ignore")


# requires loading of language libraries
#         python3.7 -m spacy download it

THISMODULE="combinedNER"
THISMODULEVERSION=2 # refferred to as k.NERVERSION in database/article
# #
# Functions
# #

    

def loadLanguageRule(rulesDB,languageRule):
    langRules=loadRules(rulesDB,k.LANGUAGE)
    if langRules == {}:
        logEvent(args.verbose,logDB,THISMODULE,"warn","","","","skip: no "+k.LANGUAGE+" rule in "+k.RULES+" DB")
        lang=""
    else:
        if langRules[languageRule]:
            lang=langRules[languageRule]
        else:
            logEvent(args.verbose,logDB,THISMODULE,"warn","","","","skip: no "+languageRule+" rule in "+k.LANGUAGE)
            lang=""
    return(lang)


def spacyNer(text,title,lang):
    import re
    import spacy
    
    from hactar.strings import convertCodePoints
    from langdetect import detect

    text=re.sub('»','"',text)
    text=re.sub('«','"',text)
    text=re.sub('’','\'',text)
    t=convertCodePoints(text)
    if t == False:
        logEvent(args.verbose,logDB,THISMODULE,"err","",str(item[k.UUID]),"","codePointConversionError:"+k.TEXT)
    else:
        text=t
    text=convertCodePoints(text)

#    if len(text) > 200:
#        # detect language in body article, and use it if different than default language
#        dlang=detect(text)
#        if dlang != lang and dlang != "":
#            try:
#                nlp = spacy.load(dlang)
#            except Exception as e:
#                #python3.7 -m spacy download it
#                if args.verbose: 
#                    logEvent(args.verbose,logDB,THISMODULE,"warn","",item[k.UUID],"","WARN: Spacy error loading ["+dlang+"]. Reverting to ["+lang+"].")
    try:
        nlp = spacy.load(lang)
    except Exception as e:
        if args.verbose:
            logEvent(args.verbose,logDB,THISMODULE,"warn","",item[k.UUID],"","ERR: Exiting. Spacy tried to load ["+lang+"] but got error ["+str(e)+"] Try [python -m spacy download "+lang+"].")
        exit()

    doc = nlp(text)    
    body={}
    for ent in doc.ents:
        body[ent.text]=ent.label_

    doc = nlp(title)    
    title={}
    for ent in doc.ents:
        title[ent.text]=ent.label_
        
    return(body,title,lang)


# #
# MAIN
# #

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-c", "--collection", dest="collection",  help="mongodb collection, defaults to {k.ARTICLE}")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-Y", "--year", dest="year", help="limit action to one year")
parser.add_argument("--dryrun", action="store_true", help="dont write to db")
parser.add_argument("--overwrite", action="store_true", help="reassign a class to all items in db")
parser.add_argument("--index", action="store_true", help="force build indexing")
parser.add_argument("-U", "--uuid", dest="uuid", help="limit action to a single item identified by uuid")
      

(args) = parser.parse_args()

db=openDB(args.server,args.dbname)
#feedsourceDB = db[k.FEEDSOURCE]
if args.collection:
    articleDB = db[args.collection]
else:
    articleDB = db[k.ARTICLE]
logDB = db[k.EVENTLOG]
rulesDB = db[k.RULES]



if args.index:
    logEvent(args.verbose,logDB,THISMODULE,"ok","","","","indexing")
    try:
#        articleDB.create_index([(k.NER, HASHED)],background=True)
        articleDB.create_index(k.NERVERSION)
        articleDB.create_index(k.YEAR)
        articleDB.create_index(k.UUID)
    except Exception as e:
        logEvent(args.verbose,logDB,THISMODULE,"warn","","","","Indexing warning message: "+str(e))



if args.overwrite:
    search= {}
else:
    conditions=[
        {k.NERVERSION: {'$exists': False}},
    ]
    if args.year:
          conditions.append({k.YEAR: int(args.year)})
    if args.uuid:
          conditions.append({k.UUID: args.uuid})
    search={'$and': conditions}

lang=loadLanguageRule(rulesDB, k.SPACYLANG)
names={}


    
all = articleDB.find(search,no_cursor_timeout=True).batch_size(1000)
for item in all:
    if k.CLEANTEXT not in item.keys():
        logEvent(args.verbose,logDB,THISMODULE,"warn","",item[k.UUID],"","no "+k.CLEANTEXT)
        break
    if k.TITLE not in item.keys():
        logEvent(args.verbose,logDB,THISMODULE,"warn","",item[k.UUID],"","no "+k.TITLE)
        break
    lang=loadLanguageRule(rulesDB,k.SPACYLANG)
    (nerb,nert,lang)=spacyNer(item[k.CLEANTEXT],item[k.TITLE],lang)
    
    nerbSet=set(nerb.keys())
    nertSet=set(nert.keys())

    necbSet=set(doNEC(item[k.CLEANTEXT],False))
    nectSet=set(doNEC(item[k.TITLE],True))

    bothb= list(necbSet & nerbSet)
    botht= list(nectSet & nertSet)
    
    fnerb={}
    for ne in bothb:
        if ne in nerb.keys():
            fnerb.update({ne: nerb[ne]})

    fnert={}
    for ne in botht:
        if ne in nert.keys():
            fnert.update({ne: nert[ne]})
            names.setdefault(ne, 0)
            names[ne] += 1

    # compute readability score
    textstat.set_lang(lang)
    fres=textstat.flesch_reading_ease(item[k.CLEANTEXT])

    nerd = {
        "title": fnert,
        "body": fnerb,
        "language": lang,
        "fres": fres,
    }
        
    if not args.dryrun:
        updateArticleNER(articleDB,item[k.ID],nerd,THISMODULEVERSION)

    logEvent(args.verbose,logDB,THISMODULE,"ok","",item[k.UUID],"",str(len(fnerb))+"/"+str(len(fnert))+" named entities (body/title). fres: "+str(fres))



