#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import codecs
import bson
import bz2
import os
import datetime
import re
import validators
from bson import Binary
from pymongo import MongoClient
from argparse import ArgumentParser


parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-D", "--dir", type=str,help='target directory name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-k", "--key", dest="key", help="key:value")
(args) = parser.parse_args()
if (not args.dbname) :
        print(sys.argv[0], ": database name argument needed")
        quit()
dbname=args.dbname

if args.dir: 
    try: 
        os.makedirs(args.dir)
    except OSError:
       if not os.path.isdir(args.dir):
           raise
           quit()

if args.server:
       server=args.server
else:
       server="localhost:27017"

client = MongoClient(server)
db = client[dbname]
now = datetime.datetime.now()

articleDB = db["article"]

if args.key:
    (key,value)= args.key.split(':')
    if args.verbose: print(f"# extracting {key}:{value} from {args.server}/{args.dbname}/article")
    all=articleDB.find({key : value}, no_cursor_timeout=False)
else:   
    all = articleDB.find({},no_cursor_timeout=False)
    if args.verbose: print(f"# extracting all from {args.server}/{args.dbname}/article")
    #all = articleDB.find({"source":  re.compile('/.*AVVENIRE.*/')},no_cursor_timeout=False)

if args.dir:
    UTF8Writer = codecs.getwriter('utf8')
    sys.stdout = UTF8Writer(sys.stdout)


for item in all:
    oid=str(item['_id'])
    if args.dir:
        fname=args.dir+'/article'+'-'+oid+'.txt'
        f=open(fname, 'w+')
        sys.stdout = f
    for k in item.keys():
        if args.dir:
            f.write ((str(k).upper())+": "+
                str(item[k])+
                u"\n")
        else:
            print((str(k).upper())+": "+
                str(item[k])+
                u"\n")
    if args.dir:
        f.flush()
        f.close()
