#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo,
#                  2020 Alberto Zanatta
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import getopt
import codecs
import bson
import bz2
import re
import validators
import hashlib
import lxml
import unicodedata
import datetime
import http.client
import chardet
import uuid
import json
import hashlib
import pprint

from scrapy.selector import Selector
from bson import Binary
from pymongo import MongoClient
from bson import ObjectId
#from lxml.html.clean import Cleaner
from lxml import etree, html
from argparse import ArgumentParser
from dateutil.parser import parse
from dateutil.tz import gettz
import dateparser 

from hactar.net import getArticle
from hactar.db import logEvent
from hactar.db import markFeedActivity
from hactar.db import openDB

from hactar.process import processArticle
from hactar.strings import cleanText
from hactar.strings import doNEC

import constant as k

import importlib

def debug(item):
    print("#DEBUG|",end='')
    for key in [k.YEAR,k.URLS,k.SOURCES,k.DATESPUBLISHED,k.TITLE,k.YEAR,k.CLEANTEXT]:
        if key in itemkeys:
            print(item[key],end='| ')

#                pprint.pprint(item)


def markDontindex(item,reason):
    articleDB.update_one( { k.ID: item[k.ID]},
                          { '$set': {k.DONTINDEX: str(reason)}})


def normalizeFeed(s,delim):
    # trim leading spaces
    if len(s) == 0:
        return()
    if s[0]==' ':
        s=s[1:]
    # consider only first three elements of more structured feeds 
    if delim !="":
        l=s.split(delim)[0:3]
        s="-".join(l)
    s=s.replace('\'','')
    s=s.replace(',','')
    s=s.replace(' ','-')
    return(s)

class Counter:
    all = 0
    errors =0
    ok =0
    limit=0

    def display(self):
        print(f"# processed {self.all} items: {self.ok} ok, {self.errors} errors")



def processItem(item):    
    itemkeys=set(list(item.keys()))
    i={}
    if k.UUID in itemkeys:
        counter.ok+=1
        if isinstance(item[k.URLS],str):
            if not args.dryrun:
                articleDB.update_one( { k.ID: item[k.ID]},
                    { '$set': {k.URLS: [item[k.URLS]]}})
            print(f"{item[k.ID]}: {item[k.URLS]} --> {[item[k.URLS]]}")
            return()
        if isinstance(item[k.URLS][0],list):
            if not args.dryrun:
                articleDB.update_one( { k.ID: item[k.ID]},
                    { '$set': {k.URLS: item[k.URLS][0]}})
            print(f"{item[k.ID]}: {item[k.URLS]} --> {item[k.URLS][0]}")
            return()
    if k.DONTINDEX in itemkeys:
        counter.errors+=1
        print(f"{item[k.ID]}: {item[k.DONTINDEX]}")
        return()
    
    if counter.limit and counter.all >= counter.limit:
        return()
    counter.all+=1
    
    # datesPublished
    if k.DATESPUBLISHED in itemkeys:
        i.update({k.DATESPUBLISHED: item[k.DATESPUBLISHED]})
    else:
        if args.verbose: print(f"#ERROR: {item[k.ID]} missing field {key}")
        counter.errors+=1
        return()

    # url fields
    miss=True
    url=""
    for key in [ 'URLs', 'url' ]:
        if key in itemkeys:
            if isinstance(item[key],list):
                url=item[key][0]
            else:
                url=item[key]
            i.update({k.URLS:[url]})
            miss=False
            break
    if miss:
        if args.verbose: print(f"#ERROR: {item[k.ID]} missing fields URLs or url")
        if args.debug: debug(item)
        if not args.dryrun: markDontindex(item,"missing url")
        counter.errors+=1            
        return()
    
    # text fields
    miss=True
    for key in [ 'body', 'text' ]:
        if key in itemkeys:
            i.update({
                k.CLEANTEXT:cleanText(item[key]).replace("LEAD: ",""),
            })
            miss=False
            break
    if miss:
        if args.verbose: print(f"#ERROR: {item[k.ID]} missing fields body or text")
        if args.debug: debug(item)
        if not args.dryrun: markDontindex(item,"missing: body/text")
        return()
    
    # process metatdata
    i.update({
        k.UUID: str(uuid.uuid4()),
        k.UPDATEDTS: datetime.datetime.utcnow() ,
    })
        

    # other values may come from many fields

    for destkey in codetrans.keys():
        miss=True
        for srckey in codetrans[destkey]:
            if srckey in itemkeys:
                i.update({destkey: item[srckey]})
                miss=False
                break
        if miss:
            if args.verbose: print(f"#WARN: {item[k.ID]} no {destkey}: item is missing {list(codetrans[destkey])}")
        

    # address missing year
    if k.YEAR not in i.keys():
        if "publication_date" in itemkeys:
            i.update({k.YEAR: int(item["publication_date"][0:4])})
                     
    # feeds - sources
    sset=item["sourceSet"]
    src=[]
    separators={"online_section": ";",
                "sources": "/",
                "taxonomic_classifiers": "/",
                "news_desk": "",
                "feature_page": "",
                "descriptors" : "",
                "page_number": "",
                }
    found=False
    for key in (set(separators.keys()) & set(list(itemkeys))):
        if isinstance(item[key], list):
            for s in item[key]:
                s=normalizeFeed(s,separators[key])
                src.append(f"archive|{sset}|{s}")
                found=True
        elif isinstance(item[key], str):
            if separators[key] != "":
                for s in item[key].split(separators[key]):
                    s=normalizeFeed(s,separators[key])
                    src.append(f"archive|{sset}|{s}")
                    found=True
            else:
                if len(item[key]) >0:
                    s=normalizeFeed(item[key],separators[key])
                    src.append(f"archive|{sset}|{s}")
                    found=True
        if found:
            if args.debug: print(f"#DEB source found in key: {key}")
            break
        
    if len(set(src)) != 0:
        i.update({k.SOURCES:list(set(src))})
    else:
        if args.verbose:print(f"#WARN: {item[k.ID]} missing {separators.keys()}")
        
    # if we have all valid fields, proceed, otherwise mark record as 'hasIssues'
    missing=set([k.CLEANTEXT,k.URLS,k.SOURCES,k.DATESPUBLISHED,k.TITLE,k.YEAR]) - set(list(i.keys()))

    # no essential fields missing
    if len(missing)==0:
        counter.ok+=1
        datepublished=str(i[k.DATESPUBLISHED][0].replace(tzinfo=None))
    
        i.update({
            #        k.DOWNLOADTS: item[k.DOWNLOADTS],
            k.TITLE: i[k.TITLE],
            k.METAHASH: hashlib.md5((str(i[k.SOURCES])+str(url)+str(datepublished)).encode('utf-8')).hexdigest(),
            k.CONTENTHASH: hashlib.md5(i[k.CLEANTEXT].encode('utf-8')).hexdigest(),
            k.NECT:list(set(doNEC(str(i[k.TITLE]),True))),
            k.NECB:list(set(doNEC(str(i[k.CLEANTEXT]),False)))
        })
        if args.verbose : print(f"OK: {i[k.YEAR]} {item[k.ID]} -> {i[k.UUID]}")

        # write to DB
        if not args.dryrun:
            articleDB.update_one( { k.ID: item[k.ID]},
                                  { '$set': i})
            
    # some essential field missing - report & skip
    else:
        counter.errors+=1
        i.update({k.DONTINDEX: "missing: " +str(missing)})
        print(f"#ERROR: {item[k.ID]} missing fields {missing}",end=';')
        if args.debug: debug(item)
    return()




#
# MAIN
#

importlib.reload(sys)

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-D", "--dryrun", action="store_true", help="dont write to DB")
parser.add_argument("-d", "--debug", action="store_true", help="extra output")
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-c", "--collection", dest="collection",  help="mongodb collection")
parser.add_argument("-n", "--nitems",  help="stop after n items")

(args) = parser.parse_args()
if (not args.dbname) : 
    print(sys.argv[0]," database name argument needed") 
    quit()

db=openDB(args.server,args.dbname)

if args.collection:
   articleDB = db[args.collection]
else:
   articleDB = db[k.ARTICLE]   

articleDB.create_index(k.CONTENTHASH)
articleDB.create_index(k.UUID)    

#query={k.CONTENTHASH: {'$exists': False}}
query={}
all = articleDB.find(query,no_cursor_timeout=False,batch_size=10000)


# supplementary timezone formats for date detection
tzinfos = {
    "CEST":gettz("Europe/Paris"), 
    "BRST": -7200, 
    "CST": gettz("America/Chicago")
}

codetrans={
    k.YEAR: ["year", "publication_year"],
    k.TITLE: ["title", "nyt_id"],
    k.AUTHORS: ["normalized_byline", "byline", "authors"],
    k.SUMMARY: ["lead_paragraph", "headline"],
    k.INHOMEPAGE: ['inHomePage'],
}

counter=Counter()

if args.nitems:
    counter.limit=int(args.nitems)
    
for item in all:
    processItem(item)

counter.display()
