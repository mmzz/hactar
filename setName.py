#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2022 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

'''
loops over entries in Entities DB collections: people, maypeople, non-people and non-entity
moves entries according to certain conditions: has/has not valid firstname or is/is not an institution named after people. 
    - moves entry from non-entities to may-people if name has firstname and is not in nonPeople as entity named after people (institutes)
    - moves entry from maypeople to non-entity if has first name and is not in nonPeople as entity named after people (institutes)
    - moves entry from maypeople to non-people if is not in nonPeople as entity named after people (institutes)
    - moves entry from people to non-people if entry is in nonPeople as entity named after people (institutes)
    - removes entry from maypeople if entry is also in people

'''


import inspect
import sys
from tqdm import tqdm
from namesClass import WDentry, WDquery
from namesClass import debug
import constant as k

def verbose(s):
    import sys
    if args.verbose: print(s, file=sys.stderr)


def remove(item,frm):
    print(f"RM\t{frm}\t\t{item[k.NAME]}")
    try:
        if not args.dryrun:
            rem=entitiesDB[frm].delete_one({k.WID: item[k.WID]})
    except Exception as e:
        debug(f"{e} deleting {item[k.NAME]} from {frm}")


    
def move(item,frm,to):
    '''database movement operations'''
    debug(f"MV\t{frm}\t{to}\t{item[k.NAME]}")
    try:
        if not args.dryrun:
            ins=entitiesDB[to].insert_one(item)
    except Exception as e:
        print(f"{e} inserting {item[k.NAME]} to {to}")
    try:
        if not args.dryrun:
            rem=entitiesDB[frm].delete_one({k.WID: item[k.WID]})
    except Exception as e:
        print(f"{e} deleting {item[k.NAME]} from {frm}")



        
if __name__ == "__main__":
    # MAIN
    from tqdm import tqdm
    from hactar.db import openDB
    import constant as k
    import time
    from namesClass import FirstNames, nonPeopleEntities
    
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose output")
    parser.add_argument(      "--dryrun", action="store_true", help="dont write to db")
    (args) = parser.parse_args()    
    ne=FirstNames()
    nonPeople=nonPeopleEntities()
    entitiesDB=openDB(args.server, 'entities')


    # move name from non-entities to may-people if name has firstname and is not in nonPeople entites named after people (institutes)
    frm=k.NONENTITY
    debug(f"{frm}: firstnames")
    all=entitiesDB[frm].find()
    for item in tqdm(all,total=entitiesDB[frm].count_documents({})):
        # has firstname
        name=item[k.NAME].split(' ')[0]
        if ne.has_firstname(name) and not nonPeople.is_institute(name):
            move(item,frm,k.MAYPEOPLE)


    # move entry from maypeople to non-entity if has first name and is not in nonPeople entites named after people (institutes)
    frm=k.MAYPEOPLE
    debug(f"{frm}: firstnames")
    all=entitiesDB[frm].find()
    for item in tqdm(all,total=entitiesDB[frm].count_documents({})):
        # has not firstname
        try:
            name=item[k.NAME].split(' ')[0]
        except Exception as e:
            print(e,item)
        if not ne.has_firstname(name) and not nonPeople.is_institute(name):
            move(item,frm,k.NONENTITY)
                 
            
    # move name from maypeople to non-people if is not in nonPeople entites named after people (institutes)
    frm=k.MAYPEOPLE
    debug(f"{frm}: institutes")
    all=entitiesDB[frm].find()
    for item in tqdm(all,total=entitiesDB[frm].count_documents({})):
        # has firstname
        name=item[k.NAME]
        if nonPeople.is_institute(name):
            move(item,frm,k.NONPEOPLE)

    # move people to non-people if name is in nonPeople entites named after people (institutes)
    frm=k.PEOPLE
    debug(f"{frm}: institutes")
    all=entitiesDB[frm].find()
    for item in tqdm(all,total=entitiesDB[frm].count_documents({})):
        # has firstname
        name=item[k.NAME]
        if nonPeople.is_institute(name):
            move(item,frm,k.NONPEOPLE)


    # remove entry from maypeople if entry is also in people
    frm=k.MAYPEOPLE
    debug(f"{frm}: duplicates")
    all=entitiesDB[frm].find()
    for item in tqdm(all,total=entitiesDB[frm].count_documents({})):
        name=item[k.NAME]
        d=entitiesDB[k.PEOPLE].find_one({k.NAME:name})
        if d:
            remove(item,frm)
            continue
    # beware of aliases: 
    #        dd=entitiesDB[k.PEOPLE].find_one({k.ALIAS:name})
    #        if dd >0:
    #            print(f"WARN\talias\t{name}\t{dd[k.ALIAS]}")
    #            continue
        

