#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import codecs
import feedparser
import unicodedata
import dateutil
import dateparser 
import http.client
import chardet
import socket
import datetime
import json
import time
#import urltools
import re
import hashlib
import urllib.parse
from time import mktime
from pymongo import MongoClient
#from lxml.html.clean import Cleaner
from argparse import ArgumentParser
#
from hactar.strings import normalize
from hactar.db import storeFeedItem
from hactar.db import logEvent
from hactar.db import openDB
from hactar.net import getFeed
import constant as k



def getFeedLocal(url):
        import feedparser
        # 20230915 mmzz; add http:// to feeds missing it 
        if url[0:4] != 'http':
                if args.debug: print(f'corrected url[0:3] [{url[0:3]}] in {url}')
                url = 'http://'+url
        try:
            f=feedparser.parse(url)
            if f.status==404:
                    logEvent(args.verbose,logDB,"rsscollect","404",source,"",url,"")
                    return(f.status,[],None)
            if f.bozo == 1 :
                if f["entries"]:
                        if args.debug: print(f'feedparser gets {f["entries"]}')
                        return (f.bozo,f["entries"],f.bozo_exception)
                else:
                        logEvent(args.verbose,logDB,"rsscollect","skip_on_err",source,"",url,f.bozo_exception)
                        return (f.bozo,[],f.bozo_exception)
            else:
                return (f.bozo,f["entries"],None)
        except Exception as e:
            return (None,None,e)

# #
# MAIN
# #

socket.setdefaulttimeout(10)

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-u", "--url", dest="url",help="load one specific url")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-d", "--debug", action="store_true", help="debug output")

(args) = parser.parse_args()
    
# if args.url:
#         (bozo,feedItems,exception) = getFeed(args.url)
#         if exception:
#             if args.verbose: print(exception)
#         else:
#             published=None
#             for item in feedItems:
#                 if args.verbose: print("feed:",args.url,"item",item[ "link" ])
#                 print(item)
#                 if (('published' in item) 
#                         and (item["published"] != None) 
#                         and (item["published"] != 0)):
#                    published=dateutil.parse(item['published'])
#                 elif (('published_parsed' in item) 
#                         and (item["published_parsed"] != None) 
#                         and (item["published_parsed"] != 0)):
#                    published=datetime.datetime.fromtimestamp(mktime(item["published_parsed"]))
#                 if (published=="" or published==None):
#                    print("missing_date_in_feed_item")

#                 print(f"parsed: {item['published_parsed']}")              
#                 print(f"published: {item['published']}")              
#                 print(published)

#         quit()


db=openDB(args.server, args.dbname)
feedsourceDB = db[k.FEEDSOURCE]
logDB = db[k.EVENTLOG]
feeditemDB = db[k.FEEDITEM]
feeditemDB.create_index('itemMD5hash',unique=True)

#all = feedsourceDB.find({"active": True},no_cursor_timeout=False)

if args.url:
        all=[
                {'_id':'none',
                 'URL': args.url,
                 'site':'NONE',
                 'name':'NONE'
                }]
else:
        # read feeds collection
        all = feedsourceDB.find({"active": True, "protocol":"rss"},no_cursor_timeout=False)
        nitems = feedsourceDB.count_documents({"active": True, "protocol":"rss"})
        if args.debug: print(f'feedparser got {nitems} items from active rss feeds in db')

# collect data in each feed
for feed in all:
    oid=feed['_id']
    url=feed['URL']
    site=feed['site']
    name=feed['name']
    if 'homePage' in feed and feed['homePage'] == True:
       inHomePage=True
    else:
       inHomePage=False

    source="rss|"+site+"|"+name
    if args.debug: print(f"rsscollect feed: {site}|{name} at {url}")

    try:
        (bozo,feedItems,exception) = getFeedLocal(url)
        if not feedItems:
            if args.debug: print(f'{url} has no items')
            continue
    except Exception as e:
        if args.debug: print(f'{url} yelds exception {e}')
        logEvent(args.verbose,logDB,"rsscollect","skip_on_err",source,"",url,e)
        continue
    for item in feedItems:
        if args.debug: print(item)
        title=link=author=published=summary=""
        if ('title' in item): title = normalize(item["title"])
        if ('link' in item):
            l = item[ "link"]
            (link,frag) = urllib.parse.urldefrag(l) # remove fragments from URL
        else:
            logEvent(args.verbose,logDB,"rsscollect","skip",source,"",url,"missing_link_in_feed_item")
            next
        author=[]
        if ('author' in item): 
            author.append(item["author"].upper())


        # dates: published_parsed or published

        from datetime import datetime
        from time import mktime
        published=None
        
        # supplementary timezone formats for date detection
        tzinfos = {
                "CEST":dateutil.tz.gettz("Europe/Paris"), 
                "BRST": -7200, 
                "CST": dateutil.tz.gettz("America/Chicago")
        }

        for dd in ['published_parsed', 'published']:
                if ((dd in item.keys()) and (item[dd] != None) and (item[dd] != 0)):
                        if isinstance(item[dd], time.struct_time):
                                if item[dd][0] < 1000:
                                        if args.debug:
                                                print(f"warn: year is 0 [{type(item[dd])}]")
                                        break
                                try:
                                        published=datetime.fromtimestamp(mktime(item[dd]))
                                except Exception as e:
                                        if args.debug:
                                                print(f"warn:[{item[dd]}] [{type(item[dd])}] - {e}")
                                if published: break
                        
                        elif isinstance(item[dd], str):
                                try:
                                        published=dateutil.parser.parse(item[dd],  tzinfos=tzinfos,  fuzzy = True)
                                except Exception as e:
                                        if args.debug:
                                                print(f"warn:[{item[dd]}] [{type(item[dd])}] - {e}")
                                if published: break

                                try:
                                        published=dateparser.parse(item[dd],languages=['it','en', 'fr', 'es', 'pt', 'ru'])
                                except Exception as e2:
                                        if args.debug:
                                                print(f"noDate:[{item[dd]}] [{type(item[dd])}] - {e}")
                                if published: break

                        else:
                                published=None
                                logEvent(args.verbose,logDB,"rsscollect","err",source,url,"",f"noDate:[{dd}] is [{item[dd]}] and has wrong type [{type(item[dd])}]")
        if args.debug:
                if published == None:
                        logEvent(args.verbose,logDB,"rsscollect","err",source,url,"",f"noDate: [{dd}]/[{item[dd]}] has no valid date.")

        # Summary
        if ('summary' in item):
            summary = normalize(item["summary"])

	# to avoid fetching again the same item in further runs, calculate 
        # hash based on site, feed, publication date, and link
        itemhash=hashlib.md5((str(published)+link+name+site).encode('utf-8')).hexdigest()
        contenthash=hashlib.md5((str(published)+link+title).encode('utf-8')).hexdigest()
        if not args.url and feeditemDB.count_documents({"itemMD5hash": itemhash}) == 0:
            storeFeedItem(feeditemDB,oid,itemhash,url,site,name,title,link,published,author,summary,inHomePage)
            logEvent(args.verbose,logDB,"rsscollect","ok",source,"",url,"new_item")
