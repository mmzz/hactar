#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2020 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
from pymongo import MongoClient
from argparse import ArgumentParser

import constant as k
from hactar.db import loadRules
from hactar.db import openDB
from hactar.db import logEvent


# #
# Functions
# # 

def morphotag(text,lang):
    from subprocess import Popen, PIPE, STDOUT,TimeoutExpired
    errs=""
    
    command=["TextPro2.0/textpro.sh", "-c token+pos+comp_morpho", "-l "+lang]
    p = Popen(command, stdout=PIPE, stdin=PIPE, stderr=PIPE)
    try:
        outs, errs = p.communicate(timeout=15, input=text.encode())
    except TimeoutExpired:
        p.kill()
        return("","TextPro process timed out")
    if errs:
        return("",errs.decode("utf-8").strip())
    else:
        return(outs.decode("utf-8"),"")


# #
# MAIN
# #

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-Y", "--year", dest="year", help="limit action to one year")
parser.add_argument("--dryrun", action="store_true", help="dont write to db")
parser.add_argument("--overwrite", action="store_true", help="reassign a class to all items in db")
parser.add_argument("-U", "--uuid", dest="uuid", help="limit action to a single item identified by uuid")
      

(args) = parser.parse_args()

db=openDB(args.server,args.dbname)
feedsourceDB = db[k.FEEDSOURCE]
articleDB = db[k.ARTICLE]
logDB = db[k.EVENTLOG]
rulesDB = db[k.RULES]

#articleDB.create_index({k.MORPHOTAG: 'hashing'})

# db.rules.insertOne({"ruleName" : "language", ruleDict: {"morphoLang": "ita"})}
# db.rules.updateOne({"ruleName" : "language"},{ "$set": { ruleDict: {"spacyLang": "it", "morphoLang" : "ita" } } } )
langRules=loadRules(rulesDB, k.LANGUAGE)
if langRules == {}:
    lang="ita"
    if args.dryrun and args.verbose:
        print("warn: no "+k.LANGUAGE+" rule in "+k.RULES+" DB")
    else:
       logEvent(args.verbose,logDB,"morphoTag","warn","","","","skip: no "+k.LANGUAGE+" rule in "+k.RULES+" DB")
else:
    lang=langRules[k.MORPHOLANG]


if args.overwrite:
    search= {k.CLEANTEXT: {'$exists': True}}
else:
    conditions=[
          {k.HASMORPHOTAG: {'$exists': False}},
	  {k.CLEANTEXT: {'$exists': True}}
	  ]
    if args.year:
          conditions.append({k.YEAR: int(args.year)})
    search={'$and': conditions}
if args.uuid:
    search={'uuid': args.uuid}


all = articleDB.find(search,no_cursor_timeout=True).batch_size(1000)
for item in all:
    tags,err=morphotag(item[k.CLEANTEXT],lang)
    if args.dryrun and args.verbose:
        if err:
    	    print(f"ERROR: {item['uuid']} {err}")
        else:
            print(f"OK: {item['uuid']} {tags}")
    else:
        oid=item['_id']
        if err:
           articleDB.update_one( { '_id': oid},
                                 { '$set':{
                                     k.HASMORPHOTAG: False
                                 }})
       	   logEvent(args.verbose,logDB,k.MORPHOTAG,"err","",str(item['uuid']),"","Textpro error:["+err+"]")
        else:
           articleDB.update_one( { '_id': oid},
                                 { '$set':{
                                     k.MORPHOTAG: tags,
                                     k.HASMORPHOTAG: True
                                 }})
           logEvent(args.verbose,logDB,k.MORPHOTAG,"ok","",str(item['uuid']),"",str(tags.count('\n')-3)+" tokens added")

      
all.close()
