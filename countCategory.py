#/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
from pymongo import MongoClient
from argparse import ArgumentParser

import constant as k
from hactar.db import openDB

# #
# MAIN
# #

SEP=","

def printTable(title,a):
    out=f"{title}{SEP}{SEP}"
    for y in years:
        out+=str(y)+SEP
    print(out)

    for c in categ:
        for n in newsp:
            out=f"{c}{SEP}{n}{SEP}"
            for y in years:
                ky=f"{c}-{y}-{n}"
                if ky in a.keys():
                    out+=str(round(a[ky],0))+SEP
                else:
                    out+="0"+SEP
            print(out)
        print("\n")

def countErrs(err,errors):
    if err not in errors.keys():
        errors[err]=1       
    else:
        errors[err]+=1


parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-y", "--year", dest="year", help="limit to year")
parser.add_argument("-q", "--query", dest="query", help="limit to query {key: value}")
parser.add_argument("-w", "--words", action="store_true", help="count words")
parser.add_argument("-e", "--errors", action="store_true", help="count errors and anomalies")
parser.add_argument("-W", "--write", action="store_true", help=f"write to {k.SAMPLECLASS}")
parser.add_argument("-D", "--debug", action="store_true", help="debug output")

parser.add_argument(      "--indexed", action="store_true", help="select indexed documents only")

(args) = parser.parse_args()

db=openDB(args.server,args.dbname)
feedsourceDB = db[k.FEEDSOURCE]
articleDB = db[k.ARTICLE]
logDB = db[k.EVENTLOG]

n=0
if args.words:
    sums={}
    avg={}
    
count={}
if args.errors:
    errors={}

query={}

if args.year:
    year=int(args.year)
    query={k.YEAR: year}

if args.query:
    query={args.query}


if args.debug: print(f"#D: find {query}")
all = articleDB.find(query,no_cursor_timeout=False,batch_size=10000)

keys = (k.CLEANTEXTWORDS,k.CHOSENCATEGORY,k.SOURCES,k.YEAR)
for item in all:
    if not set(keys).issubset(item.keys()):
        if args.errors:
            kerr=f"NONE-NONE-NONE"
            countErrs(kerr,errors)
        if args.verbose: print (f"#ERR\t{item[k.UUID]}\t--\tmissing {set(keys)-item.keys()} in {keys}")
        continue
    (ch,news,feed)=item[k.SOURCES][0].split("|",2)
    year=item[k.YEAR]
    
    if item[k.CLEANTEXTWORDS] < 100 :
        if args.errors:
            kerr=f"short-{year}-{news}"
            countErrs(kerr,errors)
        if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\tshort {item[k.CLEANTEXTWORDS]}")
        continue
    if item[k.SOURCES] == []:
        if args.errors:
            kerr=f"noSources-{year}-{news}"
            countErrs(kerr,errors)        
        if args.verbose: print (f"#ERR\t{item[k.UUID]}\t{item[k.YEAR]}\tempty {k.SOURCES}")
        continue
    if args.indexed:
        if k.NEEDSATTENTION in item.keys():
            if args.errors:
                kerr=f"attention-{year}-{news}"
                countErrs(kerr,errors)        
            if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\t{k.NEEDSATTENTION}: {item[k.NEEDSATTENTION]} ")
            continue
        if k.DONTINDEX in item.keys():
            if args.errors:
                kerr=f"dontIndex-{year}-{news}"
                countErrs(kerr,errors)
            if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\t{k.DONTINDEX}: {item[k.DONTINDEX]} ")
            continue
        if k.ASSESSMENTS not in item.keys():
            if args.errors:
                kerr=f"noAssessment-{year}-{news}"
                countErrs(kerr,errors)
            if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\t{k.ASSESSMENTS} missing")
            continue
        if k.STACKEDST not in item[k.ASSESSMENTS].keys():
            if args.errors:
                kerr=f"noStackedST-{year}-{news}"
                countErrs(kerr,errors)
            if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\t{k.STACKEDST} missing")
            continue
    if k.CLEANTEXTISSUES in item.keys() and item[k.CLEANTEXTISSUES]!=[k.DELREPEATISSUE] and item[k.CLEANTEXTISSUES]!=[]:
        if args.errors:
            kerr=f"issues-{year}-{news}"
            countErrs(kerr,errors)
        if args.debug: print (f"#D\t{item[k.UUID]}\t{item[k.YEAR]}\tissues: {item[k.CLEANTEXTISSUES]}")
        continue
    else:
        cat=item[k.CHOSENCATEGORY]
        try:
            (ch,news,feed)=item[k.SOURCES][0].split("|",2)
        except Exception as e:
            if args.verbose: print (f"#ERR\t{item[k.UUID]}\t{item[k.YEAR]}\t{item[k.SOURCES][0]}: {e}")
        year=item[k.YEAR]
        words=item[k.CLEANTEXTWORDS]

        n+=1
        ky=f"{cat}-{year}-{news}"
        if args.verbose: print(f"OK\t{item[k.UUID]}\t\t{ky}")
        if args.write:
            articleDB.update_one( { '_id': item['_id']}, {
                '$set': {
                    k.SAMPLECLASS: ky,
                }
            }, upsert=True)

        if ky not in count.keys():
            count[ky]=1
            if args.words:
                avg[ky]=sums[ky]
                sums[ky]=words
        else:
            count[ky]+=1
            if args.words:
                sums[ky]+=words
                avg[ky]=sums[ky]/count[ky]
            

years=[]
categ=[]
newsp=[]
for ky in count.keys():
    (c,y,n) = ky.split("-")
    if y not in years:
        years.append(y)
    if c not in categ:
        categ.append(c)
    if n not in newsp:
        newsp.append(n)
years.sort()
newsp.sort()
categ.sort()

printTable("Articles",count)
if args.words:
    printTable("WordsNumber",sums)
if args.words:
    printTable("WordsPerArticle",avg)

if args.errors:
    years=[]
    errtype=[]
    newsp=[]

    for ky in errors.keys():
        (c,y,n) = ky.split("-")
        if y not in years:
            years.append(y)
        if c not in errtype:
            errtype.append(c)
        if n not in newsp:
            newsp.append(n)

    years.sort()
    newsp.sort()
    categ.sort()

    printTable("Errors", errors)
