#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import codecs
import bson
import bz2
import re
import validators
import hashlib
import lxml
import unicodedata
import datetime
import http.client
import chardet

from scrapy.selector import Selector
from bson import Binary
from pymongo import MongoClient
#from lxml.html.clean import Cleaner
from lxml import etree, html
from argparse import ArgumentParser

from hactar.net import getArticle
from hactar.db import logEvent
from hactar.db import markFeedActivity
from hactar.db import openDB
from hactar.strings import cleanText
from hactar.process import processArticle
import constant as k

import importlib

#
# MAIN
#

importlib.reload(sys)

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-d", "--debug", action="store_true", help="extra output")
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-S", "--omitSource", action="store_true", help="do not archive source html")

(args) = parser.parse_args()
if (not args.dbname) : 
    print(sys.argv[0], end=' ') 
    ": database name argument needed" 
    quit()

#dbname=args.dbname
#if args.server:
#    server=args.server
#else:
#    server="localhost:27017"
    
#client = MongoClient(server)
#db = client[dbname]

db=openDB(args.server,args.dbname)

articleDB = db[k.ARTICLE]
feeditemDB = db[k.FEEDITEM]
feedsourceDB = db[k.FEEDSOURCE]
logDB=db[k.EVENTLOG]

if args.omitSource:
    archiveDB = None
else:
    now = datetime.datetime.now()
    archiveDB = db[k.HTML+str(now.year)]    

articleDB.create_index('contentHash')
articleDB.create_index('metadataHash')    
all = feeditemDB.find({'collected': False},no_cursor_timeout=False)

for feeditem in all:
    link=feeditem['itemLink']
    oid=feeditem['_id']
    source= feeditem['channel']+'|'+feeditem['feedSite']+'|'+feeditem['feedName']
    
    # download article from link URL
    # and mark feeditem read
    feeditemDB.update_one({'_id': oid},{'$set': {'collected':True}})
    try:
        art=getArticle(link)
    except Exception as e:
        m=re.match(r'^Article.*failed with(.*)Client Error:(.*) for url:.*$',str(e))
        if m:
            err=str(m.group(1))+str(m.group(2))
        else:
            err=str(e)

        logEvent(args.verbose,logDB,"htmlcollect","skip_on_err",source,"",link,err)
        feeditemDB.update_one({'_id': oid},{'$set': {'error':err}})
        continue

    if art:
        meta=({
            'published': feeditem['itemPublished'],
            'title': feeditem['itemTitle'],
            'source': source,
            'author':feeditem['itemAuthor'],
            'summary': feeditem['itemSummary'],
	    'inHomePage': feeditem['inHomePage']
        })
        
        ok=processArticle(meta,art,articleDB,archiveDB,logDB,db,args.verbose)

        # mark last activity in feedsource for health-checking purposes

        if ok:
            markFeedActivity(feedsourceDB,feeditem['channel'],feeditem['feedSite'],feeditem['feedName'],True)
        else:
            markFeedActivity(feedsourceDB,feeditem['channel'],feeditem['feedSite'],feeditem['feedName'],False)

    else:
        logEvent(args.verbose,logDB,"htmlcollect","warn",source,"",art.url,"download_ok_but_no_article")
        continue
    
    if args.debug:
        if (ok):
            logEvent(args.verbose,logDB,"htmlcollect","ok",source,"",art.url,"article_ok")
        else :
            logEvent(args.verbose,logDB,"htmlcollect","warn",source,"",art.url,"article_discarded")
            
        print("\nITEM: ",count,link)
        if title: print("TTL: ",title.encode('utf-8'))
        if author: print("AUT: ", author)
        print("DATE: ",published)
        print("UUID:",artuuid)
            


