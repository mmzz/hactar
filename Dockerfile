FROM python:3.7

ENV MONGO="10.0.0.1:27017"
ENV STANFORD="10.0.0.1:9000"
ENV DATABASE="newsTest"
ENV STANFORDTIMEOUT="15m"

RUN apt-get install libbz2-dev
RUN git clone https://gitlab.com/mmzz/hactar

ENV PYTHONPATH="${PYTHONPATH}:/hactar/hactar"

WORKDIR /hactar
#RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN python3 -m spacy download it_core_news_sm 
RUN python3 -m spacy download en_core_web_sm
RUN python3 -m spacy download fr_core_news_sm
RUN python3 -m spacy download es_core_news_sm
RUN python3 -m spacy download pt_core_news_sm
RUN python3 -m spacy download de_core_news_sm

CMD /hactar/START


