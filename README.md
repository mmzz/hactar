
Overview
========

Hactar fetches and scrapes article pages from online newspapers and stores articles text (and optionally html source) to a mongodb archive.
Additionally:
- collects article metatadata if available (publishing date, author) 
- deduplicates aricles from same source
- performs named entity recognition [NER] on body and title

Setup
-----

1. start MongoDB server (defaults to 127.0.0.1:27017) or docker container
2. [optional for NER] start Stanford Core NLP server (defaults to 127.0.0.1:9000) or docker container
3. prepare Python3 environment (3.7) with the following libraries
	* argparse
	* bson
	* bz2
	* chardet
	* codecs
	* fuzzywuzzy
	* hashlib
	* http
	* json
	* lxml
	* newspaper3k
	* pymongo
	* scrapy
	* stanfordcorenlp
	* unicodedata
	* urllib.parse
	* urltools
	* uuid
	* validators
4. download modules
5. prepare a single source file  for newspaper sources, one line per source, ab separated
```
	NEWSPAPER	feedName	isHomepage[true|false] http|rss	link (html or rss)
```
for instance:
```
	THEGUARDIAN	HomePage	true		       rss	https://www.theguardian.com/international/rss

```
6. populate newsFeed sources collection. Each module requires a database name as an argument (dbname). For instance newsTest.
```bash
	python3 populatefeeds.py3 newsTest < sources
```
6. train crawler
```bash
	python crawl.py3 -t newsTest
```
7. start regular collection loop 
```bash
	python3 rsscollect.py3  newsTest
	python3 htmlcollect.py3 newsTest
	python3 crawl.py3 newsTest
	python3 ner.py3 newsTest
	python3 feedhealth.py3 newsTest
```
Modules and source/destination mongoDB collections 
---------------------------------------------------


|*Channel*	|*modules*	|*source collection*	|*destination collection*|
|---------------|---------------|-----------------------|------------------------|
|RSS+HTML	|populatefeeds	|-			|feedsource		 |
|RSS		|rsscollect 	|feedsource		|feeditem		 |
|RSS		|htmlcollect	|feeditem		|article [+archive]	 |
|HTTP		|crawl 		|feedsource		|article [+archive]	 |

All modules log into collection *eventlog*

