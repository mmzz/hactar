#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2022 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import inspect
import sys
import os
from tqdm import tqdm
import constant as k
import pycld2
from hactar.db import logEvent

import warnings
warnings.filterwarnings("ignore")

def debug(s):
    import sys
    if args.debug: print(s, file=sys.stderr)

def verbose(s):
    if args.verbose: print(s)

class logFile(object):
    def __init__(self,file_name):
        self.file_name = file_name
        self.handle=open(self.file_name,'w') 

    def write(self,s):
        self.handle.write(s)

    def close(self):
        self.handle.close()

class Article:
    def detect_cld2(self,s):
        import pycld2
        try:
            isReliable, textBytesFound, details = pycld2.detect(s)
            lang=details[0][1]
            prob= details[0][2]
        except Exception as e:
            verbose(f"{e} detecting language (pycld2) in {self.uuid} {self.text}")
            isReliable=False
            prob=0
            lang='??'

        return(isReliable,lang,prob)

    def detect_langdetect(self,s):
        from langdetect import detect_langs
        try:
            res= detect_langs(s)
        except Exception as e:
            verbose(f"{e} detecting (langdetect) language in {self.uuid} {self.text}")
            return(False,'??',0)
        if len(res)==1:
            reliable=True
        else:
            reliable=False
        return(reliable,res[0].lang,int(res[0].prob*100))

    def __init__(self,item):
        self.uuid=item[k.UUID]
        self.language='??'

        if k.CLEANTEXT not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnoText")
        if len(item[k.CLEANTEXT] ) < 100:
            self.valid=False
            debug(f"{item[k.UUID]}\tshortText")
        else:
            self.valid=True
        
            self.year=item[k.YEAR]
            self.text=item[k.CLEANTEXT]
            self.uuid=item[k.UUID]
            
            # fills self.language with 2-letter language code (en, it, es, ...) if both language detection libraries
            # agree with a high reliability.
            # Otherwise, checks if one of the libraries gives a language that matches the language of dbname (eg. newsEN --> en)
            # if not, adds '?' to langdetect result (eg en?, cn?).
            #
            if k.LANGUAGE in item.keys() and not args.overwrite:
                self.language=item[k.LANGUAGE]
            else:
                reliable1,lang1,prob1=self.detect_langdetect(self.text)
                reliable2,lang2,prob2=self.detect_cld2(self.text)
                    
                if reliable1==reliable2 and lang1==lang2 and (prob2 > 90 and prob1 > 90):
                    self.language=lang1
                    if args.debug: certain.write(f"{self.uuid}\t{self.language}\t\t{self.text[10:100]}\n")
                else:
                    dblang = args.dbname[4:6].lower()    # database language: newsIT --> it, newsES --> es, ...
                    if (lang1 == dblang or lang2 == dblang):
                        self.language = dblang
                    else:
                        self.language = lang1+'?'
                    if args.debug: uncertain.write(f"{self.uuid}\t{self.language}\t{reliable1}|{reliable2}\t{lang1}|{lang2}\t{prob1}|{prob2}\t{self.text[10:100]}\n")
                                            


# MAIN
if __name__ == "__main__":    

    from hactar.db import openDB
    import constant as k
    from argparse import ArgumentParser
    import uuid


    THISMODULE="setLanguage"

    parser = ArgumentParser()
    parser.add_argument(      "dbname", type=str,       help='database name')
    parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose std output")
    parser.add_argument(      "--debug", action="store_true", help="extra std err")
    parser.add_argument(      "--dryrun", action="store_true", help="dont write to db")
    parser.add_argument(      "--overwrite", action="store_true", help="overwrite to db")
    parser.add_argument(      "--nitems", type=int, help="max items")
    parser.add_argument("-D", "--disable_progress_bar", action="store_true")
    parser.add_argument(      "--uuids", help="take input from file of uuids rather than db query")
    parser.add_argument("-Y", "--year"  , help="query: year")
    (args) = parser.parse_args()
       
    articles=openDB(args.server, args.dbname)[k.ARTICLE]
    logDB=openDB(args.server, args.dbname)[k.EVENTLOG]
    verbose(f'{args.dbname} indexing key {k.LANGUAGE}')
    articles.create_index(k.LANGUAGE)

    if args.debug: uncertain=logFile(f'{args.dbname}-{args.year}-uncertain.log')
    if args.debug: certain=logFile(f'{args.dbname}-{args.year}-certain.log')

    if args.uuids:
        all=[]
        verbose(f"getting uuids from {args.uuids}")
        with open(args.uuids, 'r') as f:
            for line in f:
                if line[0] == '#':
                    continue
                item=articles.find_one({k.UUID: line.strip()},no_cursor_timeout=True)
                if item != None:
                    all.append(item)
                    verbose(f"added {item[k.UUID]}")
        total=len(all)
        verbose(f"read {total} items")
    elif args.year:
        query={k.YEAR: int(args.year)}
        verbose(f"querying {query}")
        all=articles.find(query,no_cursor_timeout=True).batch_size(1000)
        if args.nitems:
            total=int(args.nitems)
        else:
            total=articles.count_documents(query)
    else:
        query={k.LANGUAGE: {'$exists' : False}}
        verbose(f"querying {query}")
        all=articles.find(query,no_cursor_timeout=True).batch_size(1000)
        total=articles.count_documents(query)
       
    # main loop
    nitem=0
    for item in tqdm(all, total=total, disable=args.disable_progress_bar):

        if args.nitems and nitem>=args.nitems:
            break
        else:
            nitem+=1

        article=Article(item)

        # write to db
        if not args.dryrun:
            try:
                articles.update_one({k.UUID: article.uuid},
                                    {'$set': {k.LANGUAGE: article.language}})
                logEvent(args.verbose,logDB,THISMODULE,"ok","",article.uuid,"",article.language)
            except Exception as e:
                debug(f"ERROR [{e}] writing {article.uuid}/{k.LANGUAGE}:{article.language} to {articles} ")
        
    if args.debug: uncertain.close()
    if args.debug: certain.close()
