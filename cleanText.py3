#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
           

import sys
import getopt
import codecs
import bson
import bz2
import os
import datetime
import re
import validators
from bson import Binary
from pymongo import MongoClient, HASHED
from argparse import ArgumentParser
from random import *
import dateparser
from bson.objectid import ObjectId
import unicodedata
import codepoints
import array
from hactar.strings import removeRE
from hactar.strings import removeStrings
from hactar.strings import cleanPrintable
from hactar.strings import cleanLineSep
from hactar.strings import convertCodePoints
from hactar.strings import replaceRE
from hactar.strings import fix_lowvar_noLcase

from hactar.db import logEvent
from hactar.db import openDB
import constant as k


import warnings
warnings.filterwarnings("ignore")

parser = ArgumentParser()
parser.add_argument(      "dbname",    type=str,            help='database name')
parser.add_argument("-s", "--server",  dest="server",       help="mongodb server address:port")
parser.add_argument("-C", "--count", action="store_true",   help="count items before processing (could take time)")
parser.add_argument("-D", "--dryrun", action="store_true",  help="do not write to DB")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("--revision",   dest="revision", type=int,  help="overwrite items with "+k.CLEANTEXTREVISION+" lower than given argument")
parser.add_argument("--index", action="store_true", help="force build indexing")
parser.add_argument("--overwrite", action="store_true",     help="overwrite "+k.CLEANTEXT+" for *all* items in db. WARNING: clears all other conditions")
parser.add_argument("-U", "--uuid", dest="uuid",            help="limit search to a single item identified by uuid")
parser.add_argument("-Y", "--year", dest="year",            help="limit search to one year")

(args) = parser.parse_args()

# CURRENT REVISION OF THIS MODULE
CURRENTREVISION=2
THISMODULE="cleanText"

# init DB
db=openDB(args.server,args.dbname)
articleDB = db[k.ARTICLE]
logDB = db[k.EVENTLOG]
rulesDB=db[k.RULES]


# indexing
if args.index:
    if args.verbose:
        logEvent(args.verbose,logDB,THISMODULE,"ok","","","","indexing")
    try:
        #articleDB.create_index([(k.CLEANTEXT, HASHED)],background=True)
        articleDB.create_index(k.CLEANTEXTREVISION)
    except Exception as e:
        logEvent(args.verbose,logDB,THISMODULE,"warn","","","","Indexing warning message: "+str(e))

# select items in corpus
conditions=[]
if args.year:
    conditions.append({k.YEAR: int(args.year)})
if args.uuid:
    conditions.append({k.UUID: args.uuid})
if args.revision:
    conditions.append({k.CLEANTEXTREVISION: {"$lt": args.revision}})
else:
    conditions.append({k.CLEANTEXTREVISION: {k.EXISTS: False}})

if conditions != []:
    search={k.AND: conditions}
else:
    search={k.CLEANTEXTREVISION: {k.EXISTS: False}}

    
if args.overwrite:
        search={}

logEvent(args.verbose,logDB,THISMODULE,"ok","","","","find "+str(search))

#
# MAIN LOOP
#
all = articleDB.find(search,no_cursor_timeout=True).batch_size(1000)

if args.count:
    print("counting items...")
    count = articleDB.count_documents(search)
    print(f"{count} items found")
    exit

for item in all:
    if not (k.UUID in item.keys()):
        logEvent(args.verbose,logDB,"setCategory","warn","",str(item['_id']),"","missing key:"+k.UUID)
        continue
    if not (k.TEXT in item.keys()):
        logEvent(args.verbose,logDB,"setCategory","warn","",str(item[k.UUID]),"","missing key:"+k.TEXT)
        continue
    text=item[k.TEXT]
    t=convertCodePoints(text)
    if t == False:
        logEvent(args.verbose,logDB,"setCategory","err","",str(item[k.UUID]),"","codePointConversionError:"+k.TEXT)
    else:
        text=t
    text=cleanPrintable(text)
    text=cleanLineSep(text)
    text=removeStrings(text)
    text=replaceRE(text,r'(\s)+',' ')  # squash duplicate whitespaces
    (text,measures,issues)=fix_lowvar_noLcase(text,k)

    words=len(re.findall(r'\w+', text))

    
    logEvent(args.verbose,logDB,THISMODULE,"ok","",item[k.UUID],"","new_item")
    set={
        k.CLEANTEXTISSUES:issues,
        k.CLEANTEXTWORDS:words,
        k.CLEANTEXTREVISION: CURRENTREVISION,
        k.CLEANTEXTMEASURES: measures,
        k.CLEANTEXT: text,
    }
    if not args.dryrun:
       articleDB.update_one({'_id': item['_id']}, 
            { '$set': set})
    else:
        print(f"{item[k.UUID]},{set}")

