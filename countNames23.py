#/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2021 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import os
from datetime import date
from datetime import datetime
from datetime import timedelta
import json
import re
import time
#import urltools
#import urllib.parse
from pymongo import MongoClient
from argparse import ArgumentParser
import odswriter as ods
from tqdm import tqdm

import constant as k
from hactar.db import loadRules
from hactar.db import openDB
from hactar.db import logEvent


# read entities file:
# RECORD:
# full name, short name, aliases, supplemnt, complement
# FIELDS:
# full name
# CONSTANTS:
COVIDS=["sars-cov-2","covid19","sars-cov2","pandemia","covid","corona virus","coronavirus"] # terms related to Covid
RANGE=50 # search range for AND-list (supplements) and NOT-list (complements) in proximity of name 

# terms related to places
CLEANLIST=[" il", " la", " del", " della", " nel", " al", " alla", " nella", " nell'", " dell'", " all'", " viale", "via", "ospedale", "struttura", "palazzo", "via", "piazza", "piazzale", " corso", "ponte", "lungolago", "lungomare", "zona", "quartiere", "largo", "piazzetta", "vicolo" , "aeroporto", "biblioteca", "scuola", "liceo scientifico", "plesso", "liceo", "istituto","dell'istituto","All’istituto","All'istituto", "comprensivo", "alberghiera", "premio", "award"] 



def debug(*s):
    import sys
    import inspect
    if __debug__ :
        frame=sys._getframe(1).f_code.co_name
        stack=inspect.stack()
        if "self" in stack[1][0].f_locals.keys():
            classname=stack[1][0].f_locals["self"].__class__.__name__
        else:
            classname='__main__'

        print(f"#{classname}.{frame}"+" ".join(str(elem) for elem in s))


        
def load_list(fileName):
    ''' reads a list of items from file, one per line 
        comments excluded '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            lst.append(line.strip())
    return(list(set(lst)))


def load(filename):
    supplement=[]

    if args.supplement:
        with open(args.supplement) as supplementFile:
            for line in supplementFile:
                if line[0] != '#':
                    line=line.strip()
                    supplement.append(line)
    with open(filename) as entitiesFile:
        for line in entitiesFile:
               if line[0] == '#':
                   continue
               line=line.strip()
               if args.persons:
                   try:
                       (fullName,shortName,sciclass,alias,s,c)=line.rsplit(SEP,5)
                   except Exception as e:
                       debug (f"{e} with {line}")
                       exit()
                   complement=c.split(SUBSEP)
                   supplement=s.split(SUBSEP)

               else:
                   try:
                       (fullName,aliases)=line.rsplit(SEP,2)
                   except Exception as e:
                       print(f"ERROR [{e}] in [{line}]")
                   alias=aliases.split(SUBSEP)
                   complement=[]
                   supplement=[]
                   shortName=alias[0]
                   

               allEntities.update({
                   fullName: {
                       SHORTNAME: shortName,
                       ALIAS: alias,
                       SUPPLEMENT:supplement,
                       COMPLEMENT:complement,
                       # ALL
                       COUNT: int(0),
                       SCORESUM: float(0.0),
                       AVERAGE: float(0.0),
                       COVID:int(0),
                       # RELEVANT
                       COUNT_R: int(0),
                       SCORESUM_R: float(0.0),
                       AVERAGE_R: float(0.0),
                       COVID_R:int(0),
                       # NON_RELEVANT
                       COUNT_NR: int(0),
                       SCORESUM_NR: float(0.0),
                       AVERAGE_NR: float(0.0),
                       COVID_NR:int(0),
                       # 
#                       SCORES:[],
                       NEWSPAPERS:{},
                   }})
    return(allEntities)

def a(n):
    # Hypertextual search name
    #SEARCH='https://duckduckgo.com/'
    #return(f"=HYPERLINK(\"{SEARCH}?q={n.replace(' ','+')}\",\"{n}\")")
    return(n)

# search mystring in text,
# if match return substring with r chars to the right and l chars to the left of the match
# else return ""
def search(mystring,text,rng):
    l=rng
    r=rng
    t=text.lower()
    found=t.find(mystring.lower())

    if found != -1:
        left=len(text[0:found])
        right=len(text[found+len(mystring):-1])
        if left < l: l = left
        if right < r: r= right
        t=text[found-l:found+r+len(mystring)]
        if len(t)<len(mystring):
            print(f"ERR SEARCH:{mystring},{t}/{text}")
        return(t)
    else:
        return("")

# clecks if entity is immediately preceded by words in deny-list
def precededBy(text,entity,wlist):
    for word in wlist:
#        if text.lower().find(" "+word.lower()+" "+entity.lower()) != -1:
        if text.lower().find(word.lower()+" "+entity.lower()) != -1:
            return word
    return False

    
# Check if words in andlist are present in a given range of string
def hasTerm(text,string,terms):
    # get substring in the surreoundings [-RANGE,+RANGE] of text  
    subtext=search(string,text,RANGE)
    if subtext != "":
        # look for each term in list in substring
        for t in terms:
            if subtext.find(" "+t) != -1:
#            if subtext.find(" "+t+" ") != -1:
                return(t)
    return("")

# return true if matches one in list of strings 
def isCovidPositive(text,necb):
    positive=False
    for c in COVIDS:
        if c in necb:
            positive=True
            break
        else:
            t=text.lower()
            if t.find(c) != -1:
                positive=True
                break
    return(positive)

# update counters list for entity e as a dictionnary 
def countScore(e,score,assessment,covidPositive,mypaper):
    if not isinstance(score, float):
        return()
    
    counters[ALL]+=1
    allEntities[e][RELEVANT]=assessment
    allEntities[e][SCORESUM]+=score
    allEntities[e][COUNT]+=1

    #assessment
    if assessment==RELEVANT:
        counters[RELEVANT]+=1
        allEntities[e][SCORESUM_R]+=score
        allEntities[e][COUNT_R]+=1
        if covidPositive:
            counters[COVIDPOSITIVE]+=1
            allEntities[e][COVID_R]+=1
            allEntities[e][COVID]+=1
    elif assessment==NOTRELEVANT:
        counters[NOTRELEVANT]+=1
        allEntities[e][SCORESUM_NR]+=score
        allEntities[e][COUNT_NR]+=1
        if covidPositive:
            counters[COVIDPOSITIVE]+=1
            allEntities[e][COVID_NR]+=1
            allEntities[e][COVID]+=1
    else:
        print(f"ERROR{e} assessment:{assessment} score:{score} covid:{covidPositive}")

    #newspapers: count, for each name and newspaper, total occurrences of name, occurrences in relevant aritcles, in covid articles and both covid and relevant aticles
    if mypaper in newspapers.keys():

        if e not in newspapers[mypaper].keys():
            newspapers[mypaper].update({
                e: {
                    COUNT:1,
                    RELEVANT:0,
                    COVIDPOSITIVE:0,
                    COVID_R:0}})
        else:
            newspapers[mypaper][e][COUNT]+=1
        if assessment==RELEVANT:
            newspapers[mypaper][e][RELEVANT]+=1
        if covidPositive:
            newspapers[mypaper][e][COVIDPOSITIVE]+=1
        if assessment==RELEVANT and covidPositive:
            newspapers[mypaper][e][COVID_R]+=1
        
    else:
        print("ERR: newspapers missing key",e,mypaper,item[k.SOURCES])



# #
# MAIN
# #

# LABELS
ALIAS="alias"
ALL="all"
AVERAGE="average"
AVERAGE_NR="average_notrelevant"
AVERAGE_R="average_relevant"
COMPLEMENT="complement"
CORPUS="corpus"
COUNT="count"
COUNT_NR="count_notrelevant"
COUNT_R="count_relevant"
COVIDPOSITIVE="covidPositive"
COVIDNEGATIVE="covidNegative"
COVID="covid"
COVID_NR="covid_nr"
COVID_R="covid_r"
FULLNAME="fullName"
NEWSPAPERS="newspapers"
#SCORES="scores"
SCORESUM="scoresum"
SCORESUM_NR="scoresum_notrelevant"
SCORESUM_R="scoresum_relevant"
SHORTNAME="shortName"
SUPPLEMENT="supplement"
# database labels
NOTRELEVANT=k.NOTRELEVANT
RELEVANT=k.RELEVANT


parser = ArgumentParser()
parser.add_argument("dbname",          type=str,help='database name')
parser.add_argument("-s", "--server",  dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("--newspapers",    action="store_true", help="include newspapers count")
parser.add_argument("-l", "--log",     action="store_true", help="log operations to filename.log (filename from -o -p and --ods switches)")
parser.add_argument("-vv", "--veryverbose", action="store_true", help="accepts and rejects in .csv file")
parser.add_argument("-D", "--debug",     action="store_true", help="log anomalies to stdout")
parser.add_argument("-S", "--search",  type=str, help="mongo query dict")
parser.add_argument("-p", "--persons", type=str, help=f"persons file: {FULLNAME}, {SHORTNAME}, {ALIAS}, {SUPPLEMENT}, {COMPLEMENT}")
parser.add_argument("--ods",           action="store_true", help="output to ods file (libreoffice spreadsheet)")
parser.add_argument("--supplement",    type=str, help=f"supplements file (override {SUPPLEMENT} for persons")
parser.add_argument("-N", "--nitems",  type=int, help=f"limit to first N items")
parser.add_argument("-f", "--fromdate",type=str, help=f"period start date Y-m-d")
parser.add_argument("-t", "--todate",  type=str, help=f"period end date Y-m-d")
parser.add_argument("-Y", "--year",  type=str, help=f"teriod is single year")
parser.add_argument("--uuid",  type=str, help=f"take articles fron uuids file")


(args) = parser.parse_args()

db=openDB(args.server,args.dbname)
articleDB = db[k.ARTICLE]


# build log, ods, and csv filename for current query
if args.persons:
    filename=args.persons
    if '/' in filename:
        filename=args.persons.split('/')[-1]
    if '.' in filename:
        filename=filename.split('/')[0]
    SEP=","
    SUBSEP=" "

if args.uuid:
    filename=args.uuid
    if '/' in filename:
        filename=args.uuid.split('/')[-1]
    if '.' in filename:
        filename=filename.split('.')[0]

if args.fromdate and args.todate:
        filename+=f"_{args.fromdate}_{args.todate}"
elif args.year:
        filename+=f"_{args.year}"

if args.log:
    fd = os.open(f"{filename}.log", os.O_CREAT | os.O_WRONLY | os.O_NONBLOCK)
if args.veryverbose:
    dd = os.open(f"{filename}.csv", os.O_CREAT | os.O_WRONLY | os.O_NONBLOCK)


    
newspapers={}
allEntities={}
counters={
    CORPUS:0,
    RELEVANT:0,
    NOTRELEVANT:0,
    COVIDPOSITIVE:0,
    COVIDNEGATIVE:0,
    ALL:0,
}


allEntities=load(args.persons)

entities=set(allEntities.keys())
# find items in db

srch=[]
if args.search:
    srch.append(json.loads(args.search))
elif args.fromdate and args.todate:
    fromdate = datetime.combine(datetime.strptime(args.fromdate, '%Y-%m-%d').date(), datetime.min.time())
    todate   = datetime.combine(datetime.strptime(args.todate, '%Y-%m-%d').date(), datetime.max.time())
    days=  todate-fromdate
    if fromdate.year == todate.year:
        srch.append({k.YEAR: fromdate.year})
    else:
        srch.append({k.YEAR: {'$in': list(range(fromdate.year,todate.year+1,1))}})
    datefield=f"{k.DATESPUBLISHED}.0"
    datefield=f"{k.DOWNLOADTS}"
    srch.append({datefield: {"$gte": fromdate, "$lt": todate}})
elif args.year:
    srch.append({k.YEAR: int(args.year)})
    fromdate = datetime.combine(datetime.strptime(f"{args.year}-1-1", '%Y-%m-%d').date(), datetime.min.time())
    todate   = datetime.combine(datetime.strptime(f"{args.year}-12-31", '%Y-%m-%d').date(), datetime.max.time())
    days=  todate-fromdate
    datefield=f"{k.DATESPUBLISHED}.0"
    datefield=f"{k.DOWNLOADTS}"

## MAIN LOOP
nitems=0
nvalid=0
if args.log: os.write(fd,str.encode(f"#Arguments: {args}\n"))
if args.log: os.write(fd,str.encode(f"#Search:    {srch}\n"))
if args.log: os.write(fd,str.encode(f"#Span:    {days}\n"))


if args.uuid:
    uuids=set(load_list(args.uuid))
    debug(f"read {len(uuids)} uuids from {args.uuid}")
else:
    if len(srch) > 1:
        query={"$and": srch}
    else:
        query=srch[0]
    debug(f" querying {query} ")
    uuids=[]
    all=articleDB.find({"$and": srch},no_cursor_timeout=False,batch_size=10000)
    uuidsfile = os.open(f"{filename}.uuids", os.O_CREAT | os.O_WRONLY | os.O_NONBLOCK)
    total=articleDB.count_documents(query)
    for item in tqdm(all):
        uuids.append(item[k.UUID])
        os.write(uuidsfile,str.encode(f"{item[k.UUID]}\n"))
        #debug(f"read {len(uuids)} uuids from query {query}")
    os.close(uuidsfile)


#if args.log: f.write(f"KEY\tSCORE\tRELEVANT\tCOVID\tTYPE\tENTITY\tTEXT\tSHORT SEARCH KEYS\n")

for uuid in tqdm(uuids):
    item=articleDB.find_one({k.UUID: uuid},no_cursor_timeout=True)
    
    itemHasEntities = False
    # limit run to first args.nitems items.
    nitems+=1
    if args.nitems:
        if nitems > args.nitems:
            break

    # DATESPUBLISHED may have issues
    if datefield == f"{k.DATESPUBLISHED}.0":
        # Scanning one year is faster than relying on mongodb date indexes
        if not(isinstance(item[k.DATESPUBLISHED],list)):
            debug(item[k.UUID],"date_not_list",datefield)
            continue
        if item[k.DATESPUBLISHED] ==[]:
            debug(item[k.UUID],"date_empy_list",datefield)
            continue
        if not isinstance(item[k.DATESPUBLISHED][0],datetime):
            debug(item[k.UUID],"date_not_datetime",datefield)
            continue
        if item[k.DATESPUBLISHED][0] > todate:
            debug(item[k.UUID],"date>",str(item[k.DATESPUBLISHED][0].date()),str(todate))
            continue
        if item[k.DATESPUBLISHED][0] < fromdate:
            debug(item[k.UUID],"date<",str(item[k.DATESPUBLISHED][0].date()),str(fromdate))
            continue

    # skip items with issues or less than 100 words
    if item[k.CLEANTEXTWORDS] < 100 or k.CLEANTEXTISSUES not in item.keys() or item[k.CLEANTEXTISSUES]!=[] :
        debug(item[k.UUID],str(item[datefield].date()),"short")
        continue
    
    if k.DONTINDEX in item.keys():
        debug(item[k.UUID],str(item[datefield].date()),item[k.DONTINDEX])
        continue
        
    if 'scores' not in item.keys():
        debug(item[k.UUID],str(item[datefield].date()),"not_scored")
        continue

    # skip items without ST score
    elif 'stackedST' not in item['scores'].keys():
        score="ST_not_available"
        debug(item[k.UUID],str(item[datefield].date()),score)
        continue
    else:
        score=round(item['scores']['stackedST'],2)

    # skip items not assessed
    if 'assessments' not in item.keys():
        assessment="not_assessed"
        debug(item[k.UUID],str(item[datefield].date()),assessment)
        continue
    elif 'stackedST' not in item['assessments'].keys():
        assessment="ST_not_available"
        debug(item[k.UUID],str(item[datefield].date()),assessment)
        continue
    else:
        assessment=item['assessments']['stackedST']

    nvalid+=1
    # extract newspaper
    (feed,newsPaper,channel)=item[k.SOURCES][0].split("|")
    if newsPaper == "":
        debug(item[k.UUID],str(item[datefield].date()),"no_newspaper")
        continue    
    if newsPaper not in newspapers.keys():
        newspapers.update({newsPaper: {} })

    # check if has covid-related terms in text
    covidPositive=isCovidPositive(item[k.CLEANTEXT],item[k.NECB])

    # log Item as recorded
    summary=f"{item[k.UUID]}\t{item[k.DOWNLOADTS].date()}"
    scores=f"{score}, {assessment}, {covidPositive}, {newsPaper}"

    counters[CORPUS]+=1
    # for each item in DB check each item in list of name entities
    for e in entities:
        andList=allEntities[e][SUPPLEMENT] # AND-list: SHOULD have AT LEAST 1 item in this list to have a match
        notList=allEntities[e][COMPLEMENT] # NOT-list: SHOULD have NO items from this list to have a match
        itemEntities=set(item[k.NER]['body'].keys()) # fetch entities
        
        namelist=[e]
        if allEntities[e][SHORTNAME] != '':  namelist.append(allEntities[e][SHORTNAME])
        if allEntities[e][ALIAS] != ''    :  namelist.append(allEntities[e][ALIAS])

        common = set(namelist) & itemEntities # common elements between names list end item entities

        doCount=""
        for entity in common: # for each entity in common between item named entities and names in file

            # search for entity in article text
            strMatch = search(entity,item[k.CLEANTEXT],RANGE)

            # No match - should not happen
            if strMatch == "":
                if args.veryverbose: os.write(dd,str.encode(f"{summary}\t{scores}\t!\t{entity}\tGONE from text\n"))
                continue

            # CLEANLIST-list match
            cleanMatch = precededBy(strMatch,entity,CLEANLIST)
            if cleanMatch:
                if args.veryverbose: os.write(dd,str.encode(f"{summary}\t{scores}\t❎\t{e}\t{entity} CLEAN {cleanMatch}\t\"{strMatch}\"\r\n"))
                continue

            # NOT-list match
            notMatch = hasTerm(item[k.CLEANTEXT],entity,notList)
            if notList:
                if notMatch:
                    if args.veryverbose: os.write(dd,str.encode(f"{summary}\t{scores}\t❎\t{e}\t{entity} NOT {notMatch}\t\"{strMatch}\"\r\n"))
                    continue

            # AND-list match
            andMatch = hasTerm(item[k.CLEANTEXT],entity,andList)
            if andList:
                if andMatch:
                    doCount=f"{entity} AND {andMatch}"
                    itemHasEntities=True                    
                    if args.veryverbose: os.write(dd,str.encode(f"{summary}\t{scores}\t✔\t{e}\t{doCount}\t\"{strMatch}\"\r\n"))
                    continue
                
            # Plain match, no AND or NOT lists
            if args.veryverbose: os.write(dd,str.encode(f"{summary}\t{scores}\t✔\t{e}\t{entity}\t\"{strMatch}\"\r\n"))
            doCount=entity
            itemHasEntities=True
            continue
        # entities are counted once for each article: avoid to count twice shortnames, aliases and full names
        if doCount:
            countScore(e,score,assessment,covidPositive,newsPaper)
    if args.log: os.write(fd,str.encode(f"{summary}\t{itemHasEntities}\t{scores}\n"))
                            
            
if args.log: os.write(fd,str.encode(f"#END valid/items {nvalid}/{nitems}\n"))

## PRINTOUT in ODS
HEADERS=["Shortname","Full Name", "Covid articles", "n articles",  "Σ articles score", "articles score average"]
if args.ods:
    with ods.writer(open(filename+".ods","wb")) as odsfile:
        sheet_all=odsfile.new_sheet("ALL")
        ## ALL ITEMS
        sheet_all.writerow(HEADERS)
        for e in allEntities.keys():
            if allEntities[e][COUNT]>0:
                scoreaverage=round(allEntities[e][SCORESUM]/allEntities[e][COUNT],3)
            else:
                scoreaverage="-"
            sheet_all.writerow([allEntities[e][SHORTNAME],a(e),allEntities[e][COVID],allEntities[e][COUNT],round(allEntities[e][SCORESUM],3),scoreaverage])
        
        sheet_rel=odsfile.new_sheet(RELEVANT)
        ## RELEVANT
        sheet_rel.writerow(HEADERS)
        for e in allEntities.keys():
            if allEntities[e][COUNT_R]>0:
                scoreaverage_relevant=round(allEntities[e][SCORESUM_R]/allEntities[e][COUNT_R],3)
            else:
                scoreaverage_relevant="-"
            sheet_rel.writerow([allEntities[e][SHORTNAME],a(e),allEntities[e][COVID_R],allEntities[e][COUNT_R],round(allEntities[e][SCORESUM_R],3),scoreaverage_relevant])

        
        sheet_notrel=odsfile.new_sheet(NOTRELEVANT)
        ## NOT RELEVANT
        sheet_notrel.writerow(HEADERS)
        for e in allEntities.keys():
            if allEntities[e][COUNT_NR]>0:
                scoreaverage_not_relevant=round(allEntities[e][SCORESUM_NR]/allEntities[e][COUNT_NR],3)
            else:
                scoreaverage_not_relevant="-"
            sheet_notrel.writerow([allEntities[e][SHORTNAME],a(e),allEntities[e][COVID_NR],allEntities[e][COUNT_NR],round(allEntities[e][SCORESUM_NR],3),scoreaverage_not_relevant])


        if args.newspapers:
            # loop over newspapers
            papers=list(newspapers.keys())
            papers.sort()
            # one sheet for newspaper
            for paper in papers:
                npsheet=odsfile.new_sheet(paper)
                # labels row
                npsheet.writerow(["Entity",COUNT,RELEVANT,COVIDPOSITIVE,COVID_R])
                # one row for entity
                for e in allEntities.keys():
                    if e not in newspapers[paper].keys():
                        row=[a(e),0,0,0,0]
                    else:
                        # one column for properties
                        row=[a(e)]
                        for label in newspapers[paper][e].keys():
                            row.append(newspapers[paper][e][label])

                    npsheet.writerow(row)


        sheet_sums=odsfile.new_sheet("SUMS")
        ## SUMS
        sheet_sums.writerow([counters[CORPUS], "items in corpus"])
        sheet_sums.writerow([counters[CORPUS], "valid articles in corpus"])
        sheet_sums.writerow([counters[ALL], "ARTICLES containing named entities"])
        sheet_sums.writerow([counters[RELEVANT], "RELEVANT ARTICLES (stacked ST score)"])
        sheet_sums.writerow([counters[NOTRELEVANT], "NOT RELEVANT ARTICLES (stacked ST score)"])
        sheet_sums.writerow([counters[COVIDPOSITIVE], "COVID ARTICLES"])

        sheet_meta=odsfile.new_sheet("META")
        sheet_meta.writerow(["extraction date:",str(datetime.now())])
        sheet_meta.writerow(["args:",str(args)])
        sheet_meta.writerow(["query:",str(srch)]) 
        sheet_meta.writerow([f"date range: {str(fromdate)} - {str(todate)}: {str(days)} days"])
        sheet_meta.writerow([f"Narticles: {str(nitems)}, Nvalid: {str(nvalid)} "])

        sheet_meta=odsfile.new_sheet("ENTITIES")
        sheet_meta.writerow(["entity","shortname","alias","AND list","NOT list"])
        for e in allEntities:
            sheet_meta.writerow([a(e),str(allEntities[e][SHORTNAME]),str(allEntities[e][ALIAS]),str(allEntities[e][SUPPLEMENT]),str(allEntities[e][COMPLEMENT])])

        #    odsfile.close()

else:
    import pprint
    pp = pprint.PrettyPrinter(depth=4)
    pp.pprint(allEntities)
    pp.pprint(counters)
    pp.pprint(newspapers)

if args.log: os.close(fd)
if args.veryverbose: os.close(dd)
