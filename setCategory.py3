#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
#import urltools
#import urllib.parse
from pymongo import MongoClient
from argparse import ArgumentParser

import constant as k
from hactar.db import loadRules
from hactar.db import openDB
from hactar.db import logEvent

def populateRules(filename):
    # load feedclass from file
    fc=[]
    with open(filename) as json_file:
        data = json.load(json_file)
        A={
            'ruleName': k.SOURCECATEGORY,
            'ruleDict': {'ALL': data}
        }
        if args.verbose: print(f"{A}")
        
        res=rulesDB.insert_one(A)
        if res.acknowledged and args.verbose:
            print("write OK")
        else:
            print(f"ERROR: {res}")
        exit()



# #
# MAIN
# #

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-i", "--interactive", action="store_true", dest="interactive", help="insert interactively")
parser.add_argument("--debug", action="store_true", help="debug")
parser.add_argument("--dryrun", action="store_true", help="dont write to db")
parser.add_argument("--overwrite", action="store_true", help="reassign a class to all items in db")
parser.add_argument("-F", "--populateRulesFile", dest="categories_file",  help="populate rules DB fron file [filename]")



(args) = parser.parse_args()

db=openDB(args.server,args.dbname)
feedsourceDB = db[k.FEEDSOURCE]
articleDB = db[k.ARTICLE]
logDB = db[k.EVENTLOG]
rulesDB = db[k.RULES]
if args.verbose: print(f"init db done")
rules=loadRules(rulesDB, k.SOURCECATEGORY)
if args.verbose: print("rules loaded")

if args.categories_file:
    populateRules(args.categories_file)

if rules == {}:
    if args.debug or (args.dryrun and args.verbose):
        print("skip: no "+k.SOURCECATEGORY+" rule in "+k.RULES+" DB")
    else:
        logEvent(args.verbose,logDB,"setCategory","warn","","","","skip: no "+k.SOURCECATEGORY+" rule in "+k.RULES+" DB")
    exit()


fc=[]
for item in rules['ALL']:
        for feed in item['feeds']:    
           f= (feed, item['id'])
           fc.append(f)
           
feedCategory=dict(fc)
if args.debug: print(f"{len(feedCategory.keys())} rules found")

articleDB.create_index(k.SOURCECATEGORIES)

if args.overwrite:
    search= {}
else:
    search={k.SOURCECATEGORIES: {"$exists": False}}

if args.debug: print(f"searching {search}")
all = articleDB.find(search,no_cursor_timeout=False)

for item in all:
    sc=[]
    if args.debug: print(f"item {item[k.UUID]}")
    if not (k.SOURCES in item.keys()):
        logEvent(args.verbose,logDB,"setCategory","warn","",str(item[k.UUID]),s,"missing key:"+k.SOURCES)
        if args.verbose: print(f"missing {k.SOURCES}")
        continue
    for s in item[k.SOURCES]:
        if s == "": continue
        s=s.replace("\'","")
        if s in feedCategory.keys():
            c=feedCategory[s]
            sc.append(c)
            if args.debug: print(f"category found {c} in rules for {s}")
        else:
            # look also in feedsource db
            (protocol,site,name)=s.split('|')
            feedsource=feedsourceDB.find_one({k.PROTOCOL: protocol, k.SITE: site, k.NAME: name})
            if k.FEEDCATEGORY in feedsource.keys():
                c=feedsource[k.FEEDCATEGORY]
                if args.debug:  print(f'found {c} in feedsource for {s}')
            else:
                # missing feed source
                if args.dryrun and args.verbose:
                    print(item[k.UUID], "#WARN: missing category for feed source", s)
                else:
                    logEvent(args.verbose,logDB,"setCategory","warn","",str(item[k.UUID]),s,"missing category")

    fcc = dict()
    for i in sorted(sc):
        fcc[i] = fcc.get(i, 0) + 1

    # set an unique category for each article
    # skip articles with more than 2 categories
    categories = list(fcc.keys())
    if "IGN" in categories:
        categories.remove("IGN")

    if len(categories) == 0:
        chosenCategory="NONE"
    elif len(categories) > 2:
        chosenCategory="NONE"
    elif len(categories) == 2: # articles with two categories: 
        if "HOME" in categories:
            categories.remove("HOME")
            chosenCategory=categories[0]
        else:
            chosenCategory="NONE"           # skip if both categories different from HOME
    else:  # len(categories) == 1:
        chosenCategory=categories[0]    

        
    if args.dryrun and args.verbose:
        print(f"{item[k.UUID]} {list(fcc.keys())} => {chosenCategory}")
    else:
        
        oid=item['_id']
        
        articleDB.update_one( { '_id': oid}, {
            '$set': {
                k.SOURCECATEGORIES: sc,
                k.CHOSENCATEGORY: chosenCategory,
                k.UPDATEDTS: datetime.datetime.utcnow(),
            }
        }, upsert=True)
        #logEvent(verbose,logDB,programName,"ok"|"warn"|"err",source,uuid,url,message)
        logEvent(args.verbose,logDB,"setCategory","ok",item[k.SOURCES][0].split('|')[1],str(item[k.UUID]),"",chosenCategory)
        if args.debug: print("setCategory","ok",item[k.SOURCES][0].split('|')[1],str(item[k.UUID]),"",chosenCategory)



