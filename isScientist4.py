#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2022 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


########################################
# This program analyzes articles and produces statistics about entities linked to 'scientists'
# - requires DB collections:
#     - newsXX/articles: articles prepared by setPeople.py (has 'people' field: named entities linked to people)
#     - entities/namedEntities: entities prepared by setName.py
# - source:
#     -Y: single year in archive
#     --uuid: file with list of uuids
# - requires files:
#     - Enties/scienziati: known scientists
#     - Entities/SCI-occupations: list of occupations considered 'scientific'
# - produces spreadsheet with several sheets:
#     - list of articles and related statistics
#     - all entities in articles and related statistics
#     - with option --relevance: sheets with statistics for relevant articles and high/medium/low relevance
# - parameters:
#     - CUTOFF_ARTICLES: minimum number of articles in which an entity is required to be named in order to be counted
#######################################

import inspect
import sys
from tqdm import tqdm
from namesClass import WDentry, WDquery
from hactar.entities import get_wos, get_imdb, get_orcid, get_pubmed, get_scopus
import constant as k

# PARAMETERS
OCCUPATIONSFILE="Entities/SCI-occupations"
KNOWNSCIFILE="Entities/SCI-entities"
IGNOREFILE="Entities/SCI-ignore"
EVERGREENSFILE="Entities/SCI-evergreens"
#KNOWNSCIFILE="Entities/scienziati"


#FUNCTIONS
# utilities
def verbose(s):
    import sys
    if args.verbose: print(s, file=sys.stderr)

def debug(s):
    import sys
    import inspect
    if args.debug :
        frame=sys._getframe(1).f_code.co_name
        stack=inspect.stack()
        if "self" in stack[1][0].f_locals.keys():
            classname=stack[1][0].f_locals["self"].__class__.__name__
        else:
            classname='__main__'
        print(f"#{classname}.{frame}:\t{s}")

# search mystring in text,
# if match return substring with r chars to the right and l chars to the left of the match
def search(text,mystring,l,r):
    t=text.lower()
    found=t.find(mystring.lower())
    if found != -1:
        left=len(text[0:found])
        right=len(text[found+len(mystring):-1])
        if left < l: l = left
        if right < r: r= right
        t=text[found-l:found+r+len(mystring)]
        if len(t)<len(mystring):
            print(f"ERR:{mystring},{t}/{text}")
        return(t)
    else:
        return("")

def load_scientists(filename):
    ''' Load entities from file, with  a distinct syntax  '''
    ''' fullName,sciclass,shortName,alias,supplement,complement'''
    
    SEP=","
    SUBSEP=" "

    names=[]
    with open(filename) as entitiesFile:
        for line in entitiesFile:
            if line[0] == '#':
                continue
            line=line.strip()
            try:
                (fullName,sciclass,shortName,alias,s,c)=line.rsplit(SEP,5)
            except Exception as e:
                exit(f"{e} at {line}")
            names.append(fullName)
        if shortName != '': names.append(shortName)
        if alias != '': names.append(alias)
    return(set(names))
        
def load_list(fileName):
    ''' reads a list of items from file, one per line 
        comments excluded '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            lst.append(line.strip())
    return(list(set(lst)))

def load_file(fileName):
    ''' reads a list of items from file, one per line 
        comments included '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            #   if line[0] == '#':
            #       continue
            lst.append(line.strip())
    return(list(lst))

def load_ksci(fileName):
    ''' reads a tab separated list of items (one per line), 
        builds dict with key= field 0,  value=field1 
        fields: "name+surname TAB [KSCI|NON]" 
    '''
    dct={}
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            if len(line.strip().split('\t')) == 1:
                v='KSCI'
                k=line.strip()
            else:
                (k,v)=line.strip().split('\t')
            dct.update({k:v})
    return(dct)

def load_occupations(fileName):
    ''' reads a tab separated list of items (one per line), 
        of 2 tab-separated fields, 
        builds dict with key= field 0,  value=field1 '''
    dct={}
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            if len(line.strip().split('\t')) == 1:
                continue
            else:
                (k,v)=line.strip().split('\t')
                
            dct.update({k:v})
    return(dct)


def is_between(value,lower,upper):
    if value >= lower and value < upper:
        return(True)
    else:
        return(False)

def hasItems(item,KEY,subKey):
    # checks if key,subkey exixt, returns value for item[key][subkey] 

    if item and KEY in item.keys() and subKey in item[KEY].keys() and item[KEY][subKey] != None:
        #debug(item)
        nitems=len(item[KEY][subKey])
        if nitems > 0:
            return(nitems)
        else:
            return(0)
    else:
        return(-1)    



def summary(item,klabels):

    # labels to init array of search functions
    PUBMED =    "pubmed"
    SCOPUS =    "scopus"
    ORCID  =    "orcid"
    IMDB   =    "imdb"
    WOS    =    "wos"
    
    out={NAME: ""}
    if item:
        out={NAME: item[NAME]}

    if IMDB in klabels:
        out.update({"imdb.items":0})
        KEY=IMDB
        if item and KEY in item.keys() and ITEMS in item[KEY].keys():
            out.update({"imdb.items": len(item[KEY][ITEMS])})

    if ORCID in klabels:
        out.update({"orcid.items":0})
        KEY=ORCID
        if item and KEY in item.keys() and ITEMS in item[KEY].keys():
            out.update({"orcid.items": len(item[KEY][ITEMS])})

    if WOS in klabels:
        out.update({"wos.items":0})
        KEY=WOS
        if item and KEY in item.keys() and COUNT in item[KEY].keys():
            out.update({"wos.items": item[KEY][COUNT]})

    if SCOPUS in klabels:
        out.update({"scopus.documentCount":0})
        KEY=SCOPUS
        if item and KEY in item.keys() and ITEMS in item[KEY].keys():
            dc=0
            for i in item[KEY][ITEMS]:
                if "documentCount" in i.keys():
                   dc+=i["documentCount"]
            out.update({"scopus.documentCount": dc})

    if PUBMED in klabels:
        out.update({"pubmed.items": 0})
        KEY=PUBMED
        if item and KEY in item.keys() and ITEMS in item[KEY].keys():
            dc=len(item[KEY][ITEMS])
            out.update({"pubmed.items": dc})

    
    return(out)

# CLASSES

class logFile(object):
    def __init__(self,file_name):
        self.file_name = file_name
        self.handle=open(self.file_name,'w') 

    def write(self,s):
        self.handle.write(s)

    def close(self):
        self.handle.close()


class Entity:
    def __init__(self):
        from hactar.db import openDB


        self.sci_occupations=load_occupations(OCCUPATIONSFILE)
        self.known_scientists=load_scientists(KNOWNSCIFILE)
        self.ignore=load_list(IGNOREFILE)
        self.evergreens=load_list(EVERGREENSFILE)

        #self.known_scientists=load_ksci(KNOWNSCIFILE)
       
        self.people   =openDB(args.server, k.ENTITIES)[k.PEOPLE]
        self.maypeople=openDB(args.server, k.ENTITIES)[k.MAYPEOPLE]
        self.nonpeople=openDB(args.server, k.ENTITIES)[k.NONPEOPLE]

    def is_known_sci(self,name):
        if name in self.known_scientists:
            result=KSCI # known as SCI
        elif name in self.evergreens:
            result=EVERGREEN
        elif name in self.ignore:
            result=IGNORE # nonsci
        else:
            result=0
        return(result)

    # def is_known_sci(self,name):
    #     if name in self.known_scientists.keys():
    #         if self.known_scientists[name] == 'KSCI':
    #             return(1) # known as SCI
    #         elif self.known_scientists[name] == 'NON':
    #             return(-1) # known as NOT sci
    #         else:
    #             debug(f"error: {self.known_scientists[name]} value not valid ")
    #     else:
    #         return(0) # not known

    def sci_probability(self,name):
        '''
        - searches name in people database, gets list
        - returns 
          the ratio between all occupations and scientist occupations 
          the list of sci-occupations as a comma-separated list
          if all occupations are in the same class, the sci-occupation class, 'MANY' if not
        - returns -1 if no sci occupation
        '''
        count=0
        sci=[]
        found=0
        occupations=[]
        sciclasses=[]
        sciclass=''
        for name in self.people.find({k.NAME: name}):
            count+=1
            sci+=list(set(name[k.OCCUPATIONS]).intersection(set(self.sci_occupations.keys())))
            if len(sci) >0:
                found+=1
                occupations+=sci
            for o in occupations:
                sciclasses.append(self.sci_occupations[o])

        # return ratio, list of occupations and class
        if count>0:
            # set occupation class if all occupations belong to the same class
            if  len(set(sciclasses)) == 1:
                sciclass=sciclasses[0]
            elif len(set(sciclasses)) > 1:
                sciclass='MANY'
            
            return(found/count,",".join(map(str,set(occupations))),sciclass)
        # no entity found under the name
        else:
            return(-1,"", "")



        
        

    def has_orcid_pubmed(self,name):
        pp=self.people.find({k.NAME: name})
        keys=[k.SCOPUS,k.ORCID]
        result=[]
        for p in pp:
            for ky in keys:
                if ky in p.keys() and p[ky] !='':
                    result.append(p[ky])
        #return(",".join(map(str,set(result))))
        return(len(",".join(map(str,set(result)))))
    
    def is_nonpeople(self,name):
        p=self.nonpeople.find_one({k.NAME: name})
        if p != None:
            debug(f"{name} nonp")
            return(True)
        else:
            return(False)


    def has_features(self,name):
        publications=None
        celebrity=None
        
        item=namedEntitiesDB.find_one({NAME: name})
        if not item:
            return(None,None)
        
        # labels to init array of search functions
        PUBMED =    "pubmed"
        SCOPUS =    "scopus"
        ORCID  =    "orcid"
        IMDB   =    "imdb"
        WOS    =    "wos"

        ITEMS  =    "items"

        # APIvec= {
        #     SCOPUS   : get_scopus,
        #     PUBMED   : get_pubmed,
        #     ORCID    : get_orcid,
        #     IMDB     : get_imdb,
        #     WOS      : get_wos,
        # }

        # [API, thereshold of items to be significant]
        sciApis=[
            [PUBMED,5],
            [WOS,5],
            [SCOPUS,5],
        ]

        score=0
        for s in sciApis:    
            if hasItems(item,s[0],ITEMS)>=s[1]: score+=1
            if hasItems(item,ORCID,ITEMS)>0: orcid=True

        # Must have publications listed in many dbs to be author
        if score >= 2:
            publications=True
    
        # IMDB denotes celebrity
        if hasItems(item,IMDB,ITEMS) >0 :
            celebrity=True

        return(publications,celebrity)

        
class Article:
    def __init__(self,item):

        if k.DONTINDEX in item.keys():
            self.valid=False
            #debug(f"{item[k.UUID]}\tdontindex")
        elif k.DATESPUBLISHED not in item.keys() or (not isinstance(item[k.DATESPUBLISHED],list)) or len(item[k.DATESPUBLISHED]) == 0:
            self.valid=False
            debug(f"{item[k.UUID]}\tno{k.DATESPUBLISHED}")
        elif k.PEOPLE not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnopeople")
        elif k.YEAR not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnoyear")
        elif k.ASSESSMENTS not in item.keys():
            debug(f"{item[k.UUID]}\tnoassess")
            self.valid=False
        elif k.STACKEDST not in item[k.ASSESSMENTS].keys():
            debug(f"{item[k.UUID]}\tnostackedst")
            self.valid=False
        else:
            self.valid=True
            #debug(f"{item[k.UUID]}\tvalid")
            self.score=item[k.SCORES][k.STACKEDST]
            if item[k.ASSESSMENTS][k.STACKEDST] == k.RELEVANT:
                self.relevant=True
                if self.score >= 0.95 :
                    self.relClass = 'VeryHigh'
                elif self.score >= 0.85 :
                    self.relClass = 'High'
                elif self.score >= 0.75 :
                    self.relClass = 'Medium'
                elif self.score >= 0.5 :
                    self.relClass = 'Low'
            else:
                self.relevant=False
                self.relClass = 'None'
            self.names=item[k.PEOPLE]
            self.year=item[k.YEAR]
            self.categories=item[k.SOURCECATEGORIES]
            self.text=item[k.CLEANTEXT]
            self.newspaper=item[k.SOURCES][0].split("|")[1]
            self.uuid=item[k.UUID]

    def get_entity_context(self,entity):
        self.context=search(self.text,entity,50,50)

            
class Sheet:
    def __init__(self):
        self.sheet={}

    def filter_boolean(self,invars):
        ''' for readability and bug in pyexcel lib, 
        transform boolean values in 'T' or 'F',
        leave other variables unchanged'''
        TR='⊨'
        FL='⊭'
        TR='T'
        FL='F'
        if not isinstance(invars, list):
            if isinstance(invars, bool):
                if invars == True:
                    outvar=TR
                else:
                    outvar=FL
            else:
                outvar=invars
            #debug(f"{invars}-->{outvar}")
            return(outvar)
        else:
            outvars=[]
            for v in invars:
                if isinstance(v,bool):
                    if v == True:
                        nvar=TR
                    else:
                        nvar=FL
                else:
                    nvar=v
                outvars.append(nvar)
            return(outvars)


    def make_sheet(self,sheetname,counter,threshold):
        ''' makes spreadsheet sheet from counter data, one name at a time'''
        #debug(f"headers: {counter.headers}")
        SEARCH='https://duckduckgo.com/'
        self.sheet[sheetname]=[]
        row=['entity']
        # get labels from counter
        row+=counter.headers
        # write data
        self.sheet[sheetname]+=[row]
        for n in counter.list_names(threshold):
            row=[f"=HYPERLINK(\"{SEARCH}?q={n.replace(' ','+')}\",\"{n}\")"]
            for l in counter.headers:
                row.append(self.filter_boolean(counter.names[n][l]))
            self.sheet[sheetname]+=[row]
        debug(f"{sheetname} has {len(self.sheet[sheetname])} items")

    def add(self,data,header,name):
        '''add matrix to sheet, as is'''
        self.sheet[name]=[header]
        for row in data:
            self.sheet[name]+=[self.filter_boolean(row)]
            
    def include_file(self,fileName,sheetname,separator):
        ''' add a sheet with all elements from arguments file, one per row,
        for documentation purposes '''
        lines=[]
        content=load_file(fileName)
        for line in content:
            lines.append([i.strip() for i in line.split(separator)])
            #lines.append([line])

        correct=''
        forbidden='/\[]*?:'
        for c in sheetname:
            if c in forbidden:
                correct+='-'
            else:
                correct+=c

        self.sheet.update({correct: 
            lines,
        })
        debug(f"{correct} has {len(lines)} items from {fileName}")


    def save_as_xlsx(self,filename):
        import xlsxwriter
        import datetime
        spreadsheet= xlsxwriter.Workbook(filename)

        highlight = spreadsheet.add_format()
        highlight.set_bg_color('')
        bold    = spreadsheet.add_format({'bold': True})
        
        for sheet in self.sheet.keys():
            worksheet = spreadsheet.add_worksheet(sheet)
            row=0
            col=0
            maxcol=0

            for line in self.sheet[sheet]:
                for datum in line:
                    if row==0:
                        worksheet.write(row,col,datum,bold)
                    else:
                        worksheet.write(row,col,datum)
                    col+=1
                    maxcol=col
                row+=1
                col=0
            if sheet in ["all entities","only relevant","high relevant","medium relevant","low relevant"]:
                worksheet.data_validation(1, 1, row, 1, {'validate': 'list','source': [f"new-{KSCI}", f"new-{IGNORE}", f"new-{EVERGREEN}"]})
                worksheet.write_comment(1,1, 'set new entities using suggested values')
            worksheet.autofit()
            worksheet.set_column(0,0, 20 ,bold ,None)
            worksheet.autofilter(0,0,row,maxcol)

        spreadsheet.close()


        
    def save(self,fileName):
        '''save spreadsheet, all sheets'''
        from pyexcel import save_as
        from pyexcel_ods3 import save_data
        from pyexcel_xlsx import save_data
        import datetime

        for i in self.sheet.keys():
            debug(f"sheet {i} with {len(self.sheet[i])} items")
        # add metadata sheet
        self.sheet.update({"metadata": [
            ["date:",str(datetime.datetime.now())],
            ["program:",str(sys.argv [0])],
            ["args:",str(args)],
            ["number of articles",narticles],
            ["number of relevant articles",nrelevant],
            
        ]})
        
        debug("added Meta and files sheets, writing")
        save_data(fileName+".xlsx", self.sheet)

            
class Counter:
    ''' This object stores different measures and counters for each name
    '''
    def __init__(self):
        from collections import OrderedDict
        self.names={}
        #self.headers=[TAG,SCORE,KSCI,SUM,RELSUM,SCOREAVG,SCORESUM,RELPCT,SCIPROB,ORCIDPUBMED, SCOPUS, IMDB, CATST]
        self.headers=[TAG,KSCI,SUM,RELSUM,SCIPROB,ORCIDPUBMED, SCOPUS, IMDB, CATST]
#mmzz        self.headers+=CATLABEL
        self.headers+=[OCCUPATION]
        self.headers+=[SCICLASS]
        self.headers+=[WHY]
        self.data = OrderedDict()
        self.n =0
        self.nrel=0

    def add(self,name,score,relevant):
        ''' adds item to counter, including:
        - n: number of items
        - nrel: number of relevant items
        - names: dict of names found, inits structure with:
                 sum: count articles for name
                 relsum: count for relevant articles for name
                 scoresum: sum of scores of all articles
                 scoreavg: average of score (scoresum/sum)
        '''
        self.n+=1
        if relevant==True:
            rel=1
            self.nrel+=1
        else:
            rel=0

        if name in self.names.keys():
            old=self.names[name]
            new={
                SUM:      old[SUM]+1,
                RELSUM:   old[RELSUM]+rel,
                SCORESUM: old[SCORESUM]+score
                }
            new.update({
                RELPCT:   new[RELSUM]/new[SUM],
                SCOREAVG: new[SCORESUM]/new[SUM],
            })
            self.names[name].update(new)
        else:
            self.names[name]={
                SUM: 1,
                RELSUM:   rel,
                SCORESUM: score,
                RELPCT:   rel,
                SCOREAVG: score,
            }
            
    def list_names(self,threshold=5):
        ''' Lists names currently in counter
        '''
        accept=[]
        for n in self.names.keys():
            if self.names[n][SUM]>=threshold:
                accept.append(n)
        return(accept)

    def set_sci(self, name, known, probability, occupation, pubs, publications, celebrity, nonp):
        ''' Adds data to names counter on occupation, pubblications, imdb, etc...
            Classifies according to data in KSCI, SCI, MAYSCI
        '''

        e=self.names[name]

        e[SCIPROB]=probability
        e[OCCUPATION]=occupation
        e[SCICLASS]=sciclass
        e[ORCIDPUBMED]=pubs
        e[SCOPUS]=publications
        e[IMDB]=celebrity
        e[KSCI]=known
        e[TAG]=''
        e[WHY]=''
        
        
        score =0
        
        if nonp:
            e[TAG]=k.MAYP
            if e[TAG] == k.MAYP: debug(f"{name} MAYP")
            score-=.5
            
        # name appears ONLY in S&T feeds
        catsum=0
        for c in CATEGORIES:
            catsum+=e[f"cat-{c}"]
        if catsum >0 and e[CATST]/catsum == 1:
            score+=1
            e[WHY]+='onlyST,'
        
        # name has likely has scientific occupation
        if e[SCIPROB] >= 0.75:
            score+=2
            e[WHY]+='sciOccupation,'
            
        # in relevant articles most of the time, 
        if e[SCOREAVG] > 0.5 :
            score+=1
            e[WHY]+='inRelevant,'
            
        # prevalent in relevant articles
        if e[RELPCT] >=0.9:
            score+=1
            e[WHY]+='relPct90%,'
        
        # has relevant number of SCI articles
        if (e[SCORESUM] >= 10) :
            score+=1
            e[WHY]+='highRelArticles,'

        # publications
        if e[SCOPUS] == True:
            score+=1
            e[WHY]+='scopus,'
            
       # publications
        if e[ORCIDPUBMED] != 0:
            score+=1
            e[WHY]+='publications,'


        # most likely a celebrity
        if e[IMDB] == True:
            score-=2
            e[WHY]+='celeb,'

        # all-caps names are journalists names
        haslower = any([char.islower() for char in name.replace(" ","")])
        if not haslower:
            score -=2
            e[WHY]+='allcaps,'

        if e[SCIPROB] ==-1 and e[OCCUPATION] == [] or e[ORCIDPUBMED] ==0  and not (e[IMDB] or e[SCOPUS]  or e[KSCI]):
            e[WHY]+='unknown,'
            unknowns.write(f"{name}\n")
            
        e[SCORE]=score

        # check if lower, upper bound
        RANGES={
            k.SCI: [3.5,999],
            k.MAYSCI: [2,3.5]}
        for r in RANGES.keys():
            if is_between(score,RANGES[r][0],RANGES[r][1]):
                e[TAG]=r

        # Tag as KSCI if known and SCI or MAYSCI
        if known != 0:
            e[TAG] = known
        
        # if known == KSCI:
        #     e[TAG]= k.KSCI
        # elif known == -1:
        #     e[TAG]= 'NOT'
            
               
        
    def set_category(self,cats):
        '''build labels for categories, increment if present in atricle  '''
        e=self.names[name]
        for c in CATEGORIES: # scan all categories
            cat=f"cat-{c}"
            
            if cat not in e.keys(): # init
                e[cat]=0
            if c in cats: # add
                e[cat]+=1
        
    def get_keys(self):
        allkeys=[]
        for e in self.names.keys():
            for ky in self.names[e].keys():
                if ky not in allkeys:
                    ky.append(allkeys)
        return(allkeys)
        
    def print(self,name):
        s=f"{name}\t"
        for l in self.headers:
            s+=f"{self.names[name][l]}\t"
        return(s)


NAME='name'
SUM='sum'
TAG='tag'
SCORE='score'
RELSUM='relevant Sum'
SCOREAVG='score Average'
SCORESUM='score Sum'
RELPCT='Pct Relevant'
KSCI='knownSCI'
SCIPROB='sciOccupationProb'
SCICLASS='sci occupation class'
OCCUPATION='occupation'
WHY='tagReason'
ORCIDPUBMED='has Orcid/Pubmed'
SCOPUS='has Scopus/Pubmed/WOS'
IMDB='has IMDB'
EVERGREEN='Evergreen'
IGNORE='NONSCI'


CATEGORIES=[ "CR", "CU", "EC", "ED", "HOME", "IGN", "SP", "ST" ]
CATST='cat-ST'
CATLABEL=[ "cat-CR", "cat-CU", "cat-EC", "cat-ED", "cat-HOME", "cat-IGN", "cat-SP", CATST ]




if __name__ == "__main__":    
    # MAIN
    from hactar.db import openDB
    import constant as k
    import time
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(      "dbname", type=str,       help='database name')
    parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose output")
    parser.add_argument("-d", "--debug", action="store_true", help="debug output")
    parser.add_argument(      "--context", action="store_true", help="pint name in context")
    #    parser.add_argument(      "--dryrun", action="store_true", help="dont write to db")
    parser.add_argument("-N", "--nitems",  type=int, help=f"limit to first N items")
    parser.add_argument(      "--cutoff"  , help="ignore entities with article count beyond this threshold (default: 5)")
    parser.add_argument("-Y", "--year"  , help="year")
    parser.add_argument("-k", "--known"  , help="true positives list. Format: name")
    parser.add_argument(      "--uuid"  , help="uuid list Format: name")
    parser.add_argument(      "--relevance", action="store_true"  , help="performe relevanc analysis (relevant& High/Medium/Low relevant")
    (args) = parser.parse_args()



    #
    if args.cutoff:
        CUTOFF_ARTICLES=int(args.cutoff)
    else:
        CUTOFF_ARTICLES=5
    # define filename basename for all outputs
    if args.year:
        FILENAME=f"isScientist-{args.year}"
    elif args.uuid:
        FILENAME=f"isScientist-{args.uuid}"
    else:
        FILENAME="isScientist-ALL"

    # Database init
    db=openDB(args.server, args.dbname)
    dbentities=openDB(args.server, 'entities')
    articleDB = db[k.ARTICLE]
    namedEntitiesDB = db["namedEntities"]

    # uuids source init: file or query
    articles=openDB(args.server, args.dbname)[k.ARTICLE]
    if args.year:
        query={k.YEAR: int(args.year)}
    else:
        query={}
    
    if args.known:
        KNOWNSCIFILE=args.known


    if args.uuid:
        uuids=set(load_list(args.uuid))
        debug(f"read {len(uuids)} uuids from {args.uuid}")
    else:
        debug(f"querying {query}")
        uuidFile=logFile(FILENAME+'.uuids')
        uuids=[]
        nitems=0
        all=articles.find(query,no_cursor_timeout=True).batch_size(1000)
        n=articles.count_documents(query)
        for item in tqdm(all,total=n):
            # limit run to first args.nitems items.
            if args.nitems:
                nitems+=1
                if nitems > args.nitems:
                    break

            uuids.append(item[k.UUID])
            uuidFile.write(f"{item[k.UUID]}\n")
        debug(f"read {len(uuids)} uuids from query {query}, wrote them in {FILENAME+'-uuids.txt'}")
        uuidFile.close()

    # create context file 
    if args.context:
        contextfile=logFile(FILENAME+'-context.csv')
        
    # scan articles, extract names, init counters
    counterAll=Counter()
    if args.relevance:
        counterRelevant=Counter()
        counterLowRel=Counter()
        counterMedRel=Counter()
        counterHigRel=Counter()

    artlist=[]
    ent=Entity()
    sheet=Sheet()

    narticles=0
    nrelevant=0
    # iterate over articles
    # count how many relevant articles for each name, and average score
    for uuid in tqdm(uuids):

        item=articles.find_one({k.UUID: uuid},no_cursor_timeout=True)
        article=Article(item)
        if not article.valid:
            continue
        narticles+=1
        for name in article.names:
            if args.relevance and article.relevant ==True:
                # counter for relevant articles
                counterRelevant.add(name,article.score,article.relevant)
                counterRelevant.set_category(article.categories)
                nrelevant+=1
            # counter for both relevant and not relevant articles
            counterAll.add(name,article.score,article.relevant)
            counterAll.set_category(article.categories)

            # counter for low relevance articles
            if  args.relevance and article.relClass == 'Low':
                counterLowRel.add(name,article.score,article.relevant)
                counterLowRel.set_category(article.categories)

            # counter for medium relevance articles
            if args.relevance and article.relClass == 'Medium':
                counterMedRel.add(name,article.score,article.relevant)
                counterMedRel.set_category(article.categories)

            # counter for high relevance articles
            if args.relevance and article.relClass == 'High':
                counterHigRel.add(name,article.score,article.relevant)
                counterHigRel.set_category(article.categories)

            # add context in context file
            if args.context:
                article.get_entity_context(name)
                contextfile.write(f"{article.uuid}\t{article.score}\t{article.relevant}\t{article.relClass}\t{article.newspaper}\t{name}\t{article.context}\r\n")
        # build article list
        artlist.append([item[k.UUID],item[k.SOURCESET],article.relevant,article.relClass,article.score,item[k.CHOSENCATEGORY],item[k.DATESPUBLISHED][0],item[k.DOWNLOADTS]])
    if args.context:
        contextfile.close()
        
    # Names collection completed
    # Enrich counters with data about entities from various sources
   # sheet.add(artlist,['uuid','sourceSet','relevant (T/F)','relClass','score','category','date published','downloaded'],'articles')
    
    unknowns=logFile(FILENAME+".unknowns")
    allnames=counterAll.list_names(CUTOFF_ARTICLES)
    if args.relevance :
        relevantnames=counterRelevant.list_names(CUTOFF_ARTICLES)
        lowrelevantnames=counterLowRel.list_names(CUTOFF_ARTICLES)
        medrelevantnames=counterMedRel.list_names(CUTOFF_ARTICLES)
        higrelevantnames=counterHigRel.list_names(CUTOFF_ARTICLES)
    
    for name in tqdm(allnames):
       known=ent.is_known_sci(name)
       (prob,occupation,sciclass)=ent.sci_probability(name)
           
       pubs=ent.has_orcid_pubmed(name)
       nonp=ent.is_nonpeople(name)

       (publications,celebrity)=ent.has_features(name)
       counterAll.set_sci(name,known,prob,occupation,pubs,publications,celebrity,nonp)

       if args.relevance and name in relevantnames:
           counterRelevant.set_sci(name,known,prob,occupation,pubs,publications,celebrity,nonp)
           if name in lowrelevantnames:
               counterLowRel.set_sci(name,known,prob,occupation,pubs,publications,celebrity,nonp)
           if name in medrelevantnames:
               counterMedRel.set_sci(name,known,prob,occupation,pubs,publications,celebrity,nonp)
           if name in higrelevantnames:
               counterHigRel.set_sci(name,known,prob,occupation,pubs,publications,celebrity,nonp)

    unknowns.close()
    
    # Data generation completed
    # Generate one sheet for each counter, save spreadsheet file

    debug("processing all entities")
    if args.uuid:
        sheetname=args.uuid
        if '.' in args.uuid:
            sheetname=sheetname.split('.')[0]
        if '/' in sheetname:
            sheetname=sheetname.split('/')[-1]
    else:
        sheetname="all entities"
        
    sheet.make_sheet(sheetname,counterAll,CUTOFF_ARTICLES)
    if args.relevance :
        debug("processing relevant")
        sheet.make_sheet("only relevant",counterRelevant,CUTOFF_ARTICLES)
        debug("processing high relevant")
        sheet.make_sheet("high relevant",counterHigRel,CUTOFF_ARTICLES)
        debug("processing medium relevant")
        sheet.make_sheet("medium relevant",counterMedRel,CUTOFF_ARTICLES)
        debug("processing low relevant")
        sheet.make_sheet("low relevant",counterLowRel,CUTOFF_ARTICLES)

    sheet.include_file(OCCUPATIONSFILE,f"file-{OCCUPATIONSFILE}",'\t')
    sheet.include_file(KNOWNSCIFILE,f"file-{KNOWNSCIFILE}",',')
    sheet.include_file(EVERGREENSFILE,f"file-{EVERGREENSFILE}",'\t')
    sheet.include_file(IGNOREFILE,f"file-{IGNOREFILE}",'\t')
    
    
    debug(f"saving {FILENAME}.xlsx")
    sheet.save_as_xlsx(f"{FILENAME}.xlsx")
