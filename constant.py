# constants


# MONGO KEY STRINGS
AND='$and'
EXISTS='$exists'

# database names
ENTITIES='entities'

# collections names
ARTICLE = 'article'
FEEDSOURCE = 'feedsource'
FEEDITEM = 'feeditem'
EVENTLOG = 'eventlog'
HTML= 'html' # year adeed at runtime : htmlYYYY
URLHASH = 'urlhash'
RULES = 'rules'
ARCHIVE = 'archive'
PEOPLE='people'
NONPEOPLE='nonpeople'
MAYPEOPLE='maypeople'
NONENTITY='nonentity'


# #
# key names
# #

## Article schema
ARTUUID='artuuid'
ALIAS='alias'
AUTHORS='authors'
ASSESSMENTS='assessments'
BODY='body'
BZHTML='bzhtml'
CHOSENCATEGORY='chosenCategory'
CLEANTEXT='cleanText'
CLEANTEXTREVISION='cleanTextRevision'
CLEANTEXTWORDS='cleanTextWords'
CLEANTEXTISSUES='cleanTextIssues'
CLEANTEXTMEASURES='cleanTextMeasures'
CONTENTHASH='contentHash'
DATESPUBLISHED='datesPublished'
DONTINDEX='dontIndex'
DOWNLOADTS='download-ts'
DUPLICATE='duplicate'
ISDUPLICATE='isDuplicate'
DUPLICATEDETECTOR='duplicateDetector'
DUPLICATEDIFFERENTSOURCE='duplicateDifferentSource'
RELATED='related'
HASMORPHOTAG = 'hasMorphoTag'  # morphoTag indexing fails: indexTooLong
FEEDCATEGORY='feedCategory'
ID='_id'
IMAGEURL='imageURL'
INHOMEPAGE='inHomePage'
METAHASH='metaHash'
MIGRATEDFROM='migratedFrom'
MWE='MWE'  # multi-word expressions
MWTOKENS='MWtokens'
MWPOS='MWpos'
MORPHOTAG = 'morphoTag'   # morphological analysis 
NAME='name'
NECB='necb'
NECT='nect'
NEEDSATTENTION='needsAttention'
NER='NER'
NERVERSION='NERversion'
PLACES='places'
POSTAG = 'posTag'   # morphological analysis 
PROTOCOL='protocol'
REDISINDEXED='redisIndexed'
SAMPLECLASS='sampleClass'
SCORES='scores'
SCRAPER='scraper'
SITE='site'
SOURCECATEGORIES='sourceCategories'
SOURCECATEGORY = 'sourcecategory'
SOURCES='sources'
SOURCESET='sourceSet'
SOURCESETS='sourceSets'
SUMMARY='summary'
TOKENS = 'tokens'   # morphological analysis 
TEXT='text'
TITLE= 'title'
TAG='tag'
UPDATEDTS='updated-ts'
URLS='URLs'
URLS='URLs'
UUID='uuid'
WID='wid' #WikidataID
YEAR='year'


#SUBKEYS
# ASSESSMENTS
STACKEDST='stackedST'

# ARCHIVE SCHEMA
ARTUUID= 'artuuid'
SOURCE='source'
BZHTML='bzhtml'
URL='URL'
            

#People
NAME='name'
OCCUPATIONS='occupations'
EMPLOYERS='employers'
DESCRIPTION='description'
SCOPUS='scopusAuthorID'
ORCID='orcidID'
IMDB='imdb'

# Morphological analysis
MORPHOTAGS = 'morphoTags'
MORPHOLANG = 'morphoLang' # country code for morphological analysis - ita eng
CLEANTEXT = 'cleanText'   # 
YEAR = 'year'             # integer representation of year eg. int('2020')
LANGUAGE = 'language'     # 

# Linguistic features Analysis
MORPHOHIER = 'morphoHier' #morphologiacl analysis hyerarchies of regular expressions 
MORPHOREX= 'morphoRex'    # morphological anlaysis regular expressions
MORPHOSIMPLIFY = 'morphoSimplify'  # morphological simplification rules rule name
LINGUISTICFEATURES = 'morphoFeatures' # morphological analysis output of language features 
LFREX="LFRegex"   # Linguistic Features Regular expressions rules rule name
LFREPLACEMENT="LFReplacement"   # Linguistic features replacement rules
LFHIER="LFHierarchy"  # Hierarchy rules for Linguistic Features Analysis rule name
LFVERB="LFVerb"  # verbs replacement rules

# SPACY
SPACYLANG = 'spacyLang'   # language specifier - IT, en_core_web_sm

# TEXT VARIABILITY  THRESHOLDS
VAR_THRESHOLD=0.20              # under that: X % words are repeated  1: no word repetitions
NOLOWCASE_THRESHOLD=0.5         # under that: more than half of words are not lowercase
NONTEXT_THRESHOLD=0.1           # over that: probably js code
NONTEXT_CHARS='<>{}[];=\\'      # chars in non-text strings
WORDSAHEAD=5                    # n. of words to check ahead in repeated strings search (strings of WORDSAHEAD words repeated are deleted)

# CLEANTEXT MEASURES LABELS and KEYS
NOLOWCASE="lowerCasePrevalence"
NONTEXT="nonTextPrevalence"
WORDVARIABILITY="wordVariability"
LEN="characters"

#issues
CASEISSUE="caseImbalance"
SHORTTEXTISSUE="veryShortText"
WORDVARISSUE="wordVariabilityImbalace"
NONTEXTISSUE="nonText"
DELREPEATISSUE="repeatedTextDeleted"   # Deprecated - not an issue

#LABELS
RELEVANT = "RELEVANT"
NOTRELEVANT = "NOT_RELEVANT"

#Is SCIENTST labels
KSCI='KSCI'
SCI='SCI'
MAYSCI='MAYSCI'
MAYP='MAYP'
