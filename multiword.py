
#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2022 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import inspect
import sys
import os
from tqdm import tqdm
import constant as k
from namesClass import debug
from namesClass import Names

import phrasemachine
import spacy_udpipe
import udpipe
import spacy
import math


def debug(s):
    import sys
    if args.verbose: print(s, file=sys.stderr)

def verbose(s):
    if args.verbose: print(s)

class Sheet:
    def __init__(self):
        self.sheet={}

    def filter_boolean(self,invars):
        ''' for readability and bug in pyexcel lib, 
        transform boolean values in 'T' or 'F',
        leave other variables unchanged'''
        TR='⊨'
        FL='⊭'
        TR='T'
        FL='F'
        if not isinstance(invars, list):
            if isinstance(invars, bool):
                if invars == True:
                    outvar=TR
                else:
                    outvar=FL
            else:
                outvar=invars
            #debug(f"{invars}-->{outvar}")
            return(outvar)
        else:
            outvars=[]
            for v in invars:
                if isinstance(v,bool):
                    if v == True:
                        nvar=TR
                    else:
                        nvar=FL
                else:
                    nvar=v
                outvars.append(nvar)
            return(outvars)

    def add_array(self,data,header,name):
        '''add matrix to sheet, as is'''
        self.sheet[name]=[header]
        for row in data:
            self.sheet[name]+=[self.filter_boolean(row)]

    def add_dict(self,dic,header,name):
        '''add dict to sheet, key as row, value as column'''
        self.sheet[name]=[header]
        for row in dic.keys():
            self.sheet[name]+=[[row,self.filter_boolean(dic[row])]]

    def include_file(self,filename,sheetname):
        ''' add a sheet with all elements from arguments file, one per row,
        for documentation purposes '''
        lines=[]
        content=load_file(filename)
        for line in content:
            lines.append([line])

        correct=''
        forbidden='/\[]*?:'
        for c in sheetname:
            if c in forbidden:
                correct+='-'
            else:
                correct+=c

        self.sheet.update({correct: 
            lines,
        })
        debug(f"{correct} has {len(lines)} items from {filename}")

    def save(self,filename):
        '''save spreadsheet, all sheets'''
        from pyexcel import save_as
        #from pyexcel_ods3 import save_data
        from pyexcel_ods import save_data
        from pyexcel_xlsx import save_data
        import datetime

        for i in self.sheet.keys():
            debug(f"sheet {i} with {len(self.sheet[i])} items")
        # add metadata sheet
        self.sheet.update({"metadata": [
            ["date:",str(datetime.datetime.now())],
            ["program:",str(sys.argv [0])],
            ["args:",str(args)],            
            ["articles:",nitem],            
#            ["threshold:",str(THRESHOLD), "occurrences"],            
            ["Spacy Model:",SPACY_MODEL],

        ]})
        
        debug(f"added Meta and files sheets, writing {filename}")
        save_data(filename, self.sheet)

class Article:
    def __init__(self,item):
        self.uuid=item[k.UUID]
        if k.DONTINDEX in item.keys():
            self.valid=False
        elif k.YEAR not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnoyear")
        elif k.ASSESSMENTS not in item.keys():
            debug(f"{item[k.UUID]}\tnoassess")
            self.valid=False
        elif k.STACKEDST not in item[k.ASSESSMENTS].keys():
            debug(f"{item[k.UUID]}\tnostackedst")
            self.valid=False
        else:
            self.valid=True
            #debug(f"{item[k.UUID]}\tvalid")
            self.score=item[k.SCORES][k.STACKEDST]
            if item[k.ASSESSMENTS][k.STACKEDST] == k.RELEVANT:
                self.relevant=True
                if self.score >= 0.95 :
                    self.relClass = 'VeryHigh'
                elif self.score >= 0.85 :
                    self.relClass = 'High'
                elif self.score >= 0.75 :
                    self.relClass = 'Medium'
                elif self.score >= 0.5 :
                    self.relClass = 'Low'
            else:
                self.relevant=False
                self.relClass = 'None'
            self.year=item[k.YEAR]
            self.categories=item[k.SOURCECATEGORIES]
            self.text=item[k.CLEANTEXT]
            self.uuid=item[k.UUID]

            if k.PEOPLE in item.keys():
                self.people=item[k.PEOPLE]
            else:
                self.people=[]
            
            if k.POSTAG in item.keys() and k.TOKENS in item.keys():
                self.tokens=item[k.TOKENS]
                self.pos=item[k.POSTAG]
            else:
                doc=nlp(self.text)            
                self.tokens = [token.text for token in doc]
                self.pos = [token.pos_ for token in doc]

                doc = udp(self.text)
                #self.noun_chunks = doc.noun_chunks
                self.udp_tokens=doc
                
                # # write to db
                # if not args.dryrun:
                #     try:
                #         articles.update_one({k.UUID: item[k.UUID]},
                #                             {'$set': {k.POSTAG: pos}},
                #                             {'$set': {k.TOKENS: tokens}})
                #     except Exception as e:
                #         debug(f"{e} writing {articles}/{k.POSTAG} or {k.TOKENS} to {item[k.UUID]} ")

            
class Counts(object):
    ''' appends  (put) to dict[key] and returns (get) unique elements  '''
    ''' key: {key: sub_dict[key]} <-- increments value'''
    
    def __init__(self,keys):
        self.dic={}
        for l in keys:
            self.dic.update({l:{}})

    ''' creates key if absent, adds sub_dict to key dic if absent, otherwise increments sub_dict values for each key '''
    def put(self,key,sub_dict):
        if key not in self.dic.keys():
            self.dic.update({key:{}})

        for subkey in sub_dict.keys():
            if subkey not in self.dic[key].keys():
                self.dic[key].update({key:sub_dict[subkey]})
            else:
                self.dic[key][subkey]+=sub_dict[subkey]
                
    def keys(self):
        return(self.dic.keys())

    def get(self,key):
        if key in self.dic.keys():
            return(self.dic[key])
        else:
            return(None)

    def getValue(self,key,subkey):
        if key in self.dic.keys():
            if subkey in self.dic[key].keys():
                return(self.dic[key][subkey])
        else:
            return(0)

class Counter():
    def __init__(self):
        self.dic={}   # multiword counter
        self.relevanceClasses=[]  # relevance classes list
        self.classCount={}   # class counter
        self.relevantCount={'documents':0, 'terms':0}   # all relevant classes counter
        self.allCount={'documents':0, 'terms':0} # document counter, terms counter


    def getRelevantCount(self):
        return(self.relevantCount)

    def getAllCount(self):
        return(self.allCount)
        
    def getMultiwords(self):
        return(self.dic.keys())

    def getRelevanceClasses(self):
        return(self.relevanceClasses)
    
    def getClassCount(self,relClass):
        return(self.classCount[relClass])
    
    def set(self,mw,relClass,terms):
        # init relevance class counters
        if relClass not in self.relevanceClasses:
            self.relevanceClasses.append(relClass)
            
        #init mw counter
        if mw not in self.dic.keys():
            self.dic.update({mw:{}})
        # init relevance class counter for mw
        if relClass not in self.dic[mw].keys():
            self.dic[mw].update({relClass:
                                 {'terms':terms,
                                  'documents':1}
                                 })
        # increment mw counter [terms, documents]
        else:
            self.dic[mw][relClass]['terms']+=terms
            self.dic[mw][relClass]['documents']+=1

        # init or increment class counters
        if relClass not in self.classCount.keys():
            self.classCount[relClass]= {'terms':terms,
                                        'documents':1}
        else:
            self.classCount[relClass]['terms']+=terms
            self.classCount[relClass]['documents']+=1

        # increment relevant articles counters
        if relClass!='None':
            self.relevantCount['terms']+=terms
            self.relevantCount['documents']+=1

        #increment general documents and terms counters
        self.allCount['documents']+=1  # document counter
        self.allCount['terms']+=terms      # terms counter
        
    def getClassNumerosity(self,relClass,OnlyRelevant):
        n=0
        for mw in self.getMultiwords():
            if relClass in self.dic[mw].keys():
                if OnlyRelevant and relClass=='None':
                    continue
                n+=len(self.dic[mw][relClass])
        return(n)
        
    def getMwCount(self,mw,Class):
        # get number of terms and documents for a given mw in all, relevant and a specific class
        r={'terms':0, 'documents':0}
        if Class =='ANY':
            for relClass in self.dic[mw].keys():
                r['terms']+=self.dic[mw][relClass]['terms']
                r['documents']+=self.dic[mw][relClass]['documents']
        elif Class =='RELEVANT':
            for relClass in self.dic[mw].keys():
                if relClass=='None':
                    continue
                r['terms']+=self.dic[mw][relClass]['terms']
                r['documents']+=self.dic[mw][relClass]['documents']
        else :
            if Class in self.dic[mw].keys():
                r['terms']=self.dic[mw][Class]['terms']
                r['documents']=self.dic[mw][Class]['documents']
            
        return(r)
        
        
class logFile(object):
    def __init__(self,file_name):
        self.file_name = file_name
        self.handle=open(self.file_name,'w') 

    def write(self,s):
        self.handle.write(s)

    def close(self):
        self.handle.close()

if __name__ == "__main__":

    # MAIN
    from hactar.db import openDB
    import constant as k
    from argparse import ArgumentParser
    import uuid

    parser = ArgumentParser()
    parser.add_argument(      "dbname", type=str,       help='database name')
    parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose std output")
    parser.add_argument(      "--debug", action="store_true", help="extra std err")
    parser.add_argument(      "--dryrun", action="store_true", help="dont write to db")
    parser.add_argument(      "--overwrite", action="store_true", help="overwrite to db")
    parser.add_argument(      "--log", action="store_true", help="save log file")
    parser.add_argument(      "--nitems", type=int, help="max items")
    parser.add_argument(      "--uuids", help="take input from file of uuids rather than db query")
    parser.add_argument("-Y", "--year"  , help="query: year")
    (args) = parser.parse_args()

       
    articles=openDB(args.server, args.dbname)[k.ARTICLE]



    if args.uuids:
        all=[]
        verbose(f"getting uuids from {args.uuids}")
        with open(args.uuids, 'r') as f:
            for line in f:
                if line[0] == '#':
                    continue
                item=articles.find_one({k.UUID: line.strip()},no_cursor_timeout=True)
                if item != None:
                    all.append(item)
                    verbose(f"added {item[k.UUID]}")
        fileName=f"{os.path.basename(__file__).split('.')[0]}-{args.dbname}-{os.path.basename(args.uuids).split('.')[0]}"
        debug(f"logging to {fileName}.log")
        total=len(all)
        verbose(f"read {total} items")
    else:
        query={k.YEAR: int(args.year)}
        debug(f"querying {query}")
        all=articles.find(query,no_cursor_timeout=True).batch_size(1000)
        if args.nitems:
            total=int(args.nitems)
        else:
            total=articles.count_documents(query)

        fileName=f"{os.path.basename(__file__).split('.')[0]}-{args.dbname}-{args.year}"
    debug(f"logging to {fileName}.log")
    logf=logFile(fileName+'.log')

    
    # load spacy/udpipe models
    if args.dbname ==  'newsIT':
        nlp = spacy.load("it_core_news_lg")
        SPACY_MODEL="./italian-isdt-ud-2.5"
        spacy_udpipe.load_from_path(lang="it", path=SPACY_MODEL)
        udp = spacy_udpipe.load("it")

    elif args.dbname ==  'newsEN':
        nlp = spacy.load("en_core_web_trf")
        spacy_udpipe.download("en") 
        udp= spacy_udpipe.load("en")
        
    else:
        quit('no language model for {args.dbname}')

    CLASSES=['VeryHigh','High','Medium','Low','None']
    
    mw_result={}
    #for fn in CLASSES:
    #    mw_result.update({fn: logFile(f"{fileName}-{fn}.out")})
        
    mw_documents=Counts(CLASSES)
    mw_occurrences=Counts(CLASSES)
    multiwords=Counter()
    
    #dependencies=Counts([])
    
    # main loop
    nitem=0
    for item in tqdm(all, total=total):

        if args.nitems and nitem>=args.nitems:
            break
        else:
            nitem+=1

        article=Article(item)        
        if not article.valid:
            logf.write(f"warn:\t{article.uuid}\tINVALID\n")
            continue
        
        # extract multiwords and exclude multiwords that are also in article's people names
        
        candidates=phrasemachine.get_phrases(tokens=article.tokens, postags=article.pos)

        for mw in candidates['counts'].keys():
            if mw in {p.lower() for p in article.people} and mw != "":
                logf.write(f"mw:\t{article.uuid}\t{article.relClass}\t{mw}\t{candidates['counts'][mw]}\tDISCARDED\n")
            else:                
                multiwords.set(mw,article.relClass,candidates['counts'][mw])
                logf.write(f"mw:\t{article.uuid}\t{article.relClass}\t{mw}\t{candidates['counts'][mw]}\n")


        # phrase dependencies for nouns
#        for token in article.udp_tokens:
#            logf.write(f"{article.uuid}\tdep:\t{token.dep_}\t{token.lemma_}\t{token.text}\n")
#            dependencies.put(token.dep_,{token.lemma_:1})
            
#        for chunk in article.noun_chunks:
#            logff.write(f"udp:\t{chunk.root.dep_}\t{chunk.root.lemma_}\t{chunk.root.text}\t{chunk.text}\n")
#            dependencies.put(chunk.root.dep_,{chunk.root.lemma_:1})


    # output; FIRST FILE: multiwords 
    logf.write(f"info:\tCounting multiword items. N={nitem} documents.\n")
    out=Sheet()
    
    # documents numerosity - corpus
    nitems=multiwords.getAllCount()['documents']

    # Sheet: multiwords All
    matrix=[]
    sheet='All'
    h=['mw','tf','df','tf*idf']
    for mw in multiwords.getMultiwords():
        if(multiwords.getMwCount(mw,'RELEVANT')['terms'])>0:  # include only terms present in relevant articles
            r=[mw]
            tf=multiwords.getMwCount(mw,'ANY')['terms']
            df=multiwords.getMwCount(mw,'ANY')['documents']
            idf=math.log10(nitem/df)
            r+=[tf,df,tf*idf]
            matrix.append(r)
    out.add_array(matrix,h,sheet)
    
    # Sheet: multiwords relevant
    matrix=[]
    sheet='Relevant'
    h=['mw','tf','df','tf*idf']
    for mw in multiwords.getMultiwords():
        if(multiwords.getMwCount(mw,'RELEVANT')['terms'])>0:  # include only terms present in relevant articles
            r=[mw]
            tf=multiwords.getMwCount(mw,'RELEVANT')['terms']
            df=multiwords.getMwCount(mw,'RELEVANT')['documents']
            if df>0:
                idf=math.log10(nitems/df)
                r+=[tf,df,tf*idf]
            else:
                r+=[tf,df,None,None]
                idf='-'
            matrix.append(r)
    out.add_array(matrix,h,sheet)
    
    # Sheet: multiwords by relevance class
    matrix=[]
    sheet='Relevance class'
    h=['mw']
    for relevance in multiwords.getRelevanceClasses():
        h+=[relevance+'-tf',relevance+'-df',relevance+'-tf*idf']
    # values
    # iterate over mwords, build output array
    for mw in multiwords.getMultiwords():
        r=[mw]
        if(multiwords.getMwCount(mw,'RELEVANT')['terms'])>0:  # include only terms present in relevant articles
            for relevance in multiwords.getRelevanceClasses():
                tf=multiwords.getMwCount(mw,relevance)['terms']
                df=multiwords.getMwCount(mw,relevance)['documents']
                if df>0:
                    idf=math.log10(nitems/df)
                    r+=[tf,df,tf*idf]
                else:
                    r+=[tf,df,None]

            matrix.append(r)
    out.add_array(matrix,h,sheet)
    
    #close File, save
    logf.write(f"info:\twriting {fileName}.xlsx\n")
    out.save(fileName+'.xlsx')

    quit()






    

    # output; FIRST FILE: multiwords
    logf.write("Counting multiword items")
    out=Sheet()
    multi={}
    
    #scoring weights for mw expressions
    WEIGHTS={'VeryHigh':1.25,
             'High':1,
             'Medium':0.5,
             'Low':0.125,
             'None':0}

    # transpose: {Class: {mw: count}} => {mw: {class: count}}
    for relevance in CLASSES:
        for mw in multiwords.get(relevance):
            multi.update({mw:{relevance:mw_documents.getValue(relevance,mw)}})
            
    # build array: {mw: {class: count}} => [mw: class class class]    
    matrix=[]
    # count occurrences in each class, sum all occurrences and compute relevant/all occurrences ratio
    for mw in multi.keys():
        r=[mw]
        sums=0
        rel_sum=0
        rel_weight=0
        for relevance in CLASSES:
            occurrences=mw_documents.getValue(relevance,mw)
            r.append(occurrences)
            if occurrences != None:
                sums+=occurrences
                
                if relevance != 'None':
                    rel_sum+=occurrences
                    rel_weight+=occurrences*WEIGHTS[relevance]
        rel_ratio=rel_sum/sums
        rel_index=rel_weight/sums
        # print only mw present in relevant articles
        if rel_sum > THRESHOLD:
            matrix.append(r+[sums,rel_ratio,rel_index])

    out.add_array(matrix,['mw']+list(CLASSES)+['sum','rel_ratio', 'rel_weight'],'multiword per relevance')
        
    logf.write(f"writing {fileName}.xlsx\n")
    out.save(fileName+'.xlsx')

        
    # SECOND FILE ; lemmas & dependencies
    # build a table of terms and table their tags frequency
    #       tag tag tag 
    # name  n   n   n
    # lemmas=[]
    # data=[]
    # pctData=[]
    # tags=dependencies.keys()
    # verbose(f"{len(tags)} tags present")
    # # build lemmas list
    # for tag in tags:
    #     lemmas+=dependencies.get(tag).keys()
    # verbose(f"{len(lemmas)} lemmas present")

    # ndiscard=0
    # for lemma in set(list(lemmas)):#for lemma in set(list(lemmas)):
    #     counts=[]
    #     for tag in tags:
    #         count=dependencies.getValue(tag,lemma)
    #         if count==None:
    #             counts.append(0)
    #         else:
    #             counts.append(count)

        
    #     # discard lemmas whith all values below threshold
    #     if max(counts[1:-1]) >= THRESHOLD:
    #         total=sum(counts)
    #         data.append([lemma]+counts+[total])

    #         # compute data for percentage distribution of tags for each lemma
    #         pct=[]
    #         for i in counts:
    #             if i != 0:
    #                 pct.append((i/total)*100)
    #             else:
    #                 pct.append(0)
    #         pctData.append([lemma]+pct+[sum(pct)])
            
    #     else:
    #         ndiscard+=1
    # verbose(f"{ndiscard} lemmas with all tags below threshold discarded")


    # #find nouns that are both (nsubj) AND (obj OR obl:agent)
    # nsubj=dependencies.get('nsubj').keys()
    
    # obj=dependencies.get('obj').keys()
    # oblagent=dependencies.get('obl:agent').keys()
    # narrowdata=[]
    # header=['lemma','nsubj','obj','obl:agent']
    # for lemma in nsubj:
    #     n_nsubj=dependencies.get('nsubj')[lemma]
    #     if lemma in obj or lemma in oblagent:
    #         n_obj=n_oblagent=0
    #         if lemma in obj:
    #             n_obj=dependencies.get('obj')[lemma]
    #         if lemma in oblagent:
    #             n_oblagent=dependencies.get('obl:agent')[lemma]
    #         if n_nsubj + n_obj + n_oblagent >= THRESHOLD:
    #             narrowdata.append([lemma,n_nsubj,n_obj,n_oblagent])


    # out=Sheet()
    # out.add_array(data,['']+list(tags),"lemmas-tags")        
    # out.add_array(pctData,['']+list(tags),"lemmas-pct")
    # out.add_array(narrowdata,header,"nsubj AND (obj OR oblagent)")

    # logf.write(f"writing {fileName}-lemmas.xlsx\n")
    # out.save(fileName+'-lemmas.xlsx')
        
    logf.write(f"done\n")

    logf.close()
