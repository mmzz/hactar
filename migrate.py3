#!/usr/local/bin/python3.7
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018,2019 Alberto Cammozzo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.#


# This program may help in aquiring, scraping, deduplicating and storing existing corpora 
# of articles in HTML form and storing them in an hactar corpus

import sys
import getopt
import codecs
import bson
import bz2
import os
import datetime
import re
import validators
import newspaper
import hashlib
import uuid
from bson import Binary
from pymongo import MongoClient
from bson.objectid import ObjectId
from argparse import ArgumentParser
from hactar.db import updateArticle
from hactar.strings import cleanText
from hactar.strings import doNEC
import json
from fuzzywuzzy.process import dedupe as fuzzy_dedupe

from readability import Document
import html2text
from lxml import html


# scraping functions
def scrape_newspaper3k(htmlSource,scraped):
        try:
            article = newspaper.Article('')
            article.set_html(htmlSource)
            article.parse()
        except Exception as e:
            print ("WARN: scraping exception ["+e+"]\t",soid)
        scraped['text']= cleanText(str(article.text.strip())) 
        scraped['title']= cleanText(str(article.title.strip()))
        scraped['authors']=[article.authors]
        scraped['top_image']=article.top_image
        scraped['scraper']="newspaper3k"


def scrape_html2text(htmlSource,scraped):
        # call Html2text scraper
        doc = Document(input=htmlSource,
                           positive_keywords=["articleColumn", "article", "content"],
                           negative_keywords=["commentColumn", "comment", "comments"])
        h = html2text.HTML2Text()
#        h.inline_links = False # reference style links
        h.ignore_links = True
        h.wrap_links = False
        h.ignore_anchors = True
        h.ignore_images=True
        h.ignore_emphasis=True
        h.ignore_tables=True
        h.unicode_snob = True  # Prevents accents removing
        h.skip_internal_links=True
        h.body_width = 0
        text = cleanText(h.handle(doc.summary()).strip())
        title = cleanText(str(doc.short_title().encode('utf-8').strip()))
        title = cleanText(doc.short_title().strip())
        scraped['text']= text
        scraped['title']=title
        scraped['authors']=[]
        scraped['top_image']=""
        scraped['scraper']="html2text"


def updateSource(sourceDB,oid,artuuid,migratedTo):
    sourceDB.update_one( { '_id': ObjectId(oid)}, {
            '$set': {
               'destinationUuid': artuuid,
               'migratedTo': migratedTo
        },}, upsert=True
    )

#
# MAIN
# 

parser = ArgumentParser()
parser.add_argument("source", type=str,help='server:port/source database/collection')
parser.add_argument("destination", type=str,help='server:port/destination database/collection')
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-D", "--dryrun", action="store_true", help="dont write to DB")
parser.add_argument("-U", "--updateSource", action="store_true", help="write destination uuid and migratedTo fields")
parser.add_argument("-I", "--ignoreMigrated", action="store_true", help="migrate items that have already been migrated")
(args) = parser.parse_args()


(sserver,sdatabase,scollection)=args.source.split("/")
sconn = MongoClient(sserver)
sdb = sconn[sdatabase]
sourceDB = sdb[scollection]

(dserver,ddatabase,dcollection)=args.destination.split("/")
dconn = MongoClient(dserver)
ddb = dconn[ddatabase]
destinationDB = ddb[dcollection]


if not args.dryrun:
    print("RUN source:",sserver,sdb," destination:",dserver, ddb)
    print("RUN indexing migratedTo...")
    sourceDB.create_index('migratedTo')
    print("RUN indexing metaHash...")
    sourceDB.create_index('metaHash')
    print("RUN indexing contentHash...")
    sourceDB.create_index('contentHash')
    if args.updateSource:
        print("RUN indexing migratedFrom...")
        destinationDB.create_index('migratedFrom')

if args.ignoreMigrated:
    count = sourceDB.count_documents({})
    all = sourceDB.find({},no_cursor_timeout=False).batch_size(10)
else:
    count = sourceDB.count_documents({'migratedTo': False})
    all = sourceDB.find({'migratedTo': False},no_cursor_timeout=False).batch_size(10)
print("RUN documents: ",count)

#scan all source articles
for item in all:
        #gather data & metadata
        soid=str(item['_id'])
        migratedFrom= sdatabase+'-'+scollection+'-'+soid
        done = destinationDB.find_one({'migratedFrom': migratedFrom})     # check if article has already been migrated
        if done:
            artuuid=done['uuid']
            doid=str(done['_id'])
            migratedTo= ddatabase+'-'+dcollection+'-'+doid
            if args.updateSource and not args.dryrun : updateSource(sourceDB,soid,artuuid,migratedTo)
            if args.verbose: print("SKIP: already migrated\t",soid,'->',doid)
            continue

        #proceed to migration                        
        #necessary source data : url, date, feed, html source
        url=item.get('feed').get('itemURL')
        feedSummary=""
        s= item.get('feed').get('itemSummary')
        if s: feedSummary=s
        feedAuthors=[]
        a=item.get('feed').get('itemAuthor')
        if a: feedAuthors=re.split(", | e ",a)
        downloadTS=item.get('art').get('DownloadTimeStamp')
        try:
            source="rss|"+item.get('smm').get('Feed')
        except:
            print ("missing feed smm.Feed")
            continue
        published=item.get('feed').get('itemPubISOdate')
        feedTitle=item.get('feed').get('itemTitle')
        htmlSource= bz2.decompress(item.get('art').get('Bz2HTML'))

        # skip article with empty html
        if len(htmlSource) == 0:
            if args.verbose: print("SKIP: empty html\t",soid, downloadTS, published )
            if not args.dryrun: updateSource(sourceDB,soid,"none","emptyHtml")
            continue

        #init data structure for scraped content
        scraped={
                'text': "",
                'title': "",
                'authors': [],
                'top_image':"",
                'scraper':"",
             }
        #call Newspaper3k scraper
        scrape_newspaper3k(htmlSource,scraped)
       

        # skip article with empty text
        if len(scraped['text']) < 500:
            scrape_html2text(htmlSource,scraped)
            if len(scraped['text'] ) < 500:
                if args.verbose: print("SKIP: empty/short text\t",soid, downloadTS, published )
                if args.updateSource and not args.dryrun: updateSource(sourceDB,soid,"none","emptyText")
                continue

        # title: first scraped title, second feed tile, if both are empty, skip
        if scraped['title']  == "":
            if feedTitle == "":
                if args.verbose: print("SKIP: no title\t", soid, url)
                if args.updateSource and not args.dryrun: updateSource(sourceDB,soid,"none","noTitle")
                continue
            else:
                title=feedTitle
        else:
            title=scraped['title']

        # authors: first scraped, then feed authors
        if scraped['authors'] :
            authors = scraped['authors']
        else:
            authors= feedAuthors
            
        # do RE Named Entity candidates detection
        necb=doNEC(scraped['text'],False)
        nect=doNEC(title,True)

        ##
        ## DUPLICATE DETECTION
        ##

        #duplicate check on metadata hash
        metahash=hashlib.md5((str(source)+str(url)+str(published)).encode('utf-8')).hexdigest()
        contenthash=hashlib.md5(str(scraped['text']).encode('utf-8')).hexdigest()

        #case A: same content,same metadata
        dup=destinationDB.find_one( 
                { '$and': [
                    { 'contentHash': contenthash}, # select articles with same content
                    { 'metaHash':    metahash}     # AND  with same metadata 
                ]},  no_cursor_timeout=False)
        if dup:
            if args.verbose: print("SKIP: duplicate\t",soid, downloadTS, published )
            if args.updateSource and not args.dryrun: updateSource(sourceDB,soid,"none","duplicateMeta")
            continue

        #case B same content different metadata
        dup=destinationDB.find_one(
                { '$and': [
                    { 'contentHash': contenthash}   , # select articles with same content
                    { 'metaHash':   {'$ne': metahash}}   # AND different metadata 
                ]} ,  no_cursor_timeout=False)
        if dup:
            if args.verbose: 
                print("DUP: same content, different metadata", soid, "->", dup['uuid'])
#                print("\t    ",dup['datesPublished'],dup['sources'],"\t",dup['URLs'],"\n")
#                print("\t    ",published,source,"\t",url)
            if not args.dryrun:
                try:
                    destinationDB.update_one( { '_id': dup['_id']}, {
                            '$push': {
                                'sources': source,
                                'URLs': url,
                                'sources': source,
                                'datesPublished': published,
                                'migratedFrom': migratedFrom,
                            }, $slice=10, upsert=False)
                    destinationDB.update_one( { '_id': dup['_id']}, {
                            '$set': { 'updated-ts': datetime.datetime.utcnow()}
                    })

                except 	Exception as e:
                    if args.verbose: print("ERR: writing to destination DB", dup['_id'], e)
                try:
                    if args.updateSource: updateSource(sourceDB,soid,"none","duplicateMeta")
                except 	Exception as e:
                    if args.verbose: print("ERR: writing to source DB", soid, e)
            continue

        # case C: no duplicate, add new article
        necl=[]                   # named entity candidates
        if necb:
            try:
                necl=list(fuzzy_dedupe(necb))
            except Exception as e:
                if args.verbose: print("ERR: fuzzywuzzy exception", soid, e)
        
        # build data structures
        artuuid=str(uuid.uuid4()) # unique uuid for document and html archive
        article=({
            'uuid':artuuid,
            'URLs':[url],
            'sources':[source],
            'datesPublished': [published],
            'title': title,
            'summary': feedSummary,
            'download-ts':downloadTS,
            'author': authors,
            'metaHash':metahash,
            'contentHash': contenthash,
            'imageURL':scraped['top_image'],
            'text':scraped['text'],
            'necb':necl,
            'nect':nect,
            'scraper': scraped['scraper'],
            'migration-ts': datetime.datetime.utcnow(),
            'migratedFrom': [migratedFrom],
        })
        
        if not args.dryrun:
                try:
                    res=destinationDB.insert_one(article)
                    doid=str(res.inserted_id)
                    try:
                        migratedTo= ddatabase+'-'+dcollection+'-'+doid
                        if args.updateSource: updateSource(sourceDB,soid,artuuid,migratedTo)
                    except Exception as e:
                        if args.verbose: print("ERR: updateSource write error", soid, e)
                except Exception as e:
                    if args.verbose: print("ERR: write destination error", soid, e)
        else:
            doid = "n/a"

        if args.verbose: print("OK: new item\t",soid,'->',artuuid, scraped['scraper'],downloadTS)
