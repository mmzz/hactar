#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import socket
import datetime
import time
import re
import hashlib
from pymongo import MongoClient
from argparse import ArgumentParser
from hactar.db import logEvent  
from hactar.db import openDB
import constant as k


def load_list(fileName):
    ''' reads a list of items from file, one per line 
        comments excluded '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            lst.append(line.strip())
    return(list(set(lst)))





# #
# MAIN
# #

socket.setdefaulttimeout(10)

A=1
B=7
C=28

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-S", "--status", action="store_true", help="print feed status")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-A", "--periodA", action="store_true", help="time span of first period (default:"+str(A)+"days)") 
parser.add_argument("-B", "--periodB", action="store_true", help="time span of second period (default:"+str(B)+"days)")
parser.add_argument("-C", "--periodC", action="store_true", help="time span of third period (default:"+str(C)+"days)")
parser.add_argument(     "--disable", type=str, help="disable feeds in given filename")

(args) = parser.parse_args()

db = openDB(args.server, args.dbname)


if args.disable:
    disableURLs=load_list(args.disable)
    print(f"loaded {len(disableURLs)} urls to be disabled")


#dbname=args.dbname
#if args.server:
#    server=args.server
#else:
#    server="localhost:27017"

#client = MongoClient(server,serverSelectionTimeoutMS=1)
#db = client[dbname]

#feedsource = "feedsource"
#article = "article"
#eventlog = "eventlog"
#feeditem = "feeditem"
#articleDB = db[article]
#feeditemDB = db[feeditem]

logDB = db[k.EVENTLOG]
feedsourceDB = db[k.FEEDSOURCE]

now = datetime.datetime.now()
#periodC = now.replace(year=now.year - 1)
dateC = now - datetime.timedelta(days=C)
dateB = now - datetime.timedelta(days=B)
dateA = now - datetime.timedelta(days=A)

# ensure indexes are in place
feedsourceDB.create_index('active')
feedsourceDB.create_index('sources')
feedsourceDB.create_index('lastPositive')
feedsourceDB.create_index('oldRating')
feedsourceDB.create_index('rating')

# read feeds collection
if args.disable:
    feeds = feedsourceDB.find({},no_cursor_timeout=False)
else:
    feeds = feedsourceDB.find({"active": True},no_cursor_timeout=False)

needed=['protocol','site','name','rating','active']
now=datetime.datetime.utcnow()
counter={
    'items': 0,
    'invalid': 0,
    'disabled': 0,
    'inactive':0,
    }
if args.status:
    print(f"periods: A>{dateA.date()}, B>{dateB.date()}, C>{dateC.date()}")
for feed in feeds:
    counter['items'] +=1
    missing=[]
    for key in needed:
        if key not in feed.keys():
            missing.append(key)
    if missing:
        print (f"feed misses needed keys {missing}\n{feed}")
        counter['invalid'] +=1
        continue
        
    source=feed['protocol']+"|"+feed['site']+"|"+feed['name']
    rating=feed['rating']

    # classify feed items providing articles in given time spans
    # A: has items since periodA
    # B: has items since periodB
    # C: has items in period C
    # D: "DRY" no articles in periodC

    if ("lastPositive" not in feed.keys()):
        continue
    
    newRating="D"
    if (feed['lastPositive'] > dateC):
        newRating="C"
    if (feed['lastPositive'] > dateB):
        newRating="B"
    if (feed['lastPositive'] > dateA):
        newRating="A"

    if args.status:
        if newRating == "D":
            print(args.dbname+"|"+source, rating,"->",newRating, feed['URL'], feed['lastPositive'])
        else:
            print(args.dbname+"|"+source, rating,"->",newRating, feed['URL'])
        
        continue

    if args.disable:
        if feed['URL'] in disableURLs:
            if feed['active'] == False:
                print(f"inactive {source}, {rating}, {feed['URL']}")
                counter['inactive'] +=1
            else:
                feedsourceDB.update_one({'_id':feed['_id']},
                                    {
                                        '$set': {'active': False}
                                    })
                print(f"disabled {source}, {rating}, {feed['URL']}")
                counter['disabled'] +=1
        continue

    if (rating != newRating):
        feedsourceDB.update_one({'_id':feed['_id']},
            {
                '$set': {
                    'rating': newRating,
                    'oldRating': rating,
                    },
            },
            upsert=True)
        logEvent(args.verbose,logDB,"feedhealth","ok",source,"","","new_rating:"+str(rating)+"->"+str(newRating))
        
print(counter)
