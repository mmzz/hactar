#/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2021-2024 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import os
from datetime import date
from datetime import datetime
from datetime import timedelta
import json
import re
import time
#import urltools
#import urllib.parse
from pymongo import MongoClient
from argparse import ArgumentParser
import odswriter as ods
from pyexcel_xlsx import save_data
from tqdm import tqdm
from sys import argv

import constant as k
from hactar.db import loadRules
from hactar.db import openDB
from hactar.db import logEvent


COVIDS=["sars-cov-2","covid19","sars-cov2","pandemia","covid","corona virus","coronavirus"] # terms related to Covid
RANGE=50 # search range for AND-list (supplements) and NOT-list (complements) in proximity of name 

# terms related to places
CLEANLIST=[" il", " la", " del", " della", " nel", " al", " alla", " nella", " nell'", " dell'", " all'", " viale", "via", "ospedale", "struttura", "palazzo", "via", "piazza", "piazzale", " corso", "ponte", "lungolago", "lungomare", "zona", "quartiere", "largo", "piazzetta", "vicolo" , "aeroporto", "biblioteca", "scuola", "liceo scientifico", "plesso", "liceo", "istituto","dell'istituto","All’istituto","All'istituto", "comprensivo", "alberghiera", "premio", "award"] 


def log(s):
    if args.log:
        print(s)

def verbose(s):
    if args.verbose:
        print(s)

def basename(filepath):
    if '/' in filepath:
        filepath=filepath.split('/')[-1]
    if '.' in filepath:
        filepath=filepath.split('/')[0]
    return(filepath)


def log_uuid(uuid,valid):
    if valid ==  'valid':
        os.write(loguuids,str.encode(f"{uuid}\n"))
    else:
        os.write(loguuids,str.encode(f"#{uuid}\t{valid}\n"))

def debug(*s):
    import sys
    import inspect
    if args.debug :
        frame=sys._getframe(1).f_code.co_name
        stack=inspect.stack()
        if "self" in stack[1][0].f_locals.keys():
            classname=stack[1][0].f_locals["self"].__class__.__name__
        else:
            classname='__main__'

        print(f"#{classname}.{frame}"+" ".join(str(elem) for elem in s))


def load_occupations(fileName):
    ''' reads a tab separated list of items (one per line), 
        of 2 tab-separated fields, 
        builds dict with key= field 0,  value=field1 '''
    dct={}
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            if len(line.strip().split('\t')) == 1:
                continue
            else:
                (k,v)=line.strip().split('\t')
                
            dct.update({k:v})
    return(dct)

        
def load_list(fileName):
    ''' reads a list of items from file, one per line 
        comments excluded '''
    lst=[]
    with open(fileName, "r") as f:
        for line in f:
            if line[0] == '#':
                continue
            lst.append(line.strip())
    return(list(set(lst)))



def a(n):
    # Hypertextual search <name> in spreadsheet output
    SEARCH='https://duckduckgo.com/'
    return(f"=HYPERLINK(\"{SEARCH}?q={n.replace(' ','+')}\",\"{n}\")")



# CLASSES
class logFile(object):
    def __init__(self,file_name):
        self.file_name = file_name
        self.handle=open(self.file_name,'w') 

    def write(self,s):
        self.handle.write(s)

    def close(self):
        self.handle.close()


        
class SpreadSheet:
    ''' generic spreadsheet structure:
                 [ sheet:
                   {
                     [cell,...,cell],
                     [cell,..., cell]
                     ...
                    },
                    sheet: { [c,c,c],[c,c,c]},
                    sheet: { [c,c,c],[c,c,c]},
                 ]
    '''
    def __init__(self):
        self.sheet={}

    def new_sheet(self,sheetname):
        name=''
        forbidden='/\[]*?:'
        for c in sheetname:
            if c in forbidden:
                name+='-'
            else:
                name+=c

        self.sheet.update({name:[]})
        self.current=name

    def writerow(self,row):
        self.sheet[self.current].append(row)
    
    def save_as_xlsx(self,filename):
        import xlsxwriter
        import datetime
        spreadsheet= xlsxwriter.Workbook(filename)
        bold    = spreadsheet.add_format({'bold': True})
        
        for sheet in self.sheet.keys():
            worksheet = spreadsheet.add_worksheet(sheet)
            row=0
            col=0
            maxcol=0
            for line in self.sheet[sheet]:
                for datum in line:
                    if row==0:
                        worksheet.write(row,col,datum,bold)
                    else:
                        worksheet.write(row,col,datum)
                    col+=1
                    maxcol=col
                row+=1
                col=0
            worksheet.autofit()
            worksheet.set_column('B:B',30, bold ,None)
            worksheet.autofilter(0,0,row,maxcol)
            
        spreadsheet.close()

    def add(self,data,header,name):
        '''add matrix to sheet, as is'''
        self.sheet[name]=[header]
        for row in data:
            self.sheet[name]+=[self.filter_boolean(row)]
            
    def include_file(self,fileName):
        ''' add a sheet with all elements from arguments file, one per row,
        for documentation purposes '''
        lines=[]
        content=load_file(fileName)
        for line in content:
            lines.append([line])


        self.sheet.update({self.current: 
            lines,
        })

    # def save(self,fileName):
    #     '''save spreadsheet, all sheets'''
    #     from pyexcel import save_as
    #     from pyexcel_ods3 import save_data
    #     #from pyexcel_xlsx import save_data
    #     import datetime

    #     for i in self.sheet.keys():
    #         debug(f"sheet {i} with {len(self.sheet[i])} items")
    #     save_data(fileName+".xlsx", self.sheet)

class Counter:
    ''' This object stores different measures and counters for each name
    '''
    def __init__(self):
        from collections import OrderedDict
        self.names={}
        #self.headers=[TAG,SCORE,KSCI,SUM,RELSUM,SCOREAVG,SCORESUM,RELPCT,SCIPROB,PUBLICATIONS, SCOPUS, IMDB, CATST]
        self.headers=[TAG,KSCI,SUM,RELSUM,SCIPROB,PUBLICATIONS, SCOPUS, IMDB, CATST]
        #mmzz        self.headers+=CATLABEL
        self.headers+=[OCCUPATION]
        self.headers+=[SCICLASS]
        self.headers+=[WHY]
        self.data = OrderedDict()
        self.n =0
        self.nrel=0

    def add(self,name,score,relevant):
        ''' adds item to counter, including:
        - n: number of items
        - nrel: number of relevant items
        - names: dict of names found, inits structure with:
                 sum: count articles for name
                 relsum: count for relevant articles for name
                 scoresum: sum of scores of all articles
                 scoreavg: average of score (scoresum/sum)
        '''
        self.n+=1
        if relevant==True:
            rel=1
            self.nrel+=1
        else:
            rel=0

        if name in self.names.keys():
            old=self.names[name]
            new={
                SUM:      old[SUM]+1,
                RELSUM:   old[RELSUM]+rel,
                SCORESUM: old[SCORESUM]+score
                }
            new.update({
                RELPCT:   new[RELSUM]/new[SUM],
                SCOREAVG: new[SCORESUM]/new[SUM],
            })
            self.names[name].update(new)
        else:
            self.names[name]={
                SUM: 1,
                RELSUM:   rel,
                SCORESUM: score,
                RELPCT:   rel,
                SCOREAVG: score,
            }
            
    def list_names(self,threshold=5):
        ''' Lists names currently in counter
        '''
        accept=[]
        for n in self.names.keys():
            if self.names[n][SUM]>=threshold:
                accept.append(n)
        return(accept)

    def set_sci(self, name, known, probability, occupation, pubs, publications, celebrity, nonp):
        ''' Adds data to names counter on occupation, pubblications, imdb, etc...
            Classifies according to data in KSCI, SCI, MAYSCI
        '''

        e=self.names[name]

        e[SCIPROB]=probability
        e[OCCUPATION]=occupation
        e[SCICLASS]=sciclass
        e[PUBLICATIONS]=pubs
        e[SCOPUS]=publications
        e[IMDB]=celebrity
        e[KSCI]=known
        e[TAG]=''
        e[WHY]=''
        
        
        score =0
        
        if nonp:
            e[TAG]=k.MAYP
            if e[TAG] == k.MAYP: debug(f"{name} MAYP")
            score-=.5
            
        # name appears ONLY in S&T feeds
        catsum=0
        for c in CATEGORIES:
            catsum+=e[f"cat-{c}"]
        if catsum >0 and e[CATST]/catsum == 1:
            score+=1
            e[WHY]+='onlyST,'
        
        # name has likely has scientific occupation
        if e[SCIPROB] >= 0.75:
            score+=2
            e[WHY]+='sciOccupation,'
            
        # in relevant articles most of the time, 
        if e[SCOREAVG] > 0.5 :
            score+=1
            e[WHY]+='inRelevant,'
            
        # prevalent in relevant articles
        if e[RELPCT] >=0.9:
            score+=1
            e[WHY]+='relPct90%,'
        
        # has relevant number of SCI articles
        if (e[SCORESUM] >= 10) :
            score+=1
            e[WHY]+='highRelArticles,'

        # publications
        if e[SCOPUS] == True:
            score+=1
            e[WHY]+='scopus,'
            
       # publications
        if e[PUBLICATIONS] == True:
            score+=1
            e[WHY]+='publications,'


        # most likely a celebrity
        if e[IMDB] == True:
            score-=2
            e[WHY]+='celeb,'

        # all-caps names are journalists names
        haslower = any([char.islower() for char in name.replace(" ","")])
        if not haslower:
            score -=2
            e[WHY]+='allcaps,'
        
        e[SCORE]=score

        # check if lower, upper bound
        RANGES={
            k.SCI: [3.5,999],
            k.MAYSCI: [2,3.5]}
        for r in RANGES.keys():
            if is_between(score,RANGES[r][0],RANGES[r][1]):
                e[TAG]=r

        # Tag as KSCI, KNON, IGNORE if known or SCI or MAYSCI according to calculated attribution above
        if known != "": # UNKNOWN
            e[TAG]= known # may be KSCI, NOTSCI, IGNORE
            
               
        
    def set_category(self,cats):
        '''build labels for categories, increment if present in article  '''
        e=self.names[name]
        for c in CATEGORIES: # scan all categories
            cat=f"cat-{c}"
            
            if cat not in e.keys(): # init
                e[cat]=0
            if c in cats: # add
                e[cat]+=1
        
    def get_keys(self):
        allkeys=[]
        for e in self.names.keys():
            for ky in self.names[e].keys():
                if ky not in allkeys:
                    ky.append(allkeys)
        return(allkeys)
        
    def print(self,name):
        s=f"{name}\t"
        for l in self.headers:
            s+=f"{self.names[name][l]}\t"
        return(s)


def search(mystring,text,rng):
    # search mystring in text,
    # if match return substring with r chars to the right and l chars to the left of the match
    # else return ""
    l=rng
    r=rng
    t=text.lower()
    found=t.find(mystring.lower())

    if found != -1:
        left=len(text[0:found])
        right=len(text[found+len(mystring):-1])
        if left < l: l = left
        if right < r: r= right
        t=text[found-l:found+r+len(mystring)]
        if len(t)<len(mystring):
            print(f"ERR SEARCH:{mystring},{t}/{text}")
        return(t)
    else:
        return("")

# clecks if entity is immediately preceded by words in deny-list
def precededBy(text,entity,wlist):
    for word in wlist:
#        if text.lower().find(" "+word.lower()+" "+entity.lower()) != -1:
        if text.lower().find(word.lower()+" "+entity.lower()) != -1:
            return word
    return False

    
# Check if words in andlist are present in a given range of string
def hasTerm(text,string,terms):
    # get substring in the surreoundings [-RANGE,+RANGE] of text  
    subtext=search(string,text,RANGE)
    if subtext != "":
        # look for each term in list in substring
        for t in terms:
            if subtext.find(" "+t) != -1:
#            if subtext.find(" "+t+" ") != -1:
                return(t)
    return("")

# return true if matches one in list of strings 
def isCovidPositive(text,necb):
    positive=False
    for c in COVIDS:
        if c in necb:
            positive=True
            break
        else:
            t=text.lower()
            if t.find(c) != -1:
                positive=True
                break
    return(positive)

# update counters list for entity e as a dictionnary 
def countScore(e,score,assessment,covidPositive,mypaper):
    if not isinstance(score, float):
        return()
    
    counters[ALL]+=1
    allEntities.e[e][RELEVANT]=assessment
    allEntities.e[e][SCORESUM]+=score
    allEntities.e[e][COUNT]+=1

    #assessment
    if assessment==RELEVANT:
        counters[RELEVANT]+=1
        allEntities.e[e][SCORESUM_R]+=score
        allEntities.e[e][COUNT_R]+=1
        if covidPositive:
            counters[COVIDPOSITIVE]+=1
            allEntities.e[e][COVID_R]+=1
            allEntities.e[e][COVID]+=1
    elif assessment==NOTRELEVANT:
        counters[NOTRELEVANT]+=1
        allEntities.e[e][SCORESUM_NR]+=score
        allEntities.e[e][COUNT_NR]+=1
        if covidPositive:
            counters[COVIDPOSITIVE]+=1
            allEntities.e[e][COVID_NR]+=1
            allEntities.e[e][COVID]+=1
    else:
        print(f"ERROR{e} assessment:{assessment} score:{score} covid:{covidPositive}")

    #newspapers: count, for each name and newspaper, total occurrences of name, occurrences in relevant aritcles, in covid articles and both covid and relevant aticles
    if mypaper in newspapers.keys():

        if e not in newspapers[mypaper].keys():
            newspapers[mypaper].update({
                e: {
                    COUNT:1,
                    RELEVANT:0,
                    COVIDPOSITIVE:0,
                    COVID_R:0}})
        else:
            newspapers[mypaper][e][COUNT]+=1
        if assessment==RELEVANT:
            newspapers[mypaper][e][RELEVANT]+=1
        if covidPositive:
            newspapers[mypaper][e][COVIDPOSITIVE]+=1
        if assessment==RELEVANT and covidPositive:
            newspapers[mypaper][e][COVID_R]+=1
        
    else:
        print("ERR: newspapers missing key",e,mypaper,item[k.SOURCES])



class Entities:
    def __init__(self):
        from hactar.db import openDB
        OCCUPATIONSFILE="Entities/SCI-occupations"
        EVERGREENSFILE="Entities/SCI-evergreens"
        IGNOREFILE="Entities/SCI-ignore"


        self.sci_occupations=load_occupations(OCCUPATIONSFILE)
        self.sci_ignore=set(load_list(IGNOREFILE)) # known scientist to be ignored in countings
        self.sci_evergreens=set(load_list(EVERGREENSFILE)) # known scientist to be ignored in countings
        debug(f"known occupations file: {OCCUPATIONSFILE} has {len(self.sci_occupations)} items")

        self.people   =openDB(args.server, k.ENTITIES)[k.PEOPLE]
        self.maypeople=openDB(args.server, k.ENTITIES)[k.MAYPEOPLE]
        self.nonpeople=openDB(args.server, k.ENTITIES)[k.NONPEOPLE]

        self.e = {}
        

    def load_entities(self,filename):
        ''' Load entities from file, with  a distinct syntax  '''
        ''' fullName,sciclass,shortName,alias,supplement,complement'''
  
        SEP=","
        SUBSEP=" "

        with open(filename) as entitiesFile:
            for line in entitiesFile:
                if line[0] == '#':
                    continue
                line=line.strip()
                try:
                    (fullName,sciclass,shortName,alias,s,c)=line.rsplit(SEP,5)
                except Exception as e:
                   debug (f"{e} at {line}")
                   exit()

                complement=c.split(SUBSEP)
                supplement=s.split(SUBSEP)
                if sciclass== '':
                    sciclass=None
                                          
                self.e.update({
                    fullName: {
                        SHORTNAME: shortName,
                        ALIAS: alias,
                        SUPPLEMENT:supplement,
                        COMPLEMENT:complement,
                        SCICLASS:sciclass,
                        # ALL
                        COUNT: int(0),
                        SCORESUM: float(0.0),
                        AVERAGE: float(0.0),
                        COVID:int(0),
                        # RELEVANT
                        COUNT_R: int(0),
                        SCORESUM_R: float(0.0),
                        AVERAGE_R: float(0.0),
                        COVID_R:int(0),
                        # NON_RELEVANT
                        COUNT_NR: int(0),
                        SCORESUM_NR: float(0.0),
                        AVERAGE_NR: float(0.0),
                        COVID_NR:int(0),
                        # 
                        #                       SCORES:[],
                        NEWSPAPERS:{},
                    }})

                '''build full list of names'''
                namelist=[fullName]
                for n in [shortName,alias]:
                    if n != '':
                        namelist.append(n)
                self.e[fullName][ALLNAMES] = set(namelist)


    def list(self):
        return (self.e.keys())

    def get_supplement(self,fullname):
        if self.e[fullname][SUPPLEMENT]: 
            return(self.e[fullname][SUPPLEMENT])
        else:
            return(None)
        
    def get_complement(self,fullname):
        if self.e[fullname][COMPLEMENT]: 
            return(self.e[fullname][COMPLEMENT])
        else:
            return(None)
        
    def sci_probability(self,name):
        '''
        - searches name in people database, gets list
        - returns 
          the ratio between all occupations and scientist occupations 
          the list of sci-occupations as a comma-separated list
          if all occupations are in the same class, the sci-occupation class, 'MANY' if not
        - returns -1 if no sci occupation
        '''
        count=0
        sci=[]
        found=0
        occupations=[]
        sciclasses=[]
        sciclass=''

        for name in self.people.find({k.NAME: name}):
            count+=1
            sci+=list(set(name[k.OCCUPATIONS]).intersection(set(self.sci_occupations.keys())))
            if len(sci) >0:
                found+=1
                occupations+=sci
            for o in occupations:
                sciclasses.append(self.sci_occupations[o])

        # return ratio, list of occupations and class
        if count>0:
            # set occupation class if all occupations belong to the same class
            if  len(set(sciclasses)) == 1:
                sciclass=sciclasses[0]
            elif len(set(sciclasses)) > 1:
                sciclass='MANY'
            
            return(found/count,",".join(map(str,set(occupations))),sciclass)
        else:
            return(-1,"", "")

    def is_evergreen(self,name):
        if name in self.sci_evergreens:
            return(True)
        else:
            return(False)
    def is_ignore(self,name):
        if name in self.sci_ignore:
            return(True)
        else:
            return(False)

    def get(self,name):
        return(self.e[name])

    def frequencies_sheet(self,sheet,sheet_name,relevance):
        if sheet_name == ALL:
            evergreen_sheet=False
        elif sheet_name == EVERGREEN:
            evergreen_sheet=True
        else:
            exit(f"sheet name [{sheet_name}] must be {EVERGREEN} or {ALL}")
                
        if relevance == RELEVANT:
            sheet_name+="-Relevant"
            count=COUNT_R
            scoresum=SCORESUM_R
            covid=COVID_NR
        elif relevance == NOTRELEVANT:
            sheet_name+="-NotRelevant"
            count=COUNT_NR
            scoresum=SCORESUM_NR
            covid=COVID_NR
        else:
            count=COUNT
            scoresum=SCORESUM
            covid=COVID
                
        sheet.new_sheet(sheet_name)
        sheet.writerow(HEADERS)
        for name in self.list():
            if self.is_ignore(name):
                continue
            if self.is_evergreen(name) and not evergreen_sheet:
                continue
            if not self.is_evergreen(name) and evergreen_sheet:
                continue
            # if both entity and sheet are evergreen or both are not evergreen 
            (prob,occupation,sciclass)=self.sci_probability(name)        
            # if entity has given SCICLASS, this value overrides the calculated one 
            if self.e[name][SCICLASS] != None:
                sciclass=self.e[name][SCICLASS]

            if self.e[name][count]>0:
                scoreaverage=round(self.e[name][scoresum]/self.e[name][count],3)
            else:
                scoreaverage=None
                    
            sheet.writerow([
                self.e[name][SHORTNAME],
                a(name),
                sciclass,occupation,
                self.e[name][covid],
                self.e[name][count],
                round(self.e[name][scoresum],3)
                ,scoreaverage])

    

class Article:
    def __init__(self,item):

        self.newspaper=item[k.SOURCES][0].split("|")[1]

        if k.DONTINDEX in item.keys():
            self.valid=False
            #debug(f"{item[k.UUID]}\tdontindex")
        elif k.DATESPUBLISHED not in item.keys() or (not isinstance(item[k.DATESPUBLISHED],list)) or len(item[k.DATESPUBLISHED]) == 0:
            self.valid=False
            debug(f"{item[k.UUID]}\tno{k.DATESPUBLISHED}")
        elif k.PEOPLE not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnopeople")
        elif k.YEAR not in item.keys():
            self.valid=False
            debug(f"{item[k.UUID]}\tnoyear")
        elif k.ASSESSMENTS not in item.keys():
            debug(f"{item[k.UUID]}\tnoassess")
            self.valid=False
        elif k.STACKEDST not in item[k.ASSESSMENTS].keys():
            debug(f"{item[k.UUID]}\tnostackedst")
            self.valid=False
        elif self.newspaper == "":
            debug(f"{item[k.UUID]}\tno_newspaper")
            self.valid=False
        elif k.NER not in item.keys():
            debug(f"{item[k.UUID]}\tno_NER")
            self.valid=False

        else:
            self.valid=True
            #debug(f"{item[k.UUID]}\tvalid")
            self.score=item[k.SCORES][k.STACKEDST]
            if item[k.ASSESSMENTS][k.STACKEDST] == k.RELEVANT:
                self.relevant=True
                self.assessment=k.RELEVANT
                if self.score >= 0.95 :
                    self.relClass = 'VeryHigh'
                elif self.score >= 0.85 :
                    self.relClass = 'High'
                elif self.score >= 0.75 :
                    self.relClass = 'Medium'
                elif self.score >= 0.5 :
                    self.relClass = 'Low'
            else:
                self.relevant=False
                self.assessment=k.NOTRELEVANT
                self.relClass = 'None'
            self.names=item[k.PEOPLE]
            self.year=item[k.YEAR]
            self.categories=item[k.SOURCECATEGORIES]
            self.text=item[k.CLEANTEXT]
            self.uuid=item[k.UUID]
            self.datepublished=item[k.DATESPUBLISHED][0]
            self.downloadts=item[k.DOWNLOADTS]



            
        # skip items with issues or less than 100 words
        if item[k.CLEANTEXTWORDS] < 100 or k.CLEANTEXTISSUES not in item.keys() or item[k.CLEANTEXTISSUES]!=[] :
            debug(f"{item[k.UUID]}\tshort")
            self.valid=False

        if self.valid:
            self.entities=set(item[k.NER]['body'].keys())
            if isCovidPositive(item[k.CLEANTEXT],item[k.NECB]):
                self.covidPositive=True
            else:
                self.covidPositive=False
        

            
    def get_entity_context(self,entity):
        self.context=search(self.text,entity,50,50)



# #
# MAIN
# #

if __name__ == "__main__":

    # LABELS
    ALIAS="alias"
    ALL="All"
    ALLNAMES='allNames'
    AVERAGE="average"
    AVERAGE_NR="average_notrelevant"
    AVERAGE_R="average_relevant"
    COMPLEMENT="complement"
    CORPUS="corpus"
    COUNT="count"
    COUNT_NR="count_notrelevant"
    COUNT_R="count_relevant"
    COVIDPOSITIVE="covidPositive"
    COVIDNEGATIVE="covidNegative"
    COVID="covid"
    COVID_NR="covid_nr"
    COVID_R="covid_r"
    EVERGREEN="Evergreen"
    EVERGREENS="Evergreens"
    FULLNAME="fullName"
    NEWSPAPERS="newspapers"
    #SCORES="scores"
    SCICLASS="sciClass"
    SCORESUM="scoresum"
    SCORESUM_NR="scoresum_notrelevant"
    SCORESUM_R="scoresum_relevant"
    SHORTNAME="shortName"
    SUPPLEMENT="supplement"
    # database labels
    NOTRELEVANT=k.NOTRELEVANT
    RELEVANT=k.RELEVANT
    VALID="valid"

    # FILES:
    ENTITIESFILE="Entities/SCI-entities"



    parser = ArgumentParser()
    parser.add_argument("dbname",          type=str,help='database name')
    parser.add_argument("-s", "--server",  dest="server",  help="mongodb server address:port")
    parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
    parser.add_argument("--newspapers",    action="store_true", help="include newspapers count")
    parser.add_argument("-l", "--log",     action="store_true", help="log operations to filename.log (filename from -o -p and --ods switches)")
    parser.add_argument("-vv", "--veryverbose", action="store_true", help="accepts and rejects in .log file")
    parser.add_argument("-D", "--debug",     action="store_true", help="log anomalies to stdout")
    parser.add_argument("-S", "--search",  type=str, help="mongo query dict")
    parser.add_argument("-p", "--persons", type=str, help=f"persons file: {FULLNAME}, {SHORTNAME}, {ALIAS}, {SUPPLEMENT}, {COMPLEMENT}, {SCICLASS} defaults to {ENTITIESFILE}")
    parser.add_argument("--ods",           action="store_true", help="output to ods file (libreoffice spreadsheet)")
    parser.add_argument("-N", "--nitems",  type=int, help=f"limit to first N items")
    parser.add_argument("-y", "--year",  type=str, help=f"articles from  one year")
    parser.add_argument("-f", "--fromdate",type=str, help=f"period start date Y-m-d")
    parser.add_argument("-t", "--todate",  type=str, help=f"period end date Y-m-d")
    parser.add_argument("--uuid",  type=str, help=f"take articles from uuids file")


    (args) = parser.parse_args()

    #db=openDB(args.server,args.dbname)
    #articleDB = db[k.ARTICLE]




    
    newspapers={}
    allEntities=Entities()

    counters={
        CORPUS:0,
        VALID:0,
        RELEVANT:0,
        NOTRELEVANT:0,
        COVIDPOSITIVE:0,
        COVIDNEGATIVE:0,
        ALL:0,
    }

    # define filename basename for all outputs
    filename="countNames-"
    if args.year:
        filename+=f"{basename(args.year)}"
    elif args.uuid:
        filename+=f"{basename(args.uuid)}"
    else:
        filename="ALL"

    if args.persons:
        filename+=f"-{basename(args.persons)}"
        allEntities.load_entities(args.persons)
    else:
        ENTITIESFILE="Entities/SCI-entities"
        allEntities.load_entities(ENTITIESFILE)

    if args.fromdate and args.todate:
        filename+=f"-{args.fromdate}-{args.todate}"




    entities=set(allEntities.list())
    articles=openDB(args.server, args.dbname)[k.ARTICLE]

    # find items in db

    if args.veryverbose:
        llog = logFile(f"{filename}.csv")


    # build search string
    srch=[]
    if args.search:
        srch.append(json.loads(args.search))
    elif args.fromdate and args.todate:
        fromdate = datetime.combine(datetime.strptime(args.fromdate, '%Y-%m-%d').date(), datetime.min.time())
        todate   = datetime.combine(datetime.strptime(args.todate, '%Y-%m-%d').date(), datetime.max.time())
        days=  todate-fromdate
        if fromdate.year == todate.year:
            srch.append({k.YEAR: fromdate.year})
        else:
            srch.append({k.YEAR: {'$in': list(range(fromdate.year,todate.year+1,1))}})
        datefield=f"{k.DATESPUBLISHED}.0"
        datefield=f"{k.DOWNLOADTS}"
        srch.append({datefield: {"$gte": fromdate, "$lt": todate}})

        if args.uuid:
            uuids=set(load_list(args.uuid))
            verbose(f"read {len(uuids)} uuids from {args.uuid}")
        else:
            verbose(f"querying {query}")
            uuids=[]
            nitems=0
            all=articles.find(query).batch_size(1000)
            n=articles.count_documents(query)
            for item in tqdm(all,total=n):
                # limit run to first args.nitems items.
                if args.nitems:
                    nitems+=1
                    if nitems > args.nitems:
                        break

                uuids.append(item[k.UUID])

    elif args.year:
        srch.append({k.YEAR: int(args.year)})
        days=365
    else:
        days=''

    ## MAIN LOOP over articles
    nitems=0
    nvalid=0


    # make article query: get uuids of articles
    if args.uuid:
        uuids=set(load_list(args.uuid))
        verbose(f"read {len(uuids)} uuids from {args.uuid}")
    else:
        if len(srch) == 1:
            query=srch[0] 
        else:
            query={"$and": srch}
        verbose(f"querying {query}")
        uuids=[]

        verbose(f"getting articles")
        all=articles.find(query,no_cursor_timeout=False,batch_size=10000)
        total=articles.count_documents(query)
        for item in tqdm(all,total=total):
            uuids.append(item[k.UUID])
            nitems+=1
            if args.nitems:
                if nitems > int(args.nitems):
                    break

        verbose(f"read {len(uuids)} uuids from query {query}")
    verbose(f"Command line: {argv}")
    verbose(f"Arguments: {args}")
    verbose(f"Search:    {articles}:{query}")
    verbose(f"Span:    {days}")


    # loop over uuids articles
    verbose(f"processing {len(uuids)} articles")
    uuidFile=logFile(f"{filename}.uuids")
    for uuid in tqdm(uuids):
        #article=Article(articles.find_one({k.UUID: uuid},no_cursor_timeout=True))
        article=Article(articles.find_one({k.UUID: uuid}))
        

        if not article.valid:
            continue
        
        # log valid uuids in uuids file
        uuidFile.write(f"{article.uuid}\n")


        itemHasEntities = False
        # limit run to first args.nitems items.
        nitems+=1
        counters[CORPUS]+=1

        # update list of newspapers
        if article.newspaper not in newspapers.keys():
            newspapers.update({article.newspaper: {} })

        counters[VALID]+=1

        # log Item as recorded
        summary=f"{article.uuid}\t{article.downloadts}"
        scores=f"{article.score}, {article.relClass}, {article.covidPositive}, {article.newspaper}"

        # find entities in list of article names
        for name in allEntities.list():

            common = allEntities.e[name][ALLNAMES] & article.entities # common elements between names list and item entities

            doCount=""
            for entity in common: # for each entity in common between item named entities and names in file

                # search for entity in article text
                strMatch = search(entity,article.text,RANGE)

                # No match - should not happen
                if strMatch == "":
                    if args.veryverbose:
                        llog.write(f"{summary}\t{scores}\t!\t{entity}\tGONE from text\n")
                    continue

                # CLEANLIST-list match
                cleanMatch = precededBy(strMatch,entity,CLEANLIST)
                if cleanMatch:
                    if args.veryverbose:
                        llog.write(f"{summary}\t{scores}\t❎\t{name}\t{entity} CLEAN {cleanMatch}\t\"{strMatch}\"\r\n")
                    continue

                # NOT-list match
                notList=allEntities.get_complement(name) # NOT-list: SHOULD have NO items from this list to have a match
                if notList:
                    notMatch = hasTerm(article.text,entity,notList)
                    if notMatch:
                        if args.veryverbose:
                            llog.write(f"{summary}\t{scores}\t❎\t{name}\t{entity} NOT {notMatch}\t\"{strMatch}\"\r\n")
                        continue

                # AND-list match
                andList=allEntities.get_supplement(name) # AND-list: SHOULD have AT LEAST 1 item in this list to have a match
                if andList:
                    andMatch = hasTerm(article.text,entity,andList)
                    if andMatch:
                        doCount=f"{entity} AND {andMatch}"
                        itemHasEntities=True
                        if args.veryverbose:
                            llog.write(f"{summary}\t{scores}\t✔\t{name}\t{doCount}\t\"{strMatch}\"\r\n")
                        continue

                # Plain match, no AND or NOT lists
                if args.veryverbose:
                    llog.write(f"{summary}\t{scores}\t✔\t{name}\t{entity}\t\"{strMatch}\"\r\n")
                doCount=entity
                itemHasEntities=True
                continue
            # avoid to count twice shortnames, aliases and multiple full names
            if doCount != "":
                countScore(name,article.score,article.assessment,article.covidPositive,article.newspaper)
        log(f"{summary}\t{itemHasEntities}\t{scores}\n")
    log(f"#END valid/items {counters[VALID]}/{nitems}\n")
    uuidFile.close()        


    ## PRINTOUT in Spreadsheet
    HEADERS=["Shortname","Full Name", "Sci class", "occupation", "Covid articles", "n articles",  "Σ articles score", "articles score average"]
    if args.ods:
        print(f"writing {filename}")
        sheet=SpreadSheet()
        
        # frequency tables for entities according to different classes and relevance criteria
        allEntities.frequencies_sheet(sheet,ALL,"")
        allEntities.frequencies_sheet(sheet,ALL,RELEVANT)
        allEntities.frequencies_sheet(sheet,ALL,NOTRELEVANT)
        allEntities.frequencies_sheet(sheet,EVERGREEN,"")
        allEntities.frequencies_sheet(sheet,EVERGREEN,RELEVANT)

            
        ## NEWSPAPERS
        if args.newspapers:
            # loop over newspapers
            papers=list(newspapers.keys())
            papers.sort()
            # one sheet for newspaper
            for paper in papers:
                sheet.new_sheet(paper)
                # labels row
                sheet.writerow(["Entity",COUNT,RELEVANT,COVIDPOSITIVE,COVID_R])
                # one row for entity
                for name in allEntities.list():
                    if allEntities.is_evergreen(name):
                        continue

                    if name not in newspapers[paper].keys():
                        row=[a(name),0,0,0,0]
                    else:
                        # one column for properties
                        row=[a(name)]
                        for label in newspapers[paper][name].keys():
                            row.append(newspapers[paper][name][label])

                    sheet.writerow(row)

        ## SUMS        
        sheet.new_sheet("SUMS")
        sheet.writerow([counters[CORPUS], "items in corpus"])
        sheet.writerow([counters[VALID], "valid articles in corpus"])
        sheet.writerow([counters[ALL], "ARTICLES containing registered named entities"])
        sheet.writerow([counters[RELEVANT], "RELEVANT ARTICLES (stacked ST score)"])
        sheet.writerow([counters[NOTRELEVANT], "NOT RELEVANT ARTICLES (stacked ST score)"])
        sheet.writerow([counters[COVIDPOSITIVE], "COVID ARTICLES"])

        ## METADATA
        sheet.new_sheet("META")
        sheet.writerow(["program:",str(argv[0])])
        sheet.writerow(["extraction date:",str(datetime.now())])
        sheet.writerow(["command line:",str(argv)])
        sheet.writerow(["args:",str(args)])
        sheet.writerow(["query:",str(srch)])
        if args.fromdate and args.todate:
            sheet.writerow([f"date range: {str(fromdate)} - {str(todate)}: {str(days)} days"])
            sheet.writerow([f"Narticles: {str(nitems)}, Nvalid: {str(counters[VALID])} "])

        sheet.new_sheet("ENTITIES")
        sheet.writerow(["entity","sci class","shortname","alias","AND list","NOT list"])
        for name in allEntities.list():
            sheet.writerow([a(name),
                            str(allEntities.e[name][SCICLASS]),
                            str(allEntities.e[name][SHORTNAME]),
                            str(allEntities.e[name][ALIAS]),
                            str(allEntities.e[name][SUPPLEMENT]),
                            str(allEntities.e[name][COMPLEMENT])])

        ## CLOSE AND WRITE FILE
        sheet.save_as_xlsx(f"{filename}.xlsx")

    else:
        import pprint
        pp = pprint.PrettyPrinter(depth=4)
        pp.pprint(allEntities)
        pp.pprint(counters)
        pp.pprint(newspapers)

    if args.veryverbose:
        llog.close()
