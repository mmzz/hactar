#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import validators
import lxml
import datetime
import bz2
import hashlib
import uuid
import re
import time

from pymongo import MongoClient
#from lxml.html.clean import Cleaner
from lxml_html_clean import Cleaner
from lxml import etree, html
from argparse import ArgumentParser
from hactar.net import getArticle
from hactar.net import geturl
from hactar.db import storeLineHash
from hactar.db import storeArticle
from hactar.db import logEvent
from hactar.db import openDB
from hactar.db import markFeedActivity
from hactar.strings import cleanText
from hactar.process import processArticle
from hactar.process import getMeta
import constant as k

from urllib.parse import urlparse
from urllib.parse import urljoin
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector


def cleanpage(html):
    # cleaner setup
    cleaner = Cleaner()
    cleaner.html = True
    cleaner.page_structure = False
    cleaner.meta = False
    cleaner.safe_attrs_only = False
    cleaner.links = False
    cleaner.javascript = True # activate the javascript filter
    cleaner.style = True      #  activate the styles & stylesheet filter
    cleaner.links = False
    cleaner.frames = True
    cleaner.embedded = True
    cleaner.comments = True
    cleaner.annoying_tags = True
    cleaner.inline_style = True
    cleaner.page_structure = False
    #	cleaner.remove_tags = ['b','img','h']
    cleaner.kill_tags = ['img','script']
	
    #invoke cleaner
    try:
        content=cleaner.clean_html(html)
    except:
    #error: ValueError: Unicode strings with encoding declaration are not supported. Please use bytes input or XML fr 
        content = ""
    return content

def getInternalLinks(html,baseurl):
    internalLinks=[]
    xpath='//a[@href]'
    xpath='//@href'
    sel=Selector(text=html, type="html") 
    links=sel.xpath(xpath).extract() 
    if links: 
        for i in links: 
            i=i.replace("\n", "") 
            i = urljoin(baseurl, i)
            if i.startswith(baseurl):
               if i not in internalLinks:
                   internalLinks.append(i)
               if args.debug: print("internal ", i) 
            else:
               if (validators.url(i)):
                    if args.debug: print("external ", i) 
               else:
                    if args.debug: print("rejected ",i)
    return(internalLinks) 

def getLinks(url):
        links=[]
        parsed=urlparse(url)
        scheme=parsed.scheme
        netloc=parsed.netloc
        baseurl=scheme+'://'+netloc
        (html,http,headers,err)=geturl(url)
        if (http == "200") and ("text/html" in headers['Content-Type'].lower()):
            html=cleanpage(html)
            links=getInternalLinks(html,baseurl)
        else:
            logEvent(args.verbose,logDB,"crawl","warn","","",link,"getErr:"+str(err))
            
        return(links)



def pageRatio(url,html):
    
        ratio=-10
        parameter=500
        textxpath="//*[((p) or (a) or (div)) ]/node()/text()"
        dashes = url.count('_')
        dashes = dashes + url.count('-')
    
        # URL metrics
        urlLen = len(url)
        # page content metrics
        html=cleanpage(html)
        sel=Selector(text=html, type="html")
        text= sel.xpath(textxpath).extract()
        textLen = len(text)
        # links metrics
        links=len(getInternalLinks(html,url))
        
        if links > 0: 
            ratio=((textLen/links)*urlLen*--dashes)/parameter
            ratio = ratio -1

        return(ratio)


##
##MAIN
##



parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-u", "--url", dest="url", help="load one specific url")
parser.add_argument("-d", "--debug", action="store_true", help="debug extra output")
parser.add_argument("-t", "--train", action="store_true", help="train url hash db")
parser.add_argument("-S", "--omitSource", action="store_true", help="do not archive source html")

(args) = parser.parse_args()

if args.url:

    db=openDB(args.server,args.dbname)

    # fetch a single URL for testing purpose
    ts = datetime.datetime.utcnow()
    print("ITEM: ",args.url)
    try:
        art=getArticle(args.url)
    except Exception as e:
        m=re.match(r'^Article.*failed with(.*)Client Error:(.*) for url:.*$',str(e))
        if m: 
           print(m.group(1),m.group(2))
        else:
           print(str(e))
        quit()


    if art:
        # extract metadata
        (title,summary,author,published)=getMeta(art.html)
        meta=({
            'published': published,
            'title': title,
            'source': "manual",
            'author': author,
            'summary': summary,
	    'inHomePage': False
        })
            
        article=processArticle(meta,art,None,None,None,db,args.verbose)
        print(article)
        print("published ",published,"/",art.publish_date,"/",ts)

else:

    db=openDB(args.server,args.dbname)
    feedsourceDB = db[k.FEEDSOURCE]
    articleDB = db[k.ARTICLE]
    logDB = db[k.EVENTLOG]

    if args.omitSource:
        archiveDB = None
    else:
        now = datetime.datetime.now()
        archiveDB = db[k.HTML+str(now.year)]        

    urlhashDB = db[k.URLHASH]
    urlhashDB.create_index('line', unique=True)

    protocol="http"
    items=feedsourceDB.count_documents({"active": True, "protocol":protocol})
    if args.debug: print(f"crawl:{args.dbname}/{k.FEEDSOURCE} {protocol} has {items} items")
    # mmzz 2024 may: added batcgh size to avoid cursor timeouts
    feeds = feedsourceDB.find({"active": True, "protocol":protocol},no_cursor_timeout=False,batch_size=10000)

    for feed in feeds:
        oid=feed['_id']
        url=feed['URL']
        site=feed['site']
        name=feed['name']
        if 'homePage' in feed and feed['homePage'] == True:
            inHomePage=True
        else:
            inHomePage=False

        source=protocol+'|'+site+'|'+name
        if args.debug: print("crawl: FEED: ",site+"|"+name)

        # get links in base page
        (feedLinks)=getLinks(url)
        for link in feedLinks:
            if args.train:  
                # "train": populate url frequency collection
                storeLineHash(urlhashDB,link)
                continue

            # skip links pointing to images, pdf, videos, etc
            if link.lower().strip().endswith(('.png','.jpg','.gif','.mpg','.mpeg','.xml','.txt','.pdf','.doc','.docx','.xls','.xlsx','.js')):
                continue
            
            # fetch article from link in page only if its URL frequency is below threshold
            samelink=urlhashDB.find_one({"line": link})
            if samelink: 
                count = samelink['count']
                if count > 10:
                    continue

            # fetch page from link
            html = ''
            try:
                art=getArticle(link)
            except Exception as e:
                    m=re.match(r'^Article.*failed with(.*)Client Error:(.*) for url:.*$',str(e))
                    if m: 
                        logEvent(args.verbose,logDB,"crawl","warn",source,"",link,"skip_on_err:"+str(m.group(1))+str(m.group(2)))
                        markFeedActivity(feedsourceDB,protocol,site,name,False)
                    else:
                        logEvent(args.verbose,logDB,"crawl","warn",source,"",link,"skip_on_err:"+str(e))
                        markFeedActivity(feedsourceDB,protocol,site,name,False)
                    storeLineHash(urlhashDB,link)
                    continue

            # avoid binary files as PDF or fonts
            try:
                art.text[0:10].encode('UTF-8')
            except UnicodeEncodeError as e:
                logEvent(args.verbose,logDB,"crawl","warn",source,"",link,"not_UTF-8:"+str(e))
                
            if art:
                # populate url frequency collection
                storeLineHash(urlhashDB,link)

                # extract metadata
                (title,summary,author,published)=getMeta(art.html)

                if type(published) == datetime.datetime:
                    published.replace(tzinfo=None) # 20210921 - mmzz MongoDB strips TZ info. Hash values mor metadata should be calculated without TZ

                meta=({
                    'published': published,
                    'title': title,
                    'source': source,
                    'author': author,
                    'summary': summary,
		    'inHomePage': inHomePage
                })
            
                ok=processArticle(meta,art,articleDB,archiveDB,logDB,db,args.verbose)
                # logEvent(args.verbose,logDB,"crawl","ok",source,"",art.url,"new_item")
                markFeedActivity(feedsourceDB,protocol,site,name,True)

            else:
                logEvent(args.verbose,logDB,"crawl","err",source,"",url,"skip_on_missing_article")
                markFeedActivity(feedsourceDB,protocol,site,name,False)
                storeLineHash(urlhashDB,link)

                continue

            if args.debug:
                 print("\nITEM: ",link)
                 if title: print("TTL: ",title.encode('utf-8'))
                 if author: print("AUT: ", author)
                 print("DATE: ",published)
#                 print ("UUID:",item[k.UUID])
            time.sleep(0.3)

