#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import codecs
import bson
import bz2
import os
import datetime
import re
import validators
from bson import Binary
from pymongo import MongoClient
from argparse import ArgumentParser


parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("collection", type=str,help='collection name')
parser.add_argument("field", type=str,help='field name')
parser.add_argument("content", type=str,help='field name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")

(args) = parser.parse_args()
if (not args.dbname) :
        print(sys.argv[0], ": database name argument needed")
        quit()
dbname=args.dbname

if args.server:
       server=args.server
else:
       server="localhost:27017"

client = MongoClient(server)
db = client[dbname]
now = datetime.datetime.now()

DB = db[args.collection]
#searchstring="/"+args.content+"/"
#all = DB.find({args.field: searchstring},no_cursor_timeout=False)
all = DB.find({args.field: {'$regex': args.content}},no_cursor_timeout=False)
count = DB.count_documents({args.field: {'$regex': args.content}})
print ("found",count,"items while searching for \""+args.content+"\" in",server+"/"+dbname+"/"+args.field)

for item in all:
    oid=str(item['_id'])
    print (oid,str(item[args.field]))
    if args.verbose:
       for k in item.keys():
           print((str(k).upper())+": "+ str(item[k])+u"\n")
