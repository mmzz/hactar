#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import datetime
import json
import time
from pymongo import MongoClient
from argparse import ArgumentParser



# #
# MAIN
# #

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port")
parser.add_argument("-v", "--verbose", action="store_true", help="be chatty")
parser.add_argument("-w", "--write", action="store_true", help="write to db")
parser.add_argument("-W", "--overwrite", action="store_true", help="override sourceCategories field if present")
parser.add_argument("-f", "--file", dest="categories_file",  help="json file with list of categories")


(args) = parser.parse_args()
dbname=args.dbname

if args.server:
    server=args.server
else:
    server="localhost:27017"


client = MongoClient(server)
db = client[dbname]
now = datetime.datetime.now()
articleDB = db["article"]
feedsourceDB = db["feedsource"]
fc=[]


# load feedclass from file 
with open(args.categories_file) as json_file:
    data = json.load(json_file)
    for item in data:
        for feed in item['feeds']:    
           f= (feed, item['id'])
           fc.append(f)
feedClass=dict(fc)
print(fc)


if args.overwrite:
    all = articleDB.find({},no_cursor_timeout=False)
else:
    all = articleDB.find({'sourceCategories': {'$exists': False}},no_cursor_timeout=False)

for item in all:
    sc=[]
    for s in item['sources']:
        if s == "": continue
        s=s.replace("\'","")
        if s in feedClass.keys():
            c=feedClass[s]
            sc.append(c)
        else:
            print(item['uuid'], "#WARN: missing class for feed source", s)
    fcc = dict()
    for i in sorted(sc):
        fcc[i] = fcc.get(i, 0) + 1

    # set an unique category for each article
    # skip articles with more than 2 categories
    categories = list(fcc.keys())
    if "IGN" in categories:
        categories.remove("IGN")

    if len(categories) == 0:
        chosenCategory="NONE"
    elif len(categories) > 2:
        chosenCategory="NONE"
    elif len(categories) == 2: # articles with two categories: 
        if "HOME" in categories:
            categories.remove("HOME")
            chosenCategory=categories[0]
        else:
            chosenCategory="NONE"           # skip if both categories different from HOME
    else:  # len(categories) == 1:
        chosenCategory=categories[0]    

        
    if args.write:
        oid=item['_id']
        articleDB.update_one( { '_id': oid}, {
            '$set': {
                'sourceCategories': sc,
                'chosenCategory': chosenCategory
            }
        }, upsert=True)
    if args.verbose:
        print(f"{item['uuid']} {list(fcc.keys())} : {chosenCategory}")

