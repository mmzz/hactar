#!/usr/local/bin/python3.7
#-*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import socket
import codecs
import datetime
import json
import time
import re
from time import mktime
from pymongo import MongoClient
#from lxml.html.clean import Cleaner
from argparse import ArgumentParser


from hactar.net import getArticle
from hactar.db import updateArticleNER
from hactar.db import logEvent
from hactar.strings import cleanText
from stanfordcorenlp import StanfordCoreNLP
from fuzzywuzzy.process import dedupe as fuzzy_dedupe

#
# Functions
#

def doNER(nerserver,nerport,s):

    try:
        nlp = StanfordCoreNLP(nerserver,port=nerport,timeout=5,lang='en')
    except Exception as e:
        print("Stanford NER server expected at",nerserver,"port",nerport,e)
        print("     is not available, exception",e)
        print("     cannot run full NER, doing only REGEXP")
        return({})

    s=s.replace(u'~',u'\n',re.UNICODE| re.MULTILINE)
    s=s.replace(u'. ',u'\n',re.UNICODE| re.MULTILINE)
    # find named entities with stanford NER
    text=s.splitlines()
    sentence=""
    entities={}
    for sentence in text:
        if not sentence:
            continue
        # STANFORD
        # workaround for (emergent) NLP's unicode blindness 
#        b=bytes(sentence,'utf-8')
#        sentence=b.decode('ascii','ignore')
        # DO PART OF SPEECH
        #token= nlp.word_tokenize(sentence)
        #pos= nlp.pos_tag(sentence)
        try:
            ne=nlp.ner(sentence)
        except Exception as e:
            print("NER exception:",e)
            return()
        
        buf=''
        if not ne:
            nlp.close()
            return()
        
        # key and value ordering for output
        previous=ne[0][1] # first key
        for i in ne:
            if i[1] not in entities.keys(): #create new key
                entities[i[1]]=[]
            if i[1] == previous :
                buf=buf+' '+i[0]
            else: 
                entities[previous].append(buf)
                buf = i[0]
            previous=i[1]
    nlp.close()
    if 'O' in entities: del entities['O']
    return(entities)

# find named entity candidates with regular expression
def doNEC(s,title):
    from fuzzywuzzy.process import dedupe as fuzzy_dedupe
    
    s=s.replace(u'. ',u'~',re.UNICODE| re.MULTILINE)
    necRE='[^[~|«]([A-Z][\w-]{2,}(?:\s+[A-Z][\w-]+)*)'
    necREt='([A-Z][\w-]{2,}(?:\s+[A-Z][\w-]+)*)'
    if (title==True):
        nec=re.findall(necREt,s)
    else:
        nec=re.findall(necRE,s)
    return(list(fuzzy_dedupe(nec)))


# #
# MAIN
# #

socket.setdefaulttimeout(10)

parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server", help="mongodb server address:port")
parser.add_argument("-u", "--url", dest="url",help="load one specific url")
parser.add_argument("-v", "--verbose",action="store_true" , help="be chatty")
parser.add_argument("-d", "--debug",action="store_true" , help="debug extra messages and logging")
parser.add_argument("-S", "--nerserver", dest="nerserver", help="Stanford NER server in address:port form. Defaults to \"127.0.0.1:9000\"")



(args) = parser.parse_args()


# default server
nerserver='http://localhost'
nerport=9000

if args.nerserver:
    (s,p)=args.nerserver.split(":")
    nerserver="http://"+s
    nerport=int(p)
    if args.verbose: print ("connecting to Stanford NER at ",nerserver+":"+str(nerport))

if args.url:
    art=getArticle(args.url)
    text=cleanText(art.text)
    print("AUT:",art.authors)
    print("TTL:",art.title)
    print("DTE:",art.publish_date)
    print("URL:",art.url)
    print("TXT:",text)

    ner=doNER(text)
    for tag in ner.keys():
        if tag != 'O': print ("NER:",tag, ner[tag])

    exit()

if args.server:
    server=args.server
else:
    server="localhost:27017"

client = MongoClient(server)
db = client[args.dbname]
now = datetime.datetime.now()

articleDB = db["article"]
logDB=db['eventlog']

articleDB.create_index('ner-ts')
all = articleDB.find({'ner-ts':{'$exists': False}},no_cursor_timeout=False)

try:
    for item in all:
        nerb=doNER(nerserver,nerport,item['text'])
        nert=doNER(nerserver,nerport,item['title'])
        text=item['text'].replace(u'\.',u'~',re.UNICODE| re.MULTILINE)
        if 'O' in nerb: del nerb['O']
        if nerb or nert:
            updateArticleNER(articleDB,item['_id'],nerb,nert)
            logEvent(args.verbose,logDB,"ner","ok",item['source'],str(item['uuid']),item['URL'],"entities_found:"+str(len(nerb)))

except Exception as e:
    logEvent(args.verbose,logDB,"ner","warn",item['source'],str(item['uuid']),item['URL'],"NER_exception:"+str(e))

client.close()


