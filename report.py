#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import socket
import datetime
import time
import re
import operator
import hashlib
from prettytable import PrettyTable
from pymongo import MongoClient
from argparse import ArgumentParser


# send report
#
def sendmail(mailer,sender,recipients,subject,body):
    import smtplib

    msg=("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" % (sender, ", ".join(recipients),subject))
    msg += '\n'+body

    (mailserver,mailport)=mailer.split(":")
    s = smtplib.SMTP(mailserver,mailport)
    s.sendmail(sender, recipients, msg)
    s.quit()


### MAIN
#
parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  
	help="mongodb server address:port", default="127.0.0.1:27017")
parser.add_argument("-c", "--collection", dest="collection",  
	help="name of article collection", default="article")
parser.add_argument("-f", "--feedsource", dest="feedsource",  
	help="name of feedsource collection (feedsource)", default="feedsource")
parser.add_argument("-t", "--timestamp", dest="ts",  
	help="name of timestamp field", default="download-ts")
parser.add_argument("-m", "--mailer", dest="mailer",  
	help="mailer address and port (127.0.0.1:25)", default="127.0.0.1:25")
parser.add_argument("-n", "--newspaper", dest="src",  
	help="newspaper name source field (source)", default="source")
parser.add_argument("-R", "--recipient", dest="rcpt",  
	help="email address of the recipient", default="root@localhost")
parser.add_argument("-F", "--from", 	 dest="mfrom",  
	help="email address of the recipient", default="reports@localhost")
parser.add_argument("-S", "--subject",   dest="subject",  
	help="fixed part of the Subject",default="Report")

(args) = parser.parse_args()
server=args.server
src=args.src
ts=args.ts
client = MongoClient(server,serverSelectionTimeoutMS=1)
db = client[args.dbname]
articleDB = db[args.collection]
feedsourceDB = db[args.feedsource]

#logDB = db['eventlog']

now = datetime.datetime.now()

msg=""
msg += "\nTIPS report daily summary\n"
msg += "Date:\t\t"+now.strftime("%Y-%m-%d %H:%M")+"\n"
msg += "Database:\t*"+args.dbname+"* ("+server+")\n"
msg += "Collection:\t"+args.collection+"\n"



lastday = now - datetime.timedelta(hours=24)
last2day = now - datetime.timedelta(hours=48)
lasttend = now - datetime.timedelta(days=10)
lasthndred = now - datetime.timedelta(days=100)
lastthsnd = now - datetime.timedelta(days=1000)

sitenames = feedsourceDB.find().distinct('site')

tt=PrettyTable()

#tt.field_names = ['Newspaper','24h','48h','10d','100d','1000d','all']
tt.field_names = ['Newspaper','24h','48h','10d','100d','1000d']
#sum = [0,0,0,0,0,0]
sum = [0,0,0,0,0]

for s in sitenames:
        search=s
        #regx = re.compile(search, re.IGNORECASE)
        regx = re.compile(search)
        day   =articleDB.count_documents({'$and': [{src: {'$regex': search }},{ts:{"$gte": lastday  }}]})
        ytday =articleDB.count_documents({'$and': [{src: {'$regex': search }},{ts:{"$gte": last2day  }}]})
        tend  =articleDB.count_documents({'$and': [{src: {'$regex': search }},{ts:{"$gte": lasttend }}]})
        hndred=articleDB.count_documents({'$and': [{src: {'$regex': search }},{ts:{"$gte": lasthndred}}]})
        thsnd =articleDB.count_documents({'$and': [{src: {'$regex': search }},{ts:{"$gte": lastthsnd }}]})
        #all   =articleDB.count_documents({src: regx })

        b=sum
        #a=[day,ytday,tend,hndred,thsnd,all]
        a=[day,ytday,tend,hndred,thsnd]
        sum = [x+y for x,y in zip(a, b)]
        #tt.add_row([s,day,ytday,tend,hndred,thsnd,all])
        tt.add_row([s,day,ytday,tend,hndred,thsnd])

sum.insert(0,"__*SUM*__")

tt.sortby = "Newspaper"
tt.align["48h"] = "r"
tt.align["24h"] = "r"
tt.align["10d"] = "r"
tt.align["100d"] = "r"
tt.align["1000d"] = "r"
#tt.align["all"] = "r"
tt.align["Newspaper"] = "l"
tt.add_row(sum)
msg += tt.get_string()+"\n"
msg += "\n\nBest Regards\n"

title= args.subject+": "+args.dbname+" has "+str(sum[1])+" new items"
sendmail(args.mailer, args.mfrom, [args.rcpt], title, msg) 
client.close()
