#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018-2020 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import io
import sys
import socket
from datetime import date
from datetime import datetime
from datetime import timedelta
import time
import re
import operator
import hashlib
from prettytable import PrettyTable
import pprint
from pymongo import MongoClient
from argparse import ArgumentParser




def verbose(s):
    import sys
    if args.verbose: print(s, file=sys.stderr)

def debug(s):
    import sys
    import inspect
    if args.debug :
        frame=sys._getframe(1).f_code.co_name
        stack=inspect.stack()
        if "self" in stack[1][0].f_locals.keys():
            classname=stack[1][0].f_locals["self"].__class__.__name__
        else:
            classname='__main__'
        print(f"#{classname}.{frame}:\t{s}")



# send report
#
def sendmail(mailer,sender,recipients,subject,body):
    import smtplib
    from email.mime.text import MIMEText
    from email.header import Header
    from email.mime.multipart import MIMEMultipart


    msg = MIMEMultipart("alternative")
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = ", ".join(recipients)
    part1 = MIMEText(body, "plain", "utf-8")
    msg.attach(part1)

    (mailserver,mailport)=mailer.split(":")
    s = smtplib.SMTP(mailserver,mailport)
    s.sendmail(sender, recipients, msg.as_string())
    s.quit()


### MAIN
#
parser = ArgumentParser()
parser.add_argument("dbname", type=str,help='database name')
parser.add_argument("-s", "--server", dest="server",  help="mongodb server address:port", default="127.0.0.1:27017")
parser.add_argument("-c", "--collection", dest="collection",  help="name of article collection", default="article")
parser.add_argument("-f", "--feedsource", dest="feedsource",  help="name of feedsource collection (feedsource)", default="feedsource")
parser.add_argument("-t", "--timestamp", dest="ts",  help="name of timestamp field", default="download-ts")
parser.add_argument("-v", "--verbose", help="print report to stdout", action="store_true")
parser.add_argument("--debug", help="extra debugging output", action="store_true")
parser.add_argument("--nomail", help="stdout output, no mail", action="store_true")
parser.add_argument("-m", "--mailer", dest="mailer",  help="mailer address and port (127.0.0.1:25)", default="127.0.0.1:25")
parser.add_argument("-n", "--newspaper", dest="src",  help="newspaper name source field (source)", default="sources")
parser.add_argument("-d", "--datePattern", dest="datePattern",  help="pattern of days (eg. 1,30,365) to build table", default="1,2,10,100")
parser.add_argument("-R", "--recipient", dest="rcpt",  help="email address of the recipient", default="root@localhost")
parser.add_argument("-F", "--from", 	 dest="mfrom", 	help="email address of the recipient", default="reports@localhost")
parser.add_argument("-S", "--subject",   dest="subject", help="fixed part of the Subject",default="Report")
parser.add_argument("-D", "--force-date",   dest="forcedate", help="build report for a specific day [date format: YYYY-MM-DD] (default: yesterday)")

(args) = parser.parse_args()
server=args.server
src=args.src
ts=args.ts
client = MongoClient(server,serverSelectionTimeoutMS=1)
db = client[args.dbname]
articleDB = db[args.collection]
feedsourceDB = db[args.feedsource]
now = datetime.now()

if args.datePattern:
    days = [int(item) for item in args.datePattern.split(',')]
else:
    days=[1,2,10,30]    


if args.forcedate:
    fromtime = datetime.combine(datetime.strptime(args.forcedate, '%Y-%m-%d').date(), datetime.min.time())
    totime =  datetime.combine(datetime.strptime(args.forcedate, '%Y-%m-%d').date(), datetime.max.time())
    debug(f"forced time span of reference day: {fromtime} - {totime} = {fromtime-totime}")
else:
#    fromtime = datetime.combine(date.today() - timedelta(days=days[0]), datetime.min.time())
#    totime  = datetime.combine(date.today() - timedelta(days=days[0]), datetime.max.time())
    fromtime = datetime.combine(date.today()  , datetime.min.time())
    totime  = datetime.combine(date.today() , datetime.max.time())
    debug(f"default time span of reference day: {fromtime} - {totime} = {fromtime-totime}")


buf = io.StringIO()
tit = io.StringIO()


verbose(f"indexing {src}, {ts}")
articleDB.create_index(src)
articleDB.create_index(ts)


# init data structures
debug(f"finding distinct sitenames in {feedsourceDB}")
sitenames = feedsourceDB.find().distinct('site')
sitenames.sort()
debug(f"found {len(sitenames)}")
verbose(f"names: {sitenames}")
verbose(f"days: {days}")

dates={}
for d in days:
    dates.update({str(d): fromtime + timedelta(days=-d)})
    debug(f"reference day {d} {fromtime}+{d} {dates[str(d)]} = {dates[str(d)]-fromtime}")
debug(f"dates: {dates}")

lastdate=dates[str(days[-1])] # last item in dates array
firstdate=dates[str(days[0])] # first item in dates array (fromtime -1 day)
debug(f"Overall period dates span: [{firstdate} - {lastdate}] = {firstdate-lastdate}")

sums={}    # total items per period
for d in dates.keys():
    sums.update({d:0})
feeds={}   # feeds
counts={}  #items per feed per period
for s in sitenames:
    counts.update({s:{}})
    for l in dates.keys():
        counts[s].update({l:0})

# Init tables
tt=PrettyTable()
tp=PrettyTable()

msg="\n"
msg += "TIPS report\n"
msg += f"Reference day:\t{fromtime.date()}\n"
msg += "Report span:\t"+datetime.combine(firstdate  , datetime.min.time()).strftime("%Y-%m-%d %H:%M")+"\n"
msg += "\t\t"+datetime.combine(lastdate , datetime.max.time()).strftime("%Y-%m-%d %H:%M")+"\n"
msg += "Database:\t"+server+"/"+args.dbname+"\n"
msg += "Collection:\t"+args.collection+"\n"
msg += "Total items:\t"+str(articleDB.estimated_document_count())+"\n"
msg += "Date key used:\t"+args.ts+"\n"
 
print(msg,file=buf)

#
# find items, iterate over dates,
search={ts:{"$gte": lastdate}}
verbose(f"searching {search}")

all = articleDB.find(search,no_cursor_timeout=True).batch_size(1000)
for item in all:
    (proto,site,feed)=item[src][0].split("|")
    if (item[ts] <= fromtime):
        for d in dates.keys():
            if (item[ts] > dates[d]):
                debug(f"{fromtime} <=\t item {item[ts]} \t> {dates[d]}")

                counts[site][d]+=1
                sums[d]+=1
                # statistics for the day
                if d==str(days[0]):
                    for f in item[src]:
                        if f not in feeds.keys():
                            feeds.update({f:1})
                        else:
                            feeds[f]+=1
                            

# build table and check for empty newspapers
alert=[]
labels=[]
daysLabel=" days"
for l in dates.keys():
    labels.append(l+daysLabel)
tt.field_names=['Newspaper']+labels
tt.align['Newspaper']="l"
for s in sitenames:
    data=[s]
    for d in dates.keys():
        tt.align[str(d)+daysLabel]="r"
        data.append(counts[s][d])
    tt.add_row(data)
    if counts[s][str(days[0])] == 0:
        alert.append(s+" has no items")

data=["__SUM__"]
for d in dates.keys():
    data.append(sums[d])
tt.add_row(data)

buf.write(tt.get_string()+"\n")
if alert:
    for a in alert:
        buf.write("WARNING: "+str(a)+"\n")
buf.write("\n\n")


## Detail by feedsource
buf.write("\n Detail by feed\n")
labels=["Feed ("+str(days[0])+"d)","items"]
tp.field_names = labels
for f in feeds.keys():
    tp.add_row([f,feeds[f]])
tp.sortby = "items"
tp.reversesort = True
tp.align[labels[0]] = "l"
tp.align[labels[1]] = "r"
table = tp.get_string()+"\n"
buf.write(table)

if not args.forcedate:
    buf.write("\n\nFull graph available at:\n")
    buf.write("http://tipsproject.eu/a5d3505baa61ae6e67128126bbc6870b/quality/out/"+args.dbname)
    
buf.write("\n\nBest Regards\n")

verbose(buf.getvalue())

tit.write(args.subject)

if alert: 
    tit.write(" !"+str(len(alert)))
tit.write(" "+args.dbname)
if args.forcedate:
    tit.write(" "+args.forcedate)
tit.write(" +"+str(sums[str(days[0])])+" items")

if args.nomail:
    print(f"{tit.getvalue()}\n\n{buf.getvalue()}")
else:
    print("sending mail")
    sendmail(args.mailer, args.mfrom, [args.rcpt], tit.getvalue(), buf.getvalue()) 
    client.close()
