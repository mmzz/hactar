#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo,
#                  2020 Alberto Zanatta
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# scraping functions

import warnings
warnings.filterwarnings("ignore")

def isBannedURL(newspaper,dontIndex,url):
    import re
    if newspaper in dontIndex.keys():
        banurls = dontIndex[newspaper]
        for banurl in banurls:
           if re.search(banurl, url, re.IGNORECASE):
               return(True)
    return(False)

def html2unicode(html):
    from bs4 import BeautifulSoup
    import warnings
    warnings.filterwarnings("ignore", category=UserWarning, module='bs4')
    html8 = BeautifulSoup(html,"lxml")
    return(html8.text)


def mostRecent(datesList):
    import datetime
    ''' returns most recent date in the list, or None if no date in list is in date format or empty list
    '''
    # added apr 2024
    if len(datesList) == 0:
        return(None)
    result=datesList[0]
    
    for date in datesList:
        if not isinstance(date, datetime.date):
            continue
        else:
            if date.timestamp() > result.timestamp():
                result=date
    if not isinstance(result, datetime.date):
        result=None
    return(result)            

            
def convertDate(date):
    import datetime
    ''' converts a string representing a date into datetime object or None
    '''
    # added apr 2024
    from dateutil.parser import parse
    from dateutil.tz import gettz
    import dateparser 
    # supplementary timezone formats for date detection
    tzinfos = {
        "CEST":gettz("Europe/Paris"), 
        "BRST": -7200, 
        "CST": gettz("America/Chicago")
    }
    try:
        published=parse(date, tzinfos=tzinfos, fuzzy = True)
    except Exception as e:
        logEvent(verbose,logDB,"process","err","-","-","date_parse_exception:["+str(e)+"] in ["+str(p)+"]")
        try:
            published=dateparser.parse(p)
        except Exception as e:
            logEvent(verbose,logDB,"process","err","-","-","date_dateparser_exception:["+str(e)+"] in ["+str(p)+"]")
    if isinstance(published,datetime.date):
        return(published.replace(tzinfo=None))
    else:
        return(None)


def getDatesFromHtml(html):
    import re
    import time
    import datetime

    # added apr 2024
    # 
    # list of regexp catching publishing dates
    dates=[]
    expressions=[ "<meta property=\"article:published_time\".*?content=\"([^\"]*)\"",
                  "datePublished\":.?\"(.*?)\"",
                  "<meta name=\"last-modified\" content=\"([^\"]*)\"",
                  "id=\"datum1\" value=\"([^\"]*)\">",
                  #"<meta name=\"last-modified\" content=\"[\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z]\">",
                  "<meta name=\"date\" content=\"([^\"]*)\""]
    p=published=None
    for ex in expressions:
        r = re.search(ex, html)
        if r:
            p = r.group( 1 )
        if p != None:
            published=convertDate(p)
            if published != None:
                dates.append(published.replace(tzinfo=None))
                
                success=True

    if dates != []:
        result=mostRecent(dates)
    else:
        result=None
    return(result)


def getMeta(html):
    # extract some information from <meta> HTML tags
    import re
    import time
    from dateutil.parser import parse
    from dateutil.tz import gettz
    import dateparser 
    
    # supplementary timezone formats for date detection
    tzinfos = {
        "CEST":gettz("Europe/Paris"), 
        "BRST": -7200, 
        "CST": gettz("America/Chicago")
    }

    # title
    p = p1 = p2 = title = None
    r = re.search( "<meta property=\"og:title\".*?content=\"([^\"]*)\"", html )
    if r: p1 = r.group( 1 )
    r = re.search( "<title>(.*)<\title>", html )
    if r: p2 = r.group( 1 )

    if (p1 and p2):  p = p1
    elif (p2):       p = p2
    elif (p1):       p = p1

    if p:
        title=html2unicode(p)
    else:
        title=None

    # removed apr 2024 - included and expanded in function getDatesFromHtml()
    # date: multiple libraries
    # p = p1 = p2 = published = None
    # r = re.search( "<meta property=\"article:published_time\".*?content=\"([^\"]*)\"", html )
    # if r: p1=r.group( 1 )
    # r = re.search( "datePublished\":.?\"(.*?)\"", html )
    # if r: p2=r.group( 1 )
            
    # if (p1 and p2):  p = p1
    # elif (p2):       p = p2
    # elif (p1):       p = p1

    # if p:
    #     try:
    #         published=parse(p, tzinfos=tzinfos, fuzzy = True)
    #     except Exception as e:
    #         print("process","err","-","-","date_parse_exception:["+str(e)+"] in ["+str(p)+"]")
    #         try:
    #             published=dateparser.parse(p)
    #         except Exception as e:
    #             print("process","err","-","-","date_dateparser_exception:["+str(e)+"] in ["+str(p)+"]")

    # added apr2024
    published=getDatesFromHtml(html)
    
    # author
    r=re.search( "<meta property=\"article:author\".*?content=\"([^\"]*)\"", html )
    if r is None:
        author=None
    else:
        a=r.group( 1 )
        author=html2unicode(a)

    # summary
    summary=None
    r=re.search( "<meta property=\"og:description\".*?content=\"([^\"]*)\"", html )
    if r is not None:
        s=r.group( 1 )
        summary=html2unicode(s)
                    
    return(title,summary,author,published)


                
def scrape_newspaper3k(htmlSource,scraped):
# scrape using newspaper3k library
    import newspaper
    from hactar.strings import cleanText

    try:
        article = newspaper.Article('')
        article.set_html(htmlSource)
        article.parse()
    except Exception as e:
        logEvent(verbose,logDB,"newspaper3k","warn","","","","scraping_exception:["+e+"]")
    scraped['text']= cleanText(str(article.text.strip())) 
    scraped['title']= cleanText(str(article.title.strip()))
    scraped['authors']=[]
    scraped['top_image']=""
    scraped['scraper']="newspaper3k"

def scrape_html2text(htmlSource,scraped):
    # scrape using html2text library
    from readability import Document  # readability-lxml
    import html2text
    from lxml import html        # call Html2text scraper
    from hactar.strings import cleanText

# module 're' has no attribute '_pattern_type' issue
# 
#    doc = Document(input=htmlSource,
#                   positive_keywords=["articleColumn", "article", "content"],
#                   negative_keywords=["commentColumn", "comment", "comments"])

    h = html2text.HTML2Text()
    #        h.inline_links = False # reference style links
    h.ignore_links = True
    h.wrap_links = False
    h.ignore_anchors = True
    h.ignore_images=True
    h.ignore_emphasis=True
    h.ignore_tables=True
    h.unicode_snob = True  # Prevents accents removing
    h.skip_internal_links=True
    h.body_width = 0
    
    doc = Document(input=htmlSource)
    try:
        text = cleanText(h.handle(doc.summary()).strip())
    except Exception as e:
        text = ""
    try:        
        title = cleanText(str(doc.short_title().encode('utf-8').strip()))
        title = cleanText(doc.short_title().strip())
    except Exception as e:
        title = ""
    scraped['text']= text
    scraped['title']= title
    scraped['scraper']="html2text"

def processArticle(meta, art, articleDB, archiveDB, logDB, db, verbose):
    import sys
    import codecs
    import bz2
    import re
    import hashlib
    import unicodedata
    import datetime
    import chardet
    import uuid
    
    from hactar.db import storeArticle
    from hactar.db import updateArticle
    from hactar.db import deIndexArticle
    from hactar.db import reIndexArticle
    from hactar.db import logEvent
    from hactar.strings import cleanText
    from hactar.strings import doNEC
    from hactar.db import loadRules
    import constant as k

    import importlib

    # INIT
    #
    #

    # load dictionnary of URLs banned from indexing
    dontIndex=loadRules(db[k.RULES],"dontIndexURL")

    # generate unique uuid &timestamp for document and html archive
    artuuid=str(uuid.uuid4()) 
    ts=datetime.datetime.utcnow() 

    # init article structure
    article={
        k.UUID: artuuid,
        k.DOWNLOADTS: ts,
        k.UPDATEDTS: ts,
        k.SOURCES:[meta['source']],
        k.DATESPUBLISHED:[],
        k.YEAR:"",
        k.AUTHORS:[],
        k.URLS:[art.url],
        k.SUMMARY: meta['summary'],
        k.IMAGEURL: "",
        k.TEXT:"",
        k.TITLE: "",
        k.METAHASH:[],
        k.CONTENTHASH:"",
        k.NECB:"",
        k.NECT:"",
        k.SCRAPER:"",
	k.INHOMEPAGE: meta['inHomePage'],
        }

    # SCRAPING
    #
    #
    
    # if scraped text from newspaper3k is short try html2text
    # skip article with empty scraped text
    scraped={
        'text': cleanText(str(art.text.strip())),
        'title': cleanText(str(art.title.strip())),
        'authors': art.authors,
        'top_image': art.top_image,
        'scraper':"newspaper3k",
    }
    if len(art.text) < 500:
        scrape_html2text(art.html,scraped)

    if len(scraped['text']) < 500:
        logEvent(verbose,logDB,"process","warn",meta['source'],"",art.url,"empty/short text")
        return(False)

    article[k.TEXT] = scraped['text']
    article[k.IMAGEURL] = scraped['top_image']
    article[k.SCRAPER] = scraped['scraper'] 

    # MERGING
    # merge redundant metadata: title, authors, publishing date
    # title

    (html_title,html_summary,html_author,html_published)=getMeta(art.html)
    
    if meta['title'] == None:
        article[k.TITLE] = scraped['title']
    else:
        article[k.TITLE] = meta['title']

    # authors
    if meta['author'] == []:
        article[k.AUTHORS] = scraped['authors']
    else:
        article[k.AUTHORS] = meta['author']
        
    # date published
    # feed date first, then scraper metadata, then article HTML header metadata, eventually: download timestamp
    published = meta['published']
    if published == None:  published = art.publish_date
    if published == None:  published = html_published
    if published == None:
#        published = "NULL" # type mismatch messes indexes
        published= ts 
        logEvent(verbose,logDB,"process","warn",meta['source'],"",art.url,"no_publishing_date")
    else:
        article[k.DATESPUBLISHED] = [published]

    # year first published
    try:
        article[k.YEAR] = article[k.DATESPUBLISHED][0].year
    except Exception as e:
        article[k.YEAR] =  ts.year

    # NEC: RE Named Entity candidates detection
    #
    #
    article[k.NECB]=doNEC(article[k.TEXT],False)
    if article['title']:
        article[k.NECT]=doNEC(article[k.TITLE],True)
    else:
        article[k.NECT]=None

    if articleDB == None:
        return(article)

    # DUPLICATE DETECTION
    # 
    #
    source=meta['source']
    newspaper = source.split('|')[1]
    text=article[k.TEXT]
    if type(published) == datetime.datetime:
        published.replace(tzinfo=None) # 20210921 - mmzz MongoDB strips TZ info. Hash values mor metadata should be calculated without TZ
    metahash=hashlib.md5((source+art.url+str(published)).encode('utf-8')).hexdigest()
    contenthash=hashlib.md5(str(text).encode('utf-8')).hexdigest()

    # Financial Times landing page for paywall.
    if contenthash=='abfc28577ac0f311bead542522498af4' :
        return(False)
    
    ## Search for duplicates
    # A - same content, same metadata --> skip
    dup=articleDB.find_one(
	{ '$and': [ 
                { 'contentHash': contenthash}   , # select articles with same content
                { 'metaHash':    metahash}        # AND  with same metadata
	] }, no_cursor_timeout=False)
    if dup:
        logEvent(verbose,logDB,"process","ok",source,str(dup['uuid']),art.url,"skip_on_duplicate")
        return(False)

    # B - same content, different metadata --> update
    dup=articleDB.find_one(
        { '$and': [
            { 'contentHash': contenthash }   , # select articles with same content
            { 'metaHash':   {'$ne': metahash}}       # AND different metadata
        ] } , no_cursor_timeout=False)
    if dup:
        # if not in homepage, keep previous homepage status 
        inHomePage = dup['inHomePage']
        if (article[k.INHOMEPAGE]): inHomePage = True
        logEvent(verbose,logDB,"process","ok",source,str(dup["uuid"]),art.url,"update_metadata")
        updateArticle(articleDB,dup['_id'],source,published,art.url,inHomePage,metahash)

        # check for banned URLs
        if  ('dontIndex' in dup) and (dup['dontIndex'] == 'articleIsLocal') and not (isBannedURL(newspaper,dontIndex,art.url)):
            reIndexArticle(articleDB, dup['uuid'])
            logEvent(verbose,logDB,"process","ok",source,str(article[k.UUID]),art.url,"reIndex")
            print(source+art.url+str(published),metahash)
        return(True)

    # C - different content and metadata 
    #   --> add new article
    #   --> add compressed html with some metadata in yearly archive
    # NOTE: 
    # authoritative metadata about duplicate articles is kept only in 
    # the "articles" collection to avoid the burden of redundant HTML data.
    # Please note that metatdata in the "archive" collection is kept for convenience
    # and should not be considered exhaustive about all duplicates metadata for a given text

    article[k.CONTENTHASH]=contenthash
    article[k.METAHASH]=[metahash]
    
    # Store result
    if archiveDB != None:
        html=str.encode(art.html)
        bzhtml=bz2.compress(html)
        archive={
            'artuuid':article[k.UUID],
            'year': article[k.YEAR],
            'source':meta['source'],
            'bzhtml':bzhtml,
            'download-ts': ts,
	    'scraper': article[k.SCRAPER],
            'URL':art.url
        }            
    else:
        archive=None

    logEvent(verbose,logDB,"process","ok",source,str(article[k.UUID]),art.url,"new_article")
    storeArticle(articleDB,archiveDB,article,archive)
    print(source+art.url+str(published),metahash)
    
    if isBannedURL(newspaper,dontIndex,art.url):
            deIndexArticle(articleDB, article['uuid'],"articleIsLocal")
            logEvent(verbose,logDB,"process","ok",source,str(article[k.UUID]),art.url,"deIndex")

    
    return(True)
