


#  Load rules sets
# db.rules.insertOne({"ruleName" : "language", ruleDict: {"morphoLang": "ita"}})
def getRules(rulesDB,ruleName,mandatory):
   rules=loadRules(rulesDB, ruleName)
   if rules == {}:
       if args.dryrun and args.verbose:
           print(f"warn: no {ruleName} rule in {k.RULES} DB")
       else:
           logEvent(args.verbose,logDB,THISMODULE,"warn","","","",f"skip: no {ruleName} rule in {k.RULES} DB")
       if mandatory == True:
           exit()
   return(rules)


