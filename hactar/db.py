#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import codecs
import unicodedata
import http.client
import chardet
import socket
import datetime
import time
#import urltools
import re
import hashlib
import urllib.parse
from time import mktime
#from lxml.html.clean import Cleaner
from optparse import OptionParser


import constant as k


# open database connection
def openDB(server,dbname):
    from pymongo import MongoClient
    dbname=dbname
    if server is None:
        server="localhost:27017"
    client = MongoClient(server)
    db = client[dbname]
    return(db)


        
# log feed statistics and errors
def logEvent(verbose,logDB,prog,level,source,uuid,url,message):
        import json
        from pymongo import MongoClient
        ts = datetime.datetime.utcnow()
        sts = str(ts)
        if verbose:
            print(f"{sts}\t{prog}\t{level}\t{message}\t{uuid}\t{source}\t{url}")
        try:
            logDB.insert_one(
                {
                        'timestamp':ts,
                        'module':prog,
                        'level':level,
                        'source':source,
                        '':uuid,
                        'URL':url,
                        'message':message,
                }
            )
        except pymongo.errors.OperationFailure as e:
            print (e.code)
            print (e.details)

# store feed items in mongodb collection
def storeFeedItem(feeditemDB,oid,itemhash,url,site,name,title,link,published,author,summary,inHomePage):
        import json
        from pymongo import MongoClient
        feeditemDB.insert_one(
                        {
                                'itemTitle': title,
                                'feedSite': site,
                                'feedName': name,
                                'feedURL': url,
                                'itemLink': link,
                                'itemPublished': published,
                                'itemAuthor': author,
                                'itemSummary': summary,
                                'itemMD5hash': itemhash,
                                'collected': False,
                                'channel': "rss",
                                'inHomePage': inHomePage
                        }
        )
        return(1)



# log 
def logArticle(articlelogDB, oid, published, source, link, http, realurl, textsize,  headers):
    import json
    from pymongo import MongoClient
    articlelogDB.insert_one(
       {
        'feedItemOid': oid,
        'published': published,
        'source': source,
        'link': link,
        'httpCode': http,
        'URLs': realurl,
        'textsize': textsize,
        'headers': headers,
       }
      )


    
# update Article 
def updateArticle(articleDB,oid,sources,published,url,inHomePage,metahash):
    articleDB.update_one( { '_id': oid},
        { '$push': {
            'sources': sources,
            'datesPublished': published,
            'URLs': url,
            'metaHash': metahash,
        }})
                          
    articleDB.update_one( { '_id': oid},
        { '$set': {
              'inHomePage': inHomePage,
              k.UPDATEDTS:datetime.datetime.utcnow()}
        })


def updateArchive(archiveDB,uuid,values):
    articleDB.update_one( { k.ARTUUID: uuid},
                          { '$set': values})
    

def deIndexArticle(articleDB,uuid,reason):
    articleDB.update_one( { 'uuid': uuid},
        { '$set': {
            'dontIndex' : reason,
            k.UPDATEDTS :datetime.datetime.utcnow()
        }}, upsert=True)
    


    
def reIndexArticle(articleDB,uuid):
    articleDB.update_one( { 'uuid': uuid},
        { '$unset': {
            'dontIndex': 1
        }})
    articleDB.update_one( { 'uuid': uuid},
        { '$set': {
            k.UPDATEDTS :datetime.datetime.utcnow()
        }})
    
def storeNamedEntity(namesDB,E):
    from pymongo import MongoClient
    
    namesDB.insert_one(E)    


def updateNamedEntity(namesDB,name,E):
    namesDB.update_one( { 'name': name},
        {
            '$set': E,
        }, upsert=True)

    

# store successufully retrieved article, content gzipped
def storeArticle(articleDB,archiveDB,A,B):
    from pymongo import MongoClient
    from bson import Binary
    
    articleDB.insert_one(A)
    if archiveDB != None:
        archiveDB.insert_one(B)
    
    
# store NER result
def updateArticleNER(articleDB,oid,ner,version):
    import datetime
    from pymongo import MongoClient

    ts = datetime.datetime.utcnow()
    articleDB.update_one({'_id':oid},
                     {
                         '$set': {
                             k.NER: ner,
                             k.NERVERSION: version,
                             k.UPDATEDTS: datetime.datetime.utcnow(),
                         },
                     })
    
# Store a hash of each string in page, 
def storeLineHash(lineHashDB,page):
    import datetime

    for line in page.splitlines():
        md5hash=hashlib.md5(line.encode('utf-8')).hexdigest()
        ts = datetime.datetime.utcnow()
        dups= lineHashDB.count_documents({'hash':md5hash}) 
        if dups >0:
            lineHashDB.update_one({'hash':md5hash},
                    {
                        '$inc': {'count':1},
                        '$set': {'last':ts}
                    },
                    upsert=True)
                    #multi=True)
        else:
            lineHashDB.insert_one(
                {
                    'hash': md5hash,
                    'first': ts,
                    'last': ts,
                    'line': line,
                    'count': 1
                }
            )



# mark feed sources if they provided articles or fail to do so
def markFeedActivity(feedsourceDB,channel,feedSite,feedName,hasItems):
    import datetime
    from pymongo import MongoClient

    now = datetime.datetime.now()
    if (hasItems):
        feedsourceDB.update_one({
            '$and': [
                {'protocol': channel},
                {'site': feedSite},
                {'name': feedName}
            ]
        },{'$set':{'lastPositive': now}})
    else:
        feedsourceDB.update_one({
            '$and': [
                {'protocol': channel},
                {'site': feedSite},
                {'name': feedName}
            ]
        },{'$set':{'lastNegative': now}})


# load set of rules in dictionnary
def loadRules(rulesDB,ruleName):
        rules = rulesDB.find_one({"ruleName": ruleName})
        if rules:
                return(rules["ruleDict"])
        else:
                return({})
        
