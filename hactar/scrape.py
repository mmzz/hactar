#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


#
# scrape
# 
def scrape(lineHashDB,html,encoding):
        # from lxml.html.clean import Cleaner 
        from lxml_html_clean import Cleaner 
        from scrapy.selector import Selector
        import re
        import hashlib

        # cleaner setup
        cleaner = Cleaner(allow_tags=['div','p'], remove_unknown_tags=False)
        cleaner.javascript = True # activate the javascript filter
        cleaner.style = True      #  activate the styles & stylesheet filter
        cleaner.comments = True
        cleaner.annoying_tags = True
        cleaner.inline_style = True
        cleaner.page_structure = False
        cleaner.remove_tags = ['b','a','h']
        cleaner.kill_tags = ['script']
        
        #invoke cleaner
        try:
            page=cleaner.clean_html(html)
        except:
            #error: ValueError: Unicode strings with encoding declaration are not supported. Please use bytes input or XML fr 
            content = ""
            return content

        page8=page
        page8 = re.sub('\n',' ',page8) # remove NL 
#       page8 = re.sub(u'\s','',page8,re.UNICODE) # blanks -> space
        page8 = re.sub('&#13;',' ',page8) # remove CR
        page8 = re.sub('<!--.*?-->',' ',page8) # remove comments
        page8 = re.sub(' class=".*?"',' ',page8) # remove attributes
        page8 = re.sub(' id=".*?"',' ',page8)
        page8 = re.sub(' rel=".*?"',' ',page8)
        page8 = re.sub('\[an error occurred while processing this directive\]',' ',page8)
        page8 = re.sub('>\s*?<','><',page8) # remove blanks between tags 

        # cycle to remove spurious divs
        for count in range (1,20):  
                page8 = re.sub('>.{0,10}<','><',page8) # remove words under 10 chars between tags
                page8 = re.sub('<div></div>',' ',page8)
                page8 = re.sub('<p></p>',' ',page8)
                page8 = re.sub('<span></span>',' ',page8)

        page8 = re.sub('\s+',' ',page8)  # remove repeated blanks

        #XPATHs
        xpath='//*[((p) or (a) or (b) or (div) or (span)) ]/node()[(string-length() > 300)]/text()'
        xpath='//*[((p) or (div))]/node()[(string-length() > 100)]/text()'

        sel=Selector(text=page8, type="html")
        text= sel.xpath(xpath).extract()
        content = ""
        if text:
            for s in text:
                # squash duplicate whitespaces
                ' '.join(s.split())
                # remove short lines
                # on empirical analysis, no unfrequent sentence under 40 chars is a relevant part of the article text, excluding repetition of title, authors, dates, etc. 
                if len(s) < 40:         
                        next
                # remove leading whitespace 
                #if s.endswith(" "): s = s[:-1]
                if s.startswith(" "): s = s[1:]
                content += s
                content += "\n"
        return content

def cleanupText(lineHashDB,page,lineThreshold):
        import hashlib
        from pymongo import MongoClient

        content= ""
        for line in page.splitlines():
                md5hash=hashlib.md5(line).hexdigest()
                hashline=lineHashDB.find_one({'hash':md5hash})
                if hashline:
                    dups=hashline['count']
                    if dups < lineThreshold:
                        content += "~"
                        content += line
                        content += " "
                else:
                        content += "~"
                        content += line
                        content += " "
        return(content)

