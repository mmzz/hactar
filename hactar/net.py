#!/usr/local/bin/python3.7
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import getopt
import http.client
import chardet
import socket
import datetime
import json
import time
#import urltools
import re
import hashlib
import urllib.parse
from time import mktime
#from lxml.html.clean import Cleaner
from optparse import OptionParser


#DEBUG=False

# collect & scrape article with "newspaper" lib
def getNewspaperArticle(url):
    import newspaper
    from newspaper import Article
    art=Article(link)
    art.download()
    art.parse()
    #return(art.text,art.authors.art.date,art.url,art.html,art.lang)
    return(art.text)



# collect entries in feeds or exceptions
def getFeed(url):
        import feedparser
        try:
            f=feedparser.parse(url)
            if f.bozo == 1 :
                if f["entries"]:
                        return (f.bozo,f["entries"],f.bozo_exception)
                else:
                        return (f.bozo,[],f.bozo_exception)
            else:
                return (f.bozo,f["entries"],None)
        except Exception as e:
            return (None,None,e)

# retrieve URL
def geturl(url):
        import codecs
        import unicodedata
        import requests
        from bs4 import UnicodeDammit,BeautifulSoup
        DEBUG=False

        #https://github.com/requests/requests/issues/1604
        ##import requests_patch


        url.strip()
        err=code=http=""

        #get URL
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException as err:
            if DEBUG: print(err,url)
            return("","","",url,"",err)

        #http status code
        http = str(r.status_code)

        #empty page                
        if (len(r.text) == 0):
            if DEBUG: print("EMPTY [%s][%s][%s][%s]" % (http,r.encoding,code,err))
            return("",http,"",r.url,r.headers,"emptyPage")

        #detect encoding
        if (('content-type' not in r.headers) or ("text" not in r.headers['content-type'].lower())):
            encoding="NONE"
            err="noText"
            r.text=""
        else:
            detected = chardet.detect(r.content)['encoding']
            if detected : 
                encoding= detected.upper()
            if r.encoding :
                encoding = r.encoding.upper()
            if r.apparent_encoding :
                encoding = r.apparent_encoding.upper()
        if DEBUG:
            print("ENCODING: declared/apparent/detected:",r.encoding,r.apparent_encoding,detected,"URL: ",url)
        if encoding != 'ISO-8859-1' or encoding != 'UTF8' or encoding != 'UTF-82':
            encoding='UTF-8'

        return(r.text,http,code,r.url,r.headers,err)

        # other encoding strategies (failed)

        # strategy A - 
        #detect content type
        if (('content-type' not in r.headers) or ("text" not in r.headers['content-type'].lower())):
            if DEBUG: print("[%s][%s][%s][%s]" % (http,r.encoding,code,str(r.headers)))
            return("",http,"",r.url,r.headers,"noText")
        #detect encoding
        if r.encoding is None or r.encoding.upper() == 'ISO-8859-1':
            r.encoding = r.apparent_encoding.upper()
        return(r.text,http,r.encoding,r.url,r.headers,err)



        # strategy C - detect content type (Unicode, Dammit)
        #dammit = UnicodeDammit(r.text, [r.encoding, r.apparent_encoding, "utf-8", "iso-8859-1"], is_html=True,)
        soup= BeautifulSoup(r.text,"lxml")
        encoding=soup.original_encoding
        if encoding is None:
            encoding=r.encoding
        return(r.text,http,encoding,r.url,r.headers,err)


        # strategy B - detect content type (heuristic, apparent encoding)
        if (('content-type' not in r.headers) or ("text" not in r.headers['content-type'].lower())):
            if DEBUG: print("[%s][%s][%s][%s]" % (http,r.encoding,code,str(r.headers)))
            return("",http,"",r.url,r.headers,"noText")
            
        if r.encoding is None or r.encoding.upper() == 'ISO-8859-1' or r.encoding.upper() == 'ASCII':
            r.encoding = r.apparent_encoding
        return(r.text,http,r.encoding,r.url,r.headers,err)


# get article with newspaper lib
# does excellent scraping, too
# art.title,art.publish_date,art.url,art.text,art.html
def getArticle(url):
    from newspaper import Article
    art=Article(url,keep_article_html=True,request_timeout=20)
    try:
        art.download()
    except Exception as e:
        print(e)
    art.parse()
    return(art)


# retrieve URL
def geturl(url):
        import requests
        DEBUG=False
        
        #https://github.com/requests/requests/issues/1604
        ##import requests_patch


        url.strip()
        http=""

        #get URL
        try:
            r = requests.get(url)
        except requests.exceptions.RequestException as err:
            if DEBUG: print(err,url)
            return("","","",url,"",err)
        http = str(r.status_code)
        return(r.text,http,r.headers,"")
