#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#
#    Copyright (C) 2018,2019 Alberto Cammozzo
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.



# removes regular expressions from text
def removeRE(text,string):
    import re
    from re import finditer
    import sys

    if not (text and string):
            return(text)    
    p=0
    cleanText=u""
    for match in finditer(string, text) :  # https://docs.python.org/3/reference/lexical_analysis.html # inner group has not to be matched
            offset, end = match.span()
            cleanText+=text[p:offset]
            p=end
#            sys.stderr.write(f"I: {text[offset-10:offset]}[{match.group()}]{text[end:end+10]}"+"\n")
    cleanText+=text[p:]

    return(cleanText)

# replaces regular expressions in text
def replaceRE(text,string,replacement):
    import re
    from re import finditer

    if not (text and string):
            return(text)    
    p=0
    cleanText=u""
    for match in finditer(string, text) :  # https://docs.python.org/3/reference/lexical_analysis.html # inner group has not to be matched
            offset, end = match.span()
            cleanText+=text[p:offset]+replacement
            p=end
#            sys.stderr.write(f"I: {text[offset-10:offset]}[{match.group()}]{text[end:end+10]}"+"\n")
    cleanText+=text[p:]

    return(cleanText)

# removes fixed strings and characters from text
def removeStrings(text):
    import re
    from re import finditer    

    # various char replacements
    text = re.sub(r'\. \.', '.', text) # . . -> .
    text = re.sub(r'\\', '', text)     # lone \ 
    text = re.sub(r'\'\'', '"', text)  # '' -> "

    text = re.sub(r'\r', ' ', text)    # ^M
    text = re.sub(r'\n', ' ', text)    # nl
    text = re.sub(r'\t', ' ', text)    # tab
    text = re.sub(r'\*', ' ', text)    #  *
    text = re.sub(r'\#', ' ', text)    #  *


    # case by case (ITALIAN SPECIFIC)
    # 
    text = re.sub(r'Ã¹','ú', text)    # 
    text = re.sub(r'Ã¨','è', text)
    text = re.sub(r'Ã¬','í', text)
    text = re.sub(r'Ã²','ò', text)
    text = re.sub(r'Ã ','à', text)
    text = re.sub(r'Â',' ', text)    # 
    #    â should be \'  , but "debâcle" should be preserved
    text = re.sub(r'Lâ','l\'', text)
    text = re.sub(r'sullâ','sull\'', text)
    text = re.sub(r'dallâ','dall\'', text)
    text = re.sub(r'dellâ','dell\'', text)
    text = re.sub(r'nellâ','nell\'', text)
    text = re.sub(r'allâ','all\'', text)
    text = re.sub(r'quellâ','quell\'', text)
    text = re.sub(r'questâ','quest\'', text)
    text = re.sub(r'unâaltr','un\'altr', text)
    text = re.sub(r' lâ',' l\'', text)
    text = re.sub(r' câ', ' c\'', text)
    text = re.sub(r' Câ', ' C\'', text)
    text = re.sub(r' â','\'', text)
    text = re.sub(r'â ','\' ', text)
    text = re.sub(r' dâ',' d\'', text)
    text = re.sub(r' sâ',' s\'', text)
    text = re.sub(r'âanni','\'anni', text)
    text = re.sub(r' poâ',' po\'', text)
    text = re.sub(r' santâ',' sant\'', text)
    text = re.sub(r' devâ',' dev\'', text)
    text = re.sub(r'comâè','com\'è', text)
    text = re.sub(r' unâ',' un\'', text)
    text = re.sub(r'tuttâaltro',' tutt\'altro', text)


    text = re.sub(r'ï¿½','"',text)
    text = re.sub(r'&#8217;','\'',text)
    text = re.sub(r'&#8213;','-',text)
    
    text = re.sub(r'©',' ', text)    #
    text = re.sub(r'\u008D', 'ì', text)
    text = re.sub(r'\u0080', '', text) 
    text = re.sub(r'\u0085', ' ', text)  # 
    text = re.sub(r'\u00AD', '', text)   # soft-hyphen
    text = re.sub(r'\u0092', '\'', text)
    text = re.sub(r'\u0096', '-', text)  # 
    text = re.sub(r'\u009C', '', text) 
    text = re.sub(r'\u0093', '"', text)   
    text = re.sub(r'\u0094', '"', text)  
    text = re.sub(r'\u200E', '"', text)  
    text = re.sub(r'\u200B', '"', text)
    text = re.sub(r'\uFEFF', '', text)
    text = re.sub(r'<U+0090>', '', text) 

    text = re.sub(r'che\'','ché',text)
    text = re.sub(r'\se\'',' è',text)
    text = re.sub(r'piu\'','più',text)

    # strings to be removed
    strings=[ r'RIPRODUZIONE\sRISERVATA'
              ,r'Riproduzione\sriservata'
              ,r'ABBONATI\sA\sREPUBBLICA'
              ,r'Noi\snon\ssiamo\sun\spartito,\snon\scerchiamo\sconsenso,\snon\sriceviamo\sfinanziamenti\spubblici,\sma\sstiamo\sin\spiedi\sgrazie\sai\slettori\sche\sogni\smattina\sci\scomprano\sin\sedicola,\sguardano\sil\snostro\ssito\so\ssi\sabbonano\sa\sRep:'
              ,r'Se\svi\sinteressa\scontinuare\sad\sascoltare\sun\'altra\scampana,\smagari\simperfetta\se\scerti\sgiorni\sirritante,\scontinuate\sa\sfarlo\scon\sconvinzione'
              ,r'Mario\sCalabresi\sSostieni\sil\sgiornalismo'
              ,r'CONTINUA\sA\sLEGGERE L\'ARTICOLO'
              ,r'COMMENTA\sE\sCONDIVIDI'
              ,r'VUOI\sCONSIGLIARE\sQUESTO\sARTICOLO\sAI\sTUOI\sAMICI'
              ,r'SCOPRI\sLA\sPROMO'
              ,r'PROVA\sGRATIS\s\.USERNAME\s\.PASSWORD'
              ,r'accesso\sillimitato\sagli\sarticoli\sselezionati\sdal\squotidiano'
              ,r'AVVISO\sAI\sLETTORI'
              ,r'\+CONDIVIDI'
              ,r'Segui\sle\snews\sdi\sLaZampa\sit\ssu\sTwitter\s(clicca\squi)\se\ssu\sFacebook\s(clicca\squi)'
              ,r'CONDIVIDI\sNOTIZIA'
              ,r'Accesso\sillimitato\sagli\sarticoli\sselezionati\sdal\squotidiano\sLe\sedizioni\sdel\sgiornale\sogni\sgiorno\ssu\sPC,\ssmartphone\se\stablet'
              ,r'PROVA\sGRATIS\s\sSe\ssei\sgià\sun\scliente\saccedi\scon\sle\stue\scredenziali'
              ,r'DIVENTA\sFAN\sDEL\sMESSAGGERO'
              ,r'Segui\s@ilmessaggeroit'
              ,r'VEDI\sTUTTE\sLE\sFOTOGALLERY\sTorna\sall\'inizio'
              ,r'VEDI\sTUTTI\sGLI\sARTYCOLI'
              ,r'IL\sMONDO\sAVVENIRE\sLeggi\sl\'edizione\sdi\soggi\sPRIMO\sPIANO'
              ,r'IL\sMONDO\sAVVENIRE\sLeggi\sl\'edizione\sdi\soggi'
              ,r'Ogni\sprimo\smartedì\sdel\smese\sMensile\sdi\sitinerari,\sarte,\scultura\sPrimo\sPiano'
              ,r'•\s•\s###\sContributi\s####'
              ,r'Il contributo più votato ### voto data '
              ,r'Contenuto articolo'
              ,r'Accesso illimitato agli articoli selezionati dal quotidiano'
              ,r'Le edizioni del giornale ogni giorno su PC, smartphone e tablet'
              ,r'Dopo aver letto questo articolo mi sento...'
              ,r'commenta la notizia CONDIVIDI LE TUE OPINIONI SU CORRIERE DEL MEZZOGIORNO.IT'
              ,r'commenta la notizia CONDIVIDI LE TUE OPINIONI SU CORRIERE DI BOLOGNA.IT'
              ,r'commenta la notizia CONDIVIDI LE TUE OPINIONI SU CORRIERE'
              ,r'######'
              ,r'NOEPEN'
              ,r'Buone Notizie è gratis in edicola'
              ,r'USERNAME PASSWORD Se sei già un cliente accedi con le tue credenziali'
              ,r'Se sei già un cliente accedi con le tue credenziali'
              ,r'<br />'
              ,r'~'
    ]

    for string in strings:
            text=replaceRE(text,string,'')

    return(text)

# replaces various horizontal and vertical spacing with space
def cleanPrintable(text):
    return(replaceRE(text,r'((?:\\[abnfrtv])+)',' '))  # https://docs.python.org/3/reference/lexical_analysis.html # inner group has not to be matched


# removes multiple spacing with a single space
def cleanLineSep(text):
    return(replaceRE(text,r'((?:\s~)+)',' '))       # inner group has not to be matched

# converts codepoint representation as strings (e.g. \x0c\ into codepoints and decodes them to their char
def convertCodePoints(text):
    from re import finditer
    import string
    import sys

    p=0
    utfText=u""
    for match in finditer(r'((?:\\x..)+)', text) :  # inner group has not to be matched
#        sys.stderr.write(f"I: match:"+str(match)+"\n")
        ba=bytearray(b'')                           # bytearray stores bytes composing codepoint
        codepoints=int(len(match.group())/4)        # number of codepoints
        codepoint=""
        offset, end = match.span()
        utfText+=text[p:offset]
        p=end
        for i in range(0,codepoints):               # n: 1 --> 2 --> 3      \xAA\xAA\xAA
            try:
                i1=int(text[offset+(i*4)+2],16)  
                i2=int(text[offset+(i*4)+3],16)
            except  Exception as e:
                sys.stderr.write(f"W: hex-integer conversion failed at: {text[offset-10:offset]}[{match.group()}]{text[end:end+10]} {str(e)}"+"\n")
                return(False)
            i=(i1*16)+i2
            ba.append(i)
        if codepoints == 1:
            code='ISO8859-1'                        # https://www.i18nqa.com/debug/utf8-debug.html
        else:
            code='utf-8'
        try:
            codepoint=ba.decode(code)
            #                sys.stderr.write(f"I: {text[offset-10:offset]}[{match.group()}]=[{codepoint}]{text[end:end+10]}"+"\n")
        except Exception as e:
            if codepoints==2:                           # twin ISO8859-1 bytes 
                code='ISO8859-1'                        # https://www.i18nqa.com/debug/utf8-debug.html
                ba1=bytearray(b'')
                ba2=bytearray(b'')
                ba1.append(ba[0])
                ba2.append(ba[1])
                try:
                    codepoint=ba1.decode(code)+ba2.decode(code)
                except Exception as e:
                    #    sys.stderr.write(f"W: decoding failed: {text[offset-10:offset]}[{match.group()}]{text[end:end+10]}: {str(e)}"+"\n")
                    pass
        utfText+=codepoint
    utfText+=text[p:]

    return(utfText)


# normalize strings avoiding duplicate blanks, vertical spacing, etc
def normalize(s):
    import re

    s = s.replace(u'\n', '',re.UNICODE| re.MULTILINE) # remove newlines
    s = s.replace(u'\r', '',re.UNICODE| re.MULTILINE) # remove linefeed
    s = s.replace(u'\t', ' ',re.UNICODE| re.MULTILINE) # remove tabs
    s = s.replace(u'^\s+', '',re.UNICODE) # remove leading whitespace
    s = s.replace(u'\s+$', '',re.UNICODE) # remove trailing whitespace
    s = " ".join(re.split("\s+", s, flags=re.UNICODE))
   # cleanr = re.compile('<.*?>')
   # text = re.sub(cleanr, '', s)
    return(s)

# 
def cleanText(text):
    import re
    content = u''
    normalize(text)
    for line in text.splitlines():
        ' '.join(line.split())
        if line.startswith(" "): line = line[:-1]
        content += "~"
        content += line
        content += " "
    content = content.replace(u'~ ~','~')
    
    content = content.replace(u'~© Riproduzione.*$','')
    cleanupRE=['~© RIPRODUZIONE RISERVATA ',
               'Noi non siamo un partito, non cerchiamo consenso, non riceviamo finanziamenti pubblici, ma stiamo in piedi grazie ai lettori che ogni mattina ci comprano in edicola, guardano il nostro sito o si abbonano a Rep.*Mario Calabresi',
               '~© Riproduzione riservata ',
               '~LEGGI LA CRONACA.*$',
               '~Clicca per Condividere.*$']
    for e in cleanupRE:
        r = re.compile(e)
        content = re.sub(r,'',content)
    return(content)

# find named entity candidates with regular expression
# Restrictive version:
# - excludes Beginning of sentence Capitalized words
# - requires Stat-of-sentence marker "~"
# - different RE for title 
#
def doNEC(s,title):
    import re
    
    s=s.replace(u'. ',u'~',re.UNICODE| re.MULTILINE)
    necRE='[^[~|«]([A-Z][\w-]{2,}(?:\s+[A-Z][\w-]+)*)'
#    necRE=r'[^[~|«](\p{Lu}[\w-]{2,}(?:\s+[A-Z][\w-]+)*)' # unicode variant
    necREt='([A-Z][\w-]{2,}(?:\s+[A-Z][\w-]+)*)'   # title
    if (title==True):
        nec=re.findall(necREt,s)
    else:
        nec=re.findall(necRE,s)
    return(nec)

# find named entity candidates with regular expression
# Permissive version of RE :
# - works on cleanText 
# - capitalized words at beginning of sentence accepted
#   --> spurious non-word trailing characters may be catched.
# - includes Nobiliary particles
#   De, di, del, de’, della, du, von, of, der, von der, de la,
#   https://en.wikipedia.org/wiki/Nobiliary_particle
#
def doNECpermissive(s):
    import re
    
    necRE="((?:'?[A-Z][a-z]{1,})\W(?:(?:-|da|di|de|del|della|de\' ?|von|du|der|of|de la|von der)\s)?){1,}"
    nec=re.findall(necRE,s)
    trimlist=[r'\"', r' ', r'\.', r',', r'«', r'(', r')', r';']
    trimthis="|".join(trimlist)
    r=[]
    for s in nec:
        re.sub(trimthis, "", str(s))
        r.append(s)

    return(r)


#
# split using multiple delimiters
# 
def multiSplit(string,delimiters):
    import re
    regexPattern = '|'.join(map(re.escape, delimiters))
    return(re.split(regexPattern, string))


#
# find repeated N-words strings, 1 word at a time
# 
def deleteRepeatedString(keep,text):
    import re
    import constant as k

    found=False
    text=text.strip()
    WA=wordsAhead(text,k.WORDSAHEAD)
    init=text[0:WA]
    r= re.findall(re.escape(init)+".*?"+re.escape(init),text) # ungreedy RE find
    if (r):
        text=r[0][0:-WA]
        found=True
    else:
        NW = wordsAhead(text,1)
        if NW > 0:
            keep+=text[0:NW]+" "
            text=text[NW:]
            # recursion limit can break program.
            try:
                (keep,text,found)=deleteRepeatedString(keep,text)
            except RecursionError as re:
                return(keep,text,found)
    return(keep,text,found)

# 
# Calculate lowercase to text ratio and  words variability ratio
#
def var_noLowCase_ratio(text):
    # Computes two ratios:
    # variability = nr. of different words/ total words
    # noLowCase = nr. of lower case words (except first letter of weach word) /total words

    SPLITAT=';\n\',:?\. ’/”“()\"«»!-'
    
    varlist=[]
    words=0
    lowCase=0
    text= multiSplit(text,SPLITAT)
    for w in text:
        if len(w)<3:
            continue
        words+=1
        if w[1:].islower():
            lowCase+=1
        if w not in varlist:
            varlist.append(w)
    if words == 0:
        return(None,None)
    else:
        return(round(len(varlist)/words,3), round(lowCase/words,3))

#
# returns index of N words ahead in string
# 
def wordsAhead(string , N) :
    ch=" "
    occur = 0
    for i in range(len(string)) : 
        if (string[i] == ch) : 
            occur += 1  
        if (occur == N) : 
            return i     
    return -1


#
# returns ratio of specific chars against text
#
def charsInText(text, string):
    cnt=0
    for c in string:
        cnt+=text.count(c)
    l=len(text)
    if l == 0:
        return(None)
    else:
        return(round(cnt/len(text),3))

#
# tries to fix repeated strings, returns vector of issues and dict of low variability measures
#
def fix_lowvar_noLcase(text,k):
    import constant as k

    measures={}
    issues=[]
    chars=len(text)
    (var,noLowCase)=var_noLowCase_ratio(text)
    nonT= charsInText(text,k.NONTEXT_CHARS)

    measures = {
        k.NOLOWCASE: noLowCase,
        k.NONTEXT:   nonT,
        k.WORDVARIABILITY:var,
        k.LEN:       chars,        
    }

    if noLowCase == None or var == None or nonT == None :
        issues.append(k.SHORTTEXTISSUE)
    else:    
        if noLowCase < k.NOLOWCASE_THRESHOLD:
            issues.append(k.CASEISSUE)
            
        if nonT > k.NONTEXT_THRESHOLD:
            issues.append(k.NONTEXTISSUE)

        if var < k.VAR_THRESHOLD:
            # low word variavility -> probable repeated strings
            (keep,text,found)=deleteRepeatedString("",text)

            if found:
                #issues.append(k.DELREPEATISSUE)
                # deletions occurred, measure again
                (var,noLowCase)=var_noLowCase_ratio(text)
                nonT= charsInText(text,k.NONTEXT_CHARS)
                measures = {
                       k.NOLOWCASE: noLowCase,
                       k.NONTEXT:  nonT,
                       k.WORDVARIABILITY: var,
                       k.LEN:       chars,        
                    }
                # set issues accordingly
                if nonT > k.NONTEXT_THRESHOLD:
                    issues.append(k.NONTEXTISSUE)
                if (var == None) :  
                    issues.append(k.SHORTTEXTISSUE)
                elif var < k.VAR_THRESHOLD:
                    issues.append(k.WORDVARISSUE)
            else:
                # no deletions, 
                issues.append(k.WORDVARISSUE)
    return(text,measures,issues)

